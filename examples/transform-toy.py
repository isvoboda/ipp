#!/usr/bin/env python
# coding=utf-8

from __future__ import division, print_function, unicode_literals

import logging
import os
import sys
from contextlib import closing

import cv2
import numpy as np

sys.path.append("../../")

from ipp import Pipeline, Worker
from ipp.filters import (Copy, Crop, ImageGenerator, LambdaDoom, Merge, Pass,
                         Resize, SelectAttr, TransformMat, WarpImg, ZMQReader,
                         ZMQWriter)


SOCKET_ADDR = "ipc:///tmp/ipp"
logging.basicConfig(level=4)  # ipp.DEBUG_FILTER)


def toy_write_pipeline(socket_addr=SOCKET_ADDR, seed=5):
    """Generate images"""
    img_gen = ImageGenerator(attr="img", shape=(80, 128, 1), dtype="float32",
                             itype="chessboard")
    crop = Crop(attr="img", shape=(192, 192), center="center",
                pad_behavior="pad", pad_mode="constant",
                pad_kwargs={"constant_values": 0.})
    resize_out = Resize(attr="img", out_attr="rimg", shape=(96, -1))

    copy = Copy(n_copies=2)

    g1_params = [
        dict(rotate=[-30, 30]),
        dict(translate=[[-12, 12], [-12, 12]])]
    g2_params = [
        dict(rotate=[-30, 30]),
        dict(translate=[[-12, 12], [-12, 12]])]
    g1_mat = TransformMat("g1_mat", args=g1_params, seed=seed)
    g2_mat = TransformMat("g2_mat", args=g2_params, seed=seed + 2)

    g1_warp = WarpImg(attr="img", attr_mat="g1_mat",
                      attr_out="g1_img", flags=("INTER_LINEAR",))
    g2_warp = WarpImg(attr="img", attr_mat="g2_mat",
                      attr_out="g2_img", flags=("INTER_LINEAR",))

    gr1_params = [
        dict(rotate=[-30, 30]),
        dict(translate=[[-3, 3], [-3, 3]])]
    gr2_params = [
        dict(rotate=[-30, 30]),
        dict(translate=[[-3, 3], [-3, 3]])]
    gr1_mat = TransformMat("gr1_mat", attr_img="rimg",
                           args=gr1_params, seed=seed)
    gr2_mat = TransformMat("gr2_mat", attr_img="rimg",
                           args=gr2_params, seed=seed + 2)

    gr1_warp = WarpImg(attr="rimg", attr_mat="gr1_mat",
                       attr_out="gr1_img", flags=("INTER_LINEAR",))
    gr2_warp = WarpImg(attr="rimg", attr_mat="gr2_mat",
                       attr_out="gr2_img", flags=("INTER_LINEAR",))
    dummy = Pass()

    str_dot = "lambda x: setattr(x, 'invr_mat', x.gr2_mat)"
    dot = LambdaDoom(lmbd=str_dot)

    merge = Merge()
    select = SelectAttr(attrs=["g1_img", "g2_img", "gr1_img", "gr2_img"])
    writer = ZMQWriter(address=socket_addr)

    pipeline = Pipeline([
        [img_gen],
        [crop],
        [copy],
        [dummy, resize_out],
        [g1_mat, gr1_mat],
        [g2_mat, gr2_mat],
        [g1_warp, gr1_warp],
        [g2_warp, gr2_warp],
        [dummy, dot],
        [dummy, merge],
        [select],
        [writer]])

    return pipeline


def toy_read_pipeline(socket_addr=SOCKET_ADDR, seed=5):
    """Read images"""
    reader = ZMQReader(socket_addr, flags=["NOBLOCK"], timeout=15)
    return Pipeline([[reader]], "reader")


p_pl = toy_write_pipeline()
c_pl = toy_read_pipeline()

p_worker = Worker(p_pl)
p_worker.start()

c_worker = Worker(c_pl)

with closing(c_worker.work()) as gen:
    for _ in range(10):
        [packet] = next(gen)
        cv2.imshow("g1_img", packet.g1_img)
        cv2.imshow("g2_img", packet.g2_img)
        cv2.imshow("gr1_img", packet.gr1_img)
        cv2.imshow("gr2_img", packet.gr2_img)
        cv2.waitKey(500)
