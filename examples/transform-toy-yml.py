#!/usr/bin/env python
# coding=utf-8

from __future__ import division, print_function, unicode_literals

import logging
import sys
from contextlib import closing

import cv2

sys.path.append("../../")
from ipp import Pipeline, Worker, pipeline_to_yml, yml_to_pipeline



logging.basicConfig(level=4)  # ipp.DEBUG_FILTER)

pipelines = yml_to_pipeline("toy-pipeline.yml")

p_worker = Worker(pipelines[0])
p_worker.start()

c_worker = Worker(pipelines[1])

with closing(c_worker.work()) as gen:
    for _ in range(10):
        [packet] = next(gen)
        cv2.imshow("g1_img", packet.g1_img)
        cv2.imshow("g2_img", packet.g2_img)
        cv2.imshow("gr1_img", packet.gr1_img)
        cv2.imshow("gr2_img", packet.gr2_img)
        cv2.waitKey(500)
