#!/usr/bin/env python
# coding=utf-8

from __future__ import division, print_function, unicode_literals

import logging
import os
import sys
from contextlib import closing

import cv2
import numpy as np
import zmq

sys.path.append("../../../")
import ipp
from ipp import Pipeline, Worker
from ipp.filters import (Blur, Copy, Crop, CSVReader, ImageReader, Lambda,
                         LambdaDoom, Merge, Pass, PathExpander, PreviewImage,
                         PrintPacket, PyTabH5Reader, Resize, SelectAttr,
                         TransformMat, WarpImg, ZMQReader, ZMQWriter)



logging.basicConfig(level=ipp.DEBUG_FILTER)

SOCKET_ADDR = "ipc:///tmp/ipp"
CSV_PATH = "/mnt/nas.brno.innovatrics.inn/ipteam/image_db/_SRC/tofu/_DAT/pipeline-example-no-problem-to-delete/train.txt"


def tofu_pipeline(csvpath, socket_addr=SOCKET_ADDR, seed=5):
    """Generate and transform tofu images"""

    dir_path = os.path.dirname(csvpath)
    csv_reader = CSVReader(path=csvpath, loop=True, shuffle=True, seed=seed)
    update_path = PathExpander(attrs=("path",), base_path=dir_path)
    img_reader = ImageReader(path_attr="path", img_attr="img")
    normalize = Lambda(attr="img", lmbd="lambda x: x/255.")
    resize_in = Resize(attr="img", shape=(192, -1))
    resize_out = Resize(attr="img", shape=(96, -1), out_attr="rimg")
    crop = Crop(attr="img", shape=(384, 384), center="center",
                pad_behavior="pad", pad_mode="constant",
                pad_kwargs={"constant_values": 0.})
    copy = Copy(n_copies=2)

    g1_params = [
        dict(project=((-10., 10.), (-15., 15), (-25., 25.), 30)),
        dict(scale=((0.9, 1.15),)),
        dict(rotate=[-30, 30]),
        dict(translate=[[-12, 12], [-12, 12]])]
    g2_params = [
        dict(project=((-10., 10.), (-15., 15.), (-45., 45.), (30., 45.))),
        dict(scale=((0.9, 1.15),)),
        dict(rotate=[-30, 30]),
        dict(translate=[[-12, 12], [-12, 12]])]
    g1_mat = TransformMat("g1_mat", args=g1_params, inverse=True, seed=seed)
    g2_mat = TransformMat("g2_mat", args=g2_params,
                          inverse=True, seed=seed + 13)
    g1_warp = WarpImg(attr="img", attr_mat="g1_mat", attr_out="g1_img",
                      flags=("INTER_LINEAR", "WARP_INVERSE_MAP"))
    g2_warp = WarpImg(attr="img", attr_mat="g2_mat", attr_out="g2_img",
                      flags=("INTER_LINEAR", "WARP_INVERSE_MAP"))

    gr1_params = [
        dict(project=((-10., 10.), (-15., 15), (-25., 25.), 30)),
        dict(scale=((0.9, 1.15),)),
        dict(rotate=[-30, 30]),
        dict(translate=[[-3, 3], [-3, 3]])]
    gr2_params = [
        dict(project=((-10., 10.), (-15., 15.), (-45., 45.), (30., 45.))),
        dict(scale=((0.9, 1.15),)),
        dict(rotate=[-30, 30]),
        dict(translate=[[-3, 3], [-3, 3]])]
    gr1_mat = TransformMat("gr1_mat", attr_img="rimg", args=gr1_params,
                           inverse=True, seed=seed)
    gr2_mat = TransformMat("gr2_mat", attr_img="rimg", args=gr2_params,
                           inverse=True, seed=seed + 13)
    gr1_warp = WarpImg(attr="rimg", attr_mat="gr1_mat", attr_out="gr1_img",
                       flags=("INTER_LINEAR", "WARP_INVERSE_MAP"))
    gr2_warp = WarpImg(attr="rimg", attr_mat="gr2_mat", attr_out="gr2_img",
                       flags=("INTER_LINEAR", "WARP_INVERSE_MAP"))

    gr_warp = WarpImg(attr="gr2_img", attr_mat="invr_mat", attr_out="gr_img",
                      flags=("INTER_LINEAR", "WARP_INVERSE_MAP"))

    dummy = Pass()

    str_dot = "lambda x: setattr(x, 'invr_mat', np.linalg.pinv(x.gr2_mat).dot(x.gr1_mat))"
    dot = LambdaDoom(lmbd=str_dot)

    merge = Merge()

    select = SelectAttr(
        attrs=["g1_img", "g2_img", "invr_mat", "gr1_img", "gr2_img"])
    writer = ZMQWriter(address=socket_addr)

    pipeline = Pipeline([
        [csv_reader],
        [update_path],
        [img_reader],
        [crop],
        [resize_in],
        [normalize],
        [copy],
        [dummy, resize_out],
        [g1_mat, gr1_mat],
        [g2_mat, gr2_mat],
        [g1_warp, gr1_warp],
        [g2_warp, gr2_warp],
        [dummy, dot],
        [dummy, gr_warp],
        [dummy, merge],
        [select],
        [writer]])

    return pipeline


tofu_pl = tofu_pipeline(CSV_PATH)

worker = Worker(tofu_pl, "Tofu")
worker.start()

c_pl = Pipeline([[ZMQReader(SOCKET_ADDR, flags=zmq.NOBLOCK, timeout=15)]])
gen = Worker(c_pl)

with closing(gen.work()) as gen:
    for _ in range(10):
        [packet] = next(gen)
        cv2.imshow("g1_img", packet.g1_img)
        cv2.imshow("g2_img", packet.g2_img)
        cv2.imshow("gr1_img", packet.gr1_img)
        cv2.imshow("gr2_img", packet.gr2_img)
        cv2.waitKey(500)
