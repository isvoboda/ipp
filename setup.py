# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

REQS = [
    'matplotlib',
    'msgpack-python',
    'numpy',
    'opencv-contrib-python',
    'pandas',
    'pyzmq',
    'ruamel.yaml',
    'tables'
]

setup(name='ipp',
      version='0.2',
      description='Image Processing Pipeline',
      url='http://git.ba.innovatrics.net/users/pavel.svoboda/repos/ipp',
      author='Pavel Svoboda',
      author_email='pavel.svoboda@innovatrics.com',
      license='MIT',
      packages=find_packages(),
      zip_safe=False,
      python_requires='>=2.7',
      install_requires=REQS)
