# IPP - Image Processing Pipeline

Allows to simply combine several image processing functions wrapped by unified
`Filter` with common input/output interface. Filters can be combined and
arbitrary sorted into a `Pipeline`.
Pipeline support vertical and horizontal data flow allowing to process
different data with randomly sampled transform parameters.

## Known problems, limitations

- Serialized numpy arrays view is not properly deserialized
  - ToDo: try to use the msgpack-numpy serialization directly
- Worker process won't be terminated in case the parent process is terminated abruptly (Probably NIX based systems only)
- ZMQ transfer speed fall down with small messages
  - Probably related to minimal message size (maybe 4KB)
- QueFilters may hang due to non-flushed multiprocessing queue
  - Has to be controlled in parent process, not implemented yet. As we have the ØMQ, Multiprocessing Queue data transfer is deprecated.

## To be fixed

- [x] Windows compatibility
- [ ] Full support of numpy array view serialization
- [ ] Fix several types not been serialized (list of lists, list of numpy arrays, ...)

## Dependencies

- [python 2.7 requirements](requirements-python27.txt)
- [python 3 requirements](equirements-python3.txt)

## Installation using pipenv

Documentation of pipenv: https://docs.pipenv.org/

~~~Bash
git clone ssh://git@git.ba.innovatrics.net:7999/~pavel.svoboda/ipp.git
cd ipp
pipenv install --dev
pipenv shell
~~~

## Code preview

### Python code

~~~Python
#!/usr/bin/env python
# coding=utf-8

import logging
import multiprocessing

import ipp
from ipp import Pipeline, Worker
from ipp.filters import ImageGenerator, Copy, PreviewImage, Pass, ZMQReader, ZMQWriter, SaltPepperNoise

logging.basicConfig(level=logging.DEBUG)
def main():
    """Entry point"""

    channel_url = "tcp://127.0.0.1:8521"

    generator = ImageGenerator(attr="img", shape=(256, 256, 3), dtype="float32", itype="chessboard")
    copy = Copy(n_copies=2)
    pas = Pass()
    salt_pepper = SaltPepperNoise(attr="img", noise_percent=.05, salt_pepper_ratio=.5)
    preview_orig = PreviewImage(attr="img", name="Original", val_scale=1.)
    preview_noisy = PreviewImage(attr="img", name="Noisy", val_scale=1.)
    writer = ZMQWriter(address=channel_url)
    reader = ZMQReader(address=channel_url)

    pipeline_producer = Pipeline([[generator], [copy], [pas, salt_pepper], [pas, writer]])
    pipeline_consumer = Pipeline([[reader], [preview_orig, preview_noisy]])

    worker_producer = Worker(pipeline=pipeline_producer)
    worker_consumer = Worker(pipeline=pipeline_consumer)

    worker_consumer.start()
    worker_producer.start()
    worker_producer.join()
    worker_consumer.join()

if __name__ == "__main__":
    main()

~~~

<!-- --- -->

<!-- ### YAML code

~~~Yaml
workers:
- worker:
    name: DataTrainFace
    pipeline:
        - - PyTabH5Reader: &trainH5 { h5files: [path], data_paths: [/data/table] }
        - - ImageReader: &ReadFace { path_attr: path, img_attr: img, flag: IMREAD_GRAYSCALE }
        - - Rotate: &rot { angle: [-3, 3] }
        - - Blur: &blur { kernel: [0, 3], sigma: [0, 1.2] }
~~~ -->

## IPP Development Recommendations

- Please, use the [Numpy/Scipy style Python Docstring](https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt)
