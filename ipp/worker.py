# coding=utf-8
"""Worker module"""

from __future__ import division, print_function

import multiprocessing
import os
import signal
import time
from logging import (NOTSET, addLevelName, getLogger, getLoggerClass,
                     setLoggerClass)

from .filters import ContinueIteration, Packet
from .utils.loging_levels import TIME_WORKER

__all__ = [
    "Pipeline",
    "Worker"
]


class WorkerLogger(getLoggerClass()):
    """WorkerLogger provide specific logging levels """

    def __init__(self, name, level=NOTSET):
        super(WorkerLogger, self).__init__(name, level)

        addLevelName(TIME_WORKER, "time_worker")

    def time_worker(self, msg, *args, **kwargs):
        """Logging worker time level"""
        if self.isEnabledFor(TIME_WORKER):
            self._log(TIME_WORKER, msg, args, **kwargs)


setLoggerClass(WorkerLogger)


class Pipeline(object):
    """Pipeline

    Implements a Context Manager wrapper together with a signal
    handler to safely dispose all the filters.

    Parameters:
    -----------
    pipeline : list of list of Filter
        List of list [[]] of filters defining the processing pipeline

    """

    def __init__(self, pipeline, name=None):
        self.pipeline = pipeline
        self.pipeline_name = name or type(self).__name__
        self.signal_list = []
        self.deferred = []
        self.previous_handlers = {}
        self.interrupted = False
        self._cache = {}

    @property
    def log(self):
        """Lazy initialized logger"""
        try:
            return self._cache["log"]
        except KeyError:
            logger_name = __name__ + "." + self.pipeline_name
            self._cache["log"] = getLogger(logger_name)
        return self._cache["log"]

    def _sig_handler(self, _sign, _stack_frame):
        """Stores deferred incoming signals and interrupt the CM"""
        self.deferred.append(_sign)

        if not self.interrupted:
            self.interrupted = True
            raise StopIteration

    def __enter__(self):
        signals = ["SIGINT", "SIGTERM", "SIGHUP"]
        for sig in signals:
            try:
                self.signal_list.append(getattr(signal, sig))
            except AttributeError as attr_exp:
                self.log.debug("%s", attr_exp)
                continue

        for sig_num in self.signal_list:
            self.previous_handlers[sig_num] = (
                signal.signal(sig_num, self._sig_handler) or signal.SIG_DFL)

        return self.pipeline

    def __exit__(self, exc_type, exc_value, traceback):
        # Dispose all the pipeline filters
        self.dispose()
        # Set default signal handlers
        for sign in self.signal_list:
            signal.signal(sign, self.previous_handlers[sign])
        # Resend the deferred signals
        while self.deferred:
            _sign = self.deferred.pop(0)
            os.kill(os.getpid(), _sign)

    def dispose(self):
        """Dispose all the pipeline filters"""
        self.log.info("Disposing the pipeline filters")
        for layer in self.pipeline:
            for p_filter in layer:
                p_filter.dispose()


class Worker(multiprocessing.Process):
    """Process the Pipeline

    Worker sequentially calls all the filters in a pipeline.
    Data could flow by two directions, primary is the vertical flow,
    where a packet of previous filter ascends to a next filter.
    Secondary flow may occur in the horizontal direction.

    FA0 -> FB0
     |      |
     v      v
    FA1 -> FB1
     |      |
     v      v
    FA2 -> FB2

    """

    def __init__(self, pipeline=None, name=None):
        """
        Worker constructor

        Parameters
        ----------
        pipeline : Pipeline
            Pipeline is context manager wrapping a list of lists [[]] of
            filters which are subsequently called.
        name : str
            Worker name
        """

        super(Worker, self).__init__(name=name)
        self.daemon = False
        self.worker_name = name or self.name
        self.pipeline = pipeline
        self.pipeline.pipeline_name = self.worker_name
        self.stop_event = multiprocessing.Event()
        self.time_scale = 1000 # milliseconds
        self._cache = {}

    @property
    def log(self):
        """Lazy initialized logger"""
        try:
            return self._cache["log"]
        except KeyError:
            logger_name = __name__ + "." + self.worker_name
            self._cache["log"] = getLogger(logger_name)
        return self._cache["log"]


    def _work(self, pipeline):
        try:
            packets = [Packet()] * len(pipeline[0])
            for l_id, layer in enumerate(pipeline):
                packets_layer = []

                assert len(packets) == len(layer)

                for f_id, (packet, p_filter) in enumerate(zip(packets, layer)):
                    packet_out = p_filter(packet, packets_layer)
                    packets_layer.extend(packet_out)
                packets = packets_layer

            return packets

        except (AttributeError, ValueError):
            self.log.exception("Filter %s id %d in layer %d failed.",
                               type(p_filter).__name__, f_id, l_id)
            raise

        except AssertionError:
            self.log.error("Pipeline layer %d/%d does not fit with %d "
                           "filters the number of input packets %d",
                           l_id, len(pipeline), len(layer), len(packets))
            raise

    def work(self):
        """Generator function running the pipeline yields the packets

        Example
        -------
        >>> for packets in worker.work():
        >>>    # do some stuff with packets
        >>>
        >>> # Control number N of iteration
        >>> for i, packets in (tup for tup in zip(range(N), worker.work())):
        >>>    # do some stuff with packets
        """
        self.log.info("start")
        packets = None
        with self.pipeline as pipeline:
            while True:
                begin_work = time.time()
                try:
                    packets = self._work(pipeline)
                    yield packets

                except ContinueIteration:
                    continue
                except StopIteration:
                    self.log.info("stop")
                    break

                end_work = time.time()
                # Use specific IPPLoggerLevels TIME_WORKER level
                # pylint: disable=E1101
                self.log.time_worker("Processing single data took: %f ms",
                                     (end_work - begin_work) * self.time_scale)

    def run(self):
        """Start the pipeline loop

        Note
        ----
        Worker inherits from multiprocessing.Process, i.e. to run
        this method in separate process, call the worker.start().
        worker.run() runs the method directly in the same process.
        """
        work_generator = self.work()
        while not self.stop_event.is_set():
            next(work_generator)

    def stop(self):
        """Stops worker by setting an Event object"""
        self.stop_event.set()
        self.log.info("Stop event received: %s", self.name)
