# coding=utf-8
"""Worker parser"""

from __future__ import print_function, unicode_literals

import numpy as np
import ruamel.yaml as yaml


from .filters import FilterFactory
from .worker import Pipeline

__all__ = [
    "yml_to_pipeline",
    "pipeline_to_yml"
]


PIPELINE_SEC = 'pipelines'
PIPELINE_ITEM = 'pipeline'
FILTERS_ATTR = 'filters'
NAME_ATTR = 'name'


def yml_to_pipeline(file_path):
    """Creates pipeline based on the YAML pipeline configuration

    Parameters
    ----------
    file_path : str
        Path to the configuration file.

    Raises
    ------
    IOError
        Raised when file does not exists or incorrectly formatted.

    Returns
    -------
    out : tuple
          List of created pipelines of type ipp.Pipeline
    """

    with open(file_path, 'r') as f:  # pylint: disable=C0103
        conf = yaml.round_trip_load(f)

    if not conf:
        raise IOError('Could not read file "{}"!'.format(file_path))

    if PIPELINE_SEC not in conf:
        raise IOError(
            'Invalid file format, could not find the definition of pipelines!')

    pipelines = []

    for pipeline_sec in conf[PIPELINE_SEC]:
        pl = pipeline_sec[PIPELINE_ITEM]
        parsed_filters = [[_parse_filter(x) for x in layer]
                          for layer in pl[FILTERS_ATTR]]
        name = pl.get(NAME_ATTR, None)
        pipelines.append(Pipeline(pipeline=parsed_filters, name=name))

    return pipelines


def _parse_filter(filter_dict):
    name, params = filter_dict.popitem()
    if params is None:
        params = {}
    if name in ['Picker']:
        filter_in_picker = _parse_filter(params['filter'])
        params['filter'] = filter_in_picker
    try:
        return FilterFactory().create(name, **params)
    except:
        print('Error at line {} while parsing filter "{}"!'.format(
            filter_dict.lc.line + 1, name))
        raise


def pipeline_to_yml(pipelines):
    """Generates a yml representation of the given pipeline.

    Parameters
    ----------
    pipelines : Pipeline

    Returns
    -------
    out : string
          String representation of the pipeline using YAML format
    """

    ydict = {}

    ydict[PIPELINE_SEC] = []
    for pl in pipelines:
        pdict = {NAME_ATTR: pl.pipeline_name, FILTERS_ATTR: []}
        for layer in pl.pipeline:
            pdict[FILTERS_ATTR].append(
                [_filter_to_yaml_dict(pfilter) for pfilter in layer])
        ydict[PIPELINE_SEC].append({PIPELINE_ITEM: pdict})
    return yaml.round_trip_dump(ydict)


def _conv_with_type_handling(attr):
    if isinstance(attr, np.random.RandomState):
        return None
    elif isinstance(attr, (tuple, list)):
        return [_conv_with_type_handling(at) for at in attr]

    return attr


def _filter_to_yaml_dict(pfilter):
    params = {}
    filter_name = pfilter.__class__.__name__
    for param in pfilter.parameters:
        try:
            attr = getattr(pfilter, param.name)
            params[param.name] = _conv_with_type_handling(attr)
        except AttributeError:
            continue

    return {filter_name: params}
