"""Meta Singleton
Allows to provide a singleton pattern for a particular type which
__metaclass__ attribute is set to Singleton

ToDo:
Logging is not handled as it is called during importing the module when
the logger is not initialized yet
"""

from __future__ import print_function


class Singleton(type):
    """Singleton metaclass modifying the __call__ method

    From python docs:
    You can of course also override other class methods (or add new
    methods); for example defining a custom __call__() method
    in the metaclass allows custom behavior when the class is called,
    e.g. not always creating a new instance.
    """
    # Classes types
    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        When the type is already an instance, return the existing one.
        """
        if cls not in cls._instances:
            cls._instances[cls] = super(
                Singleton, cls).__call__(*args, **kwargs)

        return cls._instances[cls]
