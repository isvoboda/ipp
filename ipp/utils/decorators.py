# coding=utf-8
"""IPP decorators"""

from functools import partial, update_wrapper, wraps
from collections import Hashable
from weakref import WeakValueDictionary

# pylint: disable=C0103,R0903

__ALL__ = [
    "LazyInit"
]

LAZY_INIT = "_lazy_init"


# def before_once(init_method=None):
#     """"Before once called decorator

#     Works with class method only.

#     ToDo: This is quite a nasty solution
#     - problem of possible overlapping attribute
#     - problem of method compatibility only

#     Stores the information, the lazy initialization passed in the
#     object instance directly (self).

#     Parameters
#     ----------
#         init_method : str
#             Name of the lazy init method defined in the filter class.
#             Default method name is '_lazy_init'.
#     """
#     _init_method = init_method or LAZY_INIT

#     def _decorator(method):
#         attr = "_lazy_initialized"
#         @wraps(method)
#         def _wrapper(self, *args, **kwargs):

#             if hasattr(self, attr):
#                 return method(self, *args, **kwargs)
#             else:
#                 init_fce = getattr(self, _init_method)
#                 init_fce()
#                 setattr(self, attr, True)
#                 return method(self, *args, **kwargs)

#         return _wrapper
#     return _decorator


class Cache(object):
    """Cache decorator

    all decorated methods are evaluated only once. The result is
    cached and returned every time the method is called again.

    Note
    ----
    See the descriptor pattern used for decorating class methods.
    """
    def __init__(self, func):
        self.func = func
        update_wrapper(self, func)
        self.attr = func.__name__
        self.cache = WeakValueDictionary()

    def __call__(self, *args, **kwargs):
        if not isinstance(args, Hashable):
            print("Can not cache the method {}".format(self.attr))
            return self.func(*args, **kwargs)
        key = self.attr + str(args)
        if key not in self.cache:
            self.cache[key] = self.func(*args, **kwargs)

        return self.cache[key]

    def __get__(self, instance, owner):
        return partial(self.__call__, instance)


class LazyInit(object):
    """Lazy initialization decorator"""
    def __init__(self, lazy_init_str=None):
        self.lazy_init_str = lazy_init_str or LAZY_INIT
        self.done = False

    def __call__(self, func):
        update_wrapper(self, func)
        self.func = func
        return self

    def __get__(self, inst, inst_type=None):
        if not hasattr(inst, "_initialized"):
            self.lazy_init = getattr(inst, self.lazy_init_str)
            self.lazy_init()
            setattr(inst, "_initialized", True)

        return partial(self.func, inst)
