# coding=utf-8
""""Basic NVidia GPU info via nvidia-ml-py"""

from __future__ import division, print_function

import logging

LOGGER = logging.getLogger(__name__.split(".")[0] + ".GPUInfo")

try:
    import pynvml
except ImportError:
    LOGGER.exception("Missing Python Bindings for the NVIDIA Management "
                     "Library --- nvidia-ml-py / nvidia-ml-py3")

__all__ = [
    "GPUInfo"
]


class GPUInfo(object):
    """Available NVidia GPUs information

        - get GPU ID based on most available memory (gpu_id)
        - get a list of GPU names in system (list_gpu_names)

    ToDo:   Method to identify a free GPU
            Handle exceptions of nvml methods!
    """

    log = LOGGER
    GiB = 1024 ** 3

    @classmethod
    def list_gpu_names(cls):
        """Returns list of available NVidia GPUs names"""

        if cls.__init_nvml():
            gpu_names = []
            handles = cls.__get_handles()
            for handle in handles:
                gpu_name = pynvml.nvmlDeviceGetName(handle)
                gpu_names.append(gpu_name)

            cls.__release_nvml()

            return gpu_names
        else:
            return None

    @classmethod
    def gpu_id(cls):
        """Returns the GPU id based on available free memory

        Returns
        -------
        int
            GPU id
        None
            In a case of missing CUDA/GPU
        """

        if cls.__init_nvml():
            dev_free_memory_list = []
            handles = cls.__get_handles()
            for i_handle, handle in enumerate(handles):
                gpu_name = pynvml.nvmlDeviceGetName(handle)
                memory_free = pynvml.nvmlDeviceGetMemoryInfo(handle).free
                memory_free /=  cls.GiB
                dev_free_memory_list.append((i_handle, gpu_name, memory_free))
                cls.log.debug("Available GPU: %s ID: %d Free mem: %.2f GiB",
                              gpu_name, i_handle, memory_free)

            dev_free_memory_list.sort(
                key=lambda sort_key: sort_key[2], reverse=True)

            (gpu_id, gpu_name, memory_free) = dev_free_memory_list[0]
            cls.log.info("GPU: %s ID: %d Free mem: %.2f GiB",
                         gpu_name, gpu_id, memory_free)

            cls.__release_nvml()

            return gpu_id

        else:
            cls.log.info("No GPU found.")
            return None

    @classmethod
    def __init_nvml(cls):
        """Initialize NVidia Management Library
        Note: Should be called before any pynvml function call."""

        try:
            cls.log.debug("Initializing NVidia Management Library")
            pynvml.nvmlInit()
            return True
        except pynvml.NVMLError_LibraryNotFound:
            cls.log.error("NVML library not found")
            return False

        except pynvml.NVMLError:
            cls.log.error("GPUInfo - pynvml initialization")
            return False

    @classmethod
    def __release_nvml(cls):
        """Shut down the initialized NVidia Management Library
        Note: Should be called after any pynvml function has been called."""

        cls.log.debug("Releasing NVidia Management Library")
        pynvml.nvmlShutdown()

    @classmethod
    def __get_handles(cls):
        """Returns list of all available GPU handles.
        Note: pynvml.nvmlInit() has to be called first."""

        n_devices = pynvml.nvmlDeviceGetCount()
        handles = []
        for device_id in range(n_devices):
            handles.append(pynvml.nvmlDeviceGetHandleByIndex(device_id))

        return handles
