# coding=utf-8
"""IPP logging levels

Define the specific IPP log levels
"""

TIME_TRANSFER = 18
TIME_WORKER = 16
TIME_FILTER = 14
DEBUG_FILTER = 8
