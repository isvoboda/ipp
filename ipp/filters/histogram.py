# coding=utf-8
"""Histogram filters"""

from __future__ import division, print_function

import matplotlib  # pylint: disable=c0411

# To disable X-backend
# Once in the future it is probably better to use :
# http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.switch_backend
# matplotlib.use("Agg")

import matplotlib.pyplot as plt  # pylint: disable=c0411,c0413

from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP

__all__ = [
    "NumericHistogram"
]

# pylint: disable=C0103


class NumericHistogram(Filter):

    """Filter for creating a histogram of numeric values

     Parameters
        ----------
        attr : str
            Name of the attribute expected to be held by the
            incoming packets upon which the histogram is built.
        bounds : tuple, list (min, max)
            The expected lower and upper bound of the observed values
            as a tuple of numbers.
        delta : int
            Size of the histogram bins (positive integer)
        bins : tup, list
            List or tuple of lists or tuples defining the
            intervals [min,max)
        graph: str
            If defined, plots a graph. Specifies a graph name.
        nth : int
            Defines every nth iteration to print and if graph set also
            plot the histogram.
            Default is None, i.e. histogram is plotted in the end only.
        xticks : list
            Sets graph x ticks. Defsult is based on the histogram bins
            (leftmost).

        Raises
        ------
        ValueError
            Raised if incompatible or missing settings.
    """

    parameters = [
        FP(name="attr", help_msg="Name of the attribute expected to be held \
        by the incoming packets upon which the histogram is built.",
           value_type=str, required=True),
        FP(name="bounds", help_msg="The expected lower and upper bound of the \
        observed values as a tuple of numbers.", value_type=tuple,
           required=False), FP(name="delta", help_msg="Size of the histogram \
           bins (positive integer)", value_type=int, required=False),
        FP(name="bins", help_msg="Histogram bins as a list or tuple of \
            particular intervals [min, max)", value_type=tuple,
            required=False),
        FP(name="graph", help_msg="Name of a graph to be plotted",
           value_type=str, required=False),
        FP(name="nth", help_msg="Print and if defined plot the graph every \
        nth packet", value_type=int, required=False),
        FP(name="xticks", help_msg="Sets graph x ticks.", required=False)
    ]

    def __init__(self,
                 attr,
                 bounds=None,
                 delta=None,
                 bins=None,
                 graph=None,
                 nth=None,
                 xticks=None):
        """NumHistogram constructor"""

        super(NumericHistogram, self).__init__()
        self.attr = attr
        self.total = 0

        self.delta = delta
        self.bounds = bounds

        if bounds is not None and delta is not None:
            if delta <= 0:
                raise ValueError(
                    'Histogram delta has the be a positive integer!')

            self.min, self.max = bounds
            if self.min >= self.max:
                raise ValueError('Invalid histogram range!')

            self.hist = self._generate_hist_for_range(
                self.min, self.max, self.delta)
            self.bins = self._create_bins(self.min, self.max, self.delta)
            self._update_hist = self._update_hist_bin

        elif bins is not None:
            self.bins = bins
            self.hist = [0] * len(bins)
            self._update_hist = self._update_hist_bin
        else:
            raise ValueError("bounds and delta, or bins have to be specified.")

        self.graph = graph
        self.nth = nth
        self.xticks = xticks
        self.ind = 0

    @staticmethod
    def _generate_hist_for_range(min_val, max_val, delta):
        diff = max_val - min_val + 1
        return int(diff // delta + (diff % delta > 0)) * [0]

    def update_hist(self, packet):
        """Updates the histogram based on the incoming packet

        Parameters
        ----------
        packet : Packet

        Raises
        ------
        AttributeError
            raised when packet doesn't contain the required attributes
        """
        try:
            value = getattr(packet, self.attr)
        except AttributeError:
            self.log.exception(
                "Attribute %s error at %s filter", self.attr, self.__class__)
            raise

        self._update_hist(value)

    def _create_bins(self, min_val, max_val, delta):
        bins = [(min_val + i * delta, min(max_val, min_val + (i + 1) * delta))
                for i in range(len(self.hist))]
        return bins

    def _update_hist_range(self, value):
        if value < self.min:
            ex = self._generate_hist_for_range(value, self.min, self.delta)
            self.hist = ex + self.hist
            self.min -= len(ex) * self.delta
            self.bins = self._create_bins(self.min, self.max, self.delta)
        elif value > self.max:
            if (self.max - self.min) % self.delta > 0:
                d = (self.max - self.min) // self.delta
                self.max = self.min + self.delta * (d + 1)
            ex = self._generate_hist_for_range(self.max, value, self.delta)
            self.hist = self.hist + ex
            self.max += len(ex) * self.delta
            self.bins = self._create_bins(self.min, self.max, self.delta)

        i_bin = int((value - self.min) // self.delta)
        self.total += 1
        self.hist[i_bin] += 1
        self.i_bin = i_bin

    def _update_hist_bin(self, value):
        for i_bin, interval in enumerate(self.bins):
            if interval[0] <= value < interval[1]:
                self.hist[i_bin] += 1
                self.total += 1
                self.i_bin = i_bin

    def compute_weights(self, packet):
        """Computes the weight coefs for bins in the histogram

        Parameters
        ----------
        packet : Packet
        hpacket: not used

        Returns
        -------
        out : [Packet]
              List of Packet with the computed statistics
        """
        packet.histogram_size = self.total
        packet.histogram_bins = self.bins
        packet.histogram_ibin = self.i_bin
        packet.histogram_weights = [x / self.total for x in self.hist]

        return [packet]

    def print_histogram(self):
        """Prints histogram and optionally plots the histogram graph. """

        bins_str = ["[{}:{})".format(intv[0], intv[1]) for intv in self.bins]
        msg_list = ["{}".format(val / self.total) for val in self.hist]
        max_width = max(map(len, msg_list))

        self.log.info(self.attr)
        for val_label, val in zip(bins_str, msg_list):
            self.log.info("%s : %s", val_label, val.ljust(max_width))
        self.log.info("Total: %d", self.total)

    def plot_histogram(self):
        """Plots a histogram graph"""

        figure = plt.figure()
        ax = figure.add_subplot(111)
        left_most, _ = zip(*self.bins)
        widths = [intv[1] - intv[0] for intv in self.bins]
        ax.bar(left=left_most, height=self.hist,
               width=widths, align="edge", linewidth=1, edgecolor='w')
        if self.xticks is None:
            self.xticks = left_most
        ax.set_xticks(self.xticks)
        ax.set_xlabel(self.attr)
        ax.set_ylabel("count")
        ax.set_title(self.graph)
        figure.savefig(self.graph)
        plt.close(figure)

    def __call__(self, packet, hpacket):
        self.update_hist(packet)
        if self.nth is not None and (self.total % self.nth is 0):
            self.print_histogram()
            if self.graph is not None:
                self.plot_histogram()

        return self.compute_weights(packet)

    def dispose(self):
        self.print_histogram()
        if self.graph is not None:
            self.plot_histogram()
