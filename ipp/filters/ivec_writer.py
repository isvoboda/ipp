# coding=utf-8
"""Ivec writer"""

import logging
import inno.imgtrans as imgtrans
from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP
import numpy as np

__all__ = [
    "IVecWriter"
]


class IVecWriter(Filter):
    """
    Write an image from ImageVector

    Input
    -----
    packet.img ndarray
        image to be saved to vector

    Output
    ------
    """
    parameters = [
        FP(name="vector_path", value_type=str, required=True,
           help_msg="The path where the vector should be created"),
        FP(name="compression_level", value_type=int, required=False,
           default=0, help_msg="Compression level. 0 [default] - no compression")
    ]

    def __init__(self, vector_path, compression_level=0):
        '''Initialize IVecWriter

        Parameters
        ----------
        vector_path: str
            The path where the vector should be created

        compression_level: int
            The level of compression used in image vector. If set to 0,
            no compression is used. Max compression is 9.

        '''
        super(IVecWriter, self).__init__()
        self.vector_path = vector_path
        self.compression_level = compression_level

        self.img_vector = imgtrans.ImageVector(vector_path=None,
                                               in_memory=False,
                                               compression_level=self.compression_level)
        self.img_vector.save(self.vector_path)

    def write(self, packet):
        """Write the image to ImageVector

        Parameters
        ----------
        packet : Packet

        """

        try:
            # packet.img should be uint8 in range 0 - 255
            img_uint8 = packet.img.astype(np.uint8)

            size_of_vect = self.img_vector.append(img_uint8)

            packet.vector_path = self.vector_path
            packet.vector_idx = size_of_vect - 1

            return [packet]

        except IOError:
            self.log.exception("Error reading %r", self.vector_path)
            raise
        except AttributeError:
            self.log.exception("Attribute error")
            raise

    def dispose(self):
        self.img_vector.save(vector_path=self.vector_path)

    def __call__(self, packet, horizontal_packet=None):
        """Writes the image"""
        return self.write(packet)
