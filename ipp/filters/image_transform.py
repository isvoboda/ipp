"""Image transformations"""
# coding=utf-8

from __future__ import division, print_function

import collections

import cv2
import numpy as np

from ..utils import LazyInit
from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP

__all__ = [
    "Flip",
    # "Homography",
    "Resize",
    "Rotate",
    "Scale",
    "TransformMat",
    "Translate",
    "WarpImg",
    "Align",
    "PtsAugmentation"
]

# import inno.icv2 as icv2


class Flip(Filter):

    """Flip an image based on x, y, or both axis

    Parameters
    ----------
    attr : str
        Attribute name to flip.
    flip_code : int
        Defines the axe the image is flopped over.
        0 flip based on x
        1 flip based on y (defaults)
        -1 flip based on both x and y.
    random : bool
        If set True (default False), flip is performed randomly based on
        the flip_code.
    seed : int
        Random number generator seed (default 5).

    Note
    ----
    Based on the OpenCv flip function and a flipCode.
    """
    parameters = [
        FP(name="attr", value_type=str, required=False, default="img",
           help_msg="Attribute name to flip."),
        FP(name="flip_code", value_type=int, required=False, default=1,
           help_msg="The OpenCv flipCode variable. \
           0 flip around x, \
           1 flip around y (default), \
           -1 flip both, x and y."),
        FP(name="random", value_type=bool, required=False, default=False,
           help_msg="If set True (default False), flip is performed \
           randomly based on the flip_code."),
        FP(name="seed", value_type=int, required=False, default=5,
           help_msg="Random number generator seed (default 5).")
    ]

    def __init__(self, attr=None, flip_code=1, random=False, seed=5):
        """Flip initialization"""

        super(Flip, self).__init__()
        self.attr = attr or "img"
        self.flip_code = flip_code
        self.random = random
        self.rng = np.random.RandomState(seed)

    def flip(self, packet, hpacket):
        """Flip packet.img

        Parameters
        ----------
        packet : Packet
            expected attribute packet.img
        hpacket : Packet
            not used

        Returns
        -------
        out : [Packet]
            List of packet with resized img attribute.

        """
        img = getattr(packet, self.attr)
        if self.random and self.rng.choice(2) == 1:
            img = cv2.flip(src=img, flipCode=self.flip_code)
            setattr(packet, self.attr, img)
            return [packet]
        else:
            return [packet]

    def __call__(self, packet, hpacket):
        return self.flip(packet, hpacket)


# class Homography(Filter):

#     """Filter performing homography on a given image (ie generating \
#     another by mapping points of interest to the corresponding points)

#     Parameters:
#     ----------
#     dest_pts: list of points (tuple (x, y))
#         If specified, the dest_pts will be used as
#         destination points and the ones from packet
#         will not be used
#     dest_size: tuple (width, height)
#         If specified, output image will be of dest_size,
#         otherwise output image will have the same size as input.
#     border_mode: str
#         OpenCv warp affine flag, that controlls the border handling
#         Default value is 'BORDER_REPLICATE'.
#     """

#     parameters = [
#         FP(name="attr", value_type=str, required=False, default="img",
#            help_msg="Attribute name to apply homography."),
#         FP(name="dest_pts", required=False, default=None,
#            help_msg="If specified, the dest_pts will be used as \
#                 destination points and the ones from packet \
#                 will not be used."),
#         FP(name="dest_size", required=False, default=None,
#            help_msg="If specified, output image will be of dest_size, \
#                      otherwise output image will have the same size as input."),
#         FP(name="border_mode", required=False, default="BORDER_REPLICATE", \
#            help_msg="OpenCv warp affine flag, that controlls the border handling")
#     ]

#     def __init__(self, attr=None, dest_pts=None, dest_size=None, border_mode="BORDER_REPLICATE"):
#         """Initializes the filter"""
#         super(Homography, self).__init__()
#         self.attr = attr if attr is not None else "img"
#         self.dest_pts = dest_pts
#         self.dest_size = dest_size
#         self.border_mode = border_mode

#     def transform(self, packet, hpacket=None):
#         """Performs homography

#         Parameters
#         ----------
#         packet : Packet
#             expected attributes:
#             img : ndarray
#                 Input image
#             source_pts : list of points (tuple (x, y))
#                 List of source points - at least of length 2.
#             dest_pts : list of points (tuple (x, y))
#                 List of destination points - one for each source point.
#                 It the dest_pts is specified as a filter parameter,
#                 this one will be omitted
#             dest_size : tuple (width, height)
#                 If specified, output image will be of dest_size,
#                 otherwise output image will have the same size as input.
#         hpacket : Packet
#             Not used

#         Exceptions
#         ----------
#         ValueError - raised when input data is invalid
#         AttributeError - raised when packet doesn't contain the
#             required attributes

#         Returns
#         -------
#         out : [Packet]
#                 List of Packet with the transformed image.
#         """
#         try:
#             img = getattr(packet, self.attr)
#             src_pts = list(packet.source_pts)

#             if self.dest_pts is None:
#                 dst_pts_local = list(packet.dest_pts)
#             else:
#                 dst_pts_local = list(self.dest_pts)

#             if len(src_pts) < 2 or len(dst_pts_local) < 2:
#                 raise ValueError(
#                     'List of points has to be at least of length 2!')
#             if len(src_pts) != len(dst_pts_local):
#                 raise ValueError(
#                     'Mismatch between source and destination points!', src_pts, 'and', dst_pts_local)

#             dst_size = (img.shape[1], img.shape[0])
#             if self.dest_size is not None:
#                 dst_size = self.dest_size
#             warp = icv2.get_affine_transform(src_pts, dst_pts_local) \
#                 if len(src_pts) == 2 \
#                 else cv2.getAffineTransform(np.float32(src_pts), np.float32(dst_pts_local))
#             setattr(packet, self.attr,
#                     cv2.warpAffine(img, warp, tuple(dst_size), borderMode= getattr(cv2, self.border_mode)))
#             return [packet]

#         except ValueError:
#             self.log.exception(
#                 'Value error at %s filter', self.__class__)
#             raise
#         except AttributeError:
#             self.log.exception(
#                 "Attribute error at %s filter", self.__class__)
#             raise

#     def __call__(self, packet, hpacket=None):
#         """Returns the transformed image"""
#         return self.transform(packet, hpacket)


class Resize(Filter):

    """Resize an image into a user defined shape

    Parameters
    ----------
    attr : str
        Attribute to be resized. Default is 'img'.
    shape : tuple, list
        Destination image size. If given shape dimension is equal to -1,
        the scale of this dimension is computed such that the resize
        keeps aspect ratio. (height, width)
    method : str
        Name of OpenCv interpolation method used to resize the image.
        - INTER_NEAREST - a nearest-neighbor interpolation
        - INTER_LINEAR - a bilinear interpolation (used by default)
        - INTER_AREA - resampling using pixel area relation. It may be
        a preferred method for image decimation, as it gives
        moire-free results. But when the image is zoomed, it is
        similar to the INTER_NEAREST method.
        - INTER_CUBIC - a bicubic interpolation over 4x4 pixel neighborhood
        - INTER_LANCZOS4 - a Lanczos interpolation over 8x8 pixel
        neighborhood
    out_attr: str
        If set, the resized data are stored in this attribute.
        Otherwise, the attr is overwritten.

    Raises
    ------
    AttributeError

    Attributes
    ----------
    shape : tuple
        User defined destination shape an image is resized to.
    method : str
        Interpolation method.

    """

    parameters = [
        FP(name="attr", value_type=tuple, required=False,
           help_msg="The attribute to be resized."),
        FP(name="shape", value_type=tuple, required=True,
           help_msg="The destination size an image is resized to."),
        FP(name="method", value_type=str, required=False,
           default="INTER_LANCZOS4",
           help_msg="Type of interpolation used to resize an image. Reflects \
           the interpolation names of cv2 python module, see \
           http://docs.opencv.org/2.4/modules/imgproc/doc/geometric_transformations.html#resize"),
        FP(name="out_attr", value_type=str, required=False,
           help_msg="The attribute to save the resized data in. If not set, \
           attr is overwritten")
    ]

    def __init__(self, attr=None, shape=None, method=None, out_attr=None):
        """Constructor Resize"""
        super(Resize, self).__init__()
        self.attr = attr or "img"
        self.shape = shape
        self.method = method or "INTER_LANCZOS4"
        self.resize_method = getattr(cv2, self.method)
        self.out_attr = out_attr or self.attr

    def resize(self, packet, hpacket=None):
        """Resize the defined attribute"""

        attr_data = getattr(packet, self.attr)
        in_shape = attr_data.shape
        dims = len(attr_data.shape)
        shape = list(self.shape)

        # Compute the missing size
        if -1 in shape:
            dim = shape.index(-1)
            pivot_dim = 1-dim
            f_pivot = shape[pivot_dim] / in_shape[pivot_dim]
            shape[dim] = int(round(f_pivot * in_shape[dim]))

        attr_data = cv2.resize(
            attr_data, (shape[1], shape[0]), interpolation=self.resize_method)

        if len(attr_data.shape) < dims:
            attr_data = np.reshape(
                attr_data, (shape[1], shape[0], -1))

        setattr(packet, self.out_attr, attr_data)
        return [packet]

    def __call__(self, packet, hpacket=None):
        return self.resize(packet, hpacket)


class Rotate(Filter):

    """Filter performing rotation of a given image wrt center

    Parameters
    ----------
    attr : str
        Attribute name to apply the rotation to.
    angle: number, tuple, list
        If number, it represents an exact rotation.
        If tuple or list, the interval from which the rotation angle is
        randomly chosen.
    seed : int
        Mersenne Twister random number generator seed.

    Exceptions
    ----------
    ValueError :
        raised when input data is invalid
    AttributeError :
        raised when packet doesn't contain the required attributes

    Returns
    -------
    out : [Packet]
            List of Packet with the transformed image.
    """

    parameters = [
        FP(name="attr", default="img", value_type=str,
           help_msg="Attribute name to apply the rotation to."),
        FP(name="angle", default=0,
           help_msg="If number, it represents the exact rotation. \
           If tuple or list, the interval from which the rotation angle is \
           randomly chosen."),
        FP(name="seed", default=0,
           help_msg="Initializer for the random generator."),
        FP(name="center", default=None, help_msg="center of rotation. If not set \
           center of image is used.")
    ]

    def __init__(self, attr="img", angle=0, seed=5, center=None):
        """Initializes the filter"""
        super(Rotate, self).__init__()
        self.attr = attr
        self.seed = seed
        self.angle = angle
        self.rng = np.random.RandomState(seed)
        self.center = center

    def _lazy_init(self):
        if isinstance(self.angle, (list, tuple)):
            self.rot = self._rand_rot
        else:
            self.rot = self._rot

    def _rand_rot(self, img, angle):

        sampled_angle = self.rng.uniform(*angle)
        return self._rot(img, sampled_angle)

    def _rot(self, img, angle):

        if img.shape[2] > 4:
            return self._multi_channel_rot(img, angle)
        else:
            return self._image_channel_rot(img, angle)

    def _multi_channel_rot(self, img, angle):

        imgs = [self._image_channel_rot(img[:, :, chnl:(chnl + 4)], angle)
                for chnl in range(0, img.shape[2], 4)]

        return np.dstack(imgs)

    def _image_channel_rot(self, img, angle):
        if isinstance(self.angle, (int, long, float, complex)):
            angle_cur = self.angle
        else:
            angle_cur = self.rng.uniform(*self.angle)

        rows, cols = img.shape[0:2]
        cntr = tuple(self.center) if self.center is not None else (cols // 2, rows // 2)
        warp = cv2.getRotationMatrix2D(cntr, angle_cur, 1)
        rot_img = cv2.warpAffine(img, warp, (cols, rows))

        return rot_img

    @LazyInit("_lazy_init")
    def transform(self, packet, hpacket):
        """Performs rotation

        Parameters
        ----------
        packet : Packet
            expected attributes:
            img : ndarray

        Exceptions
        ----------
        ValueError - raised when input data is invalid
        AttributeError - raised when packet doesn't contain
            the required attributes

        Returns
        -------
        out : [Packet]
            List of Packet with the transformed image.
        """

        img = getattr(packet, self.attr)
        try:
            rot_img = self.rot(img, self.angle)
        except AttributeError:
            self._lazy_init()
            rot_img = self.rot(img, self.angle)

        setattr(packet, self.attr, rot_img)
        return [packet]

    def __call__(self, packet, hpacket):
        """Returns the transformed image"""
        return self.transform(packet, hpacket)


class Scale(Filter):

    """Filter performing scaling of a given image wrt center

    Parameters
    ----------
    attr : str
        Attribute name to apply scaling to.
    scale : number, tuple, list
        If number, it represents the exact scale factor.
        If tuple or list, the interval from which the scale factor is
        randomly chosen.
    seed : int
        Initializer for the random generator.

    """
    parameters = [
        FP(name="attr", default="img", value_type="str",
           help_msg="Attribute name to apply scaling to."),
        FP(name="scale", default=1.,
           help_msg="If number, it represents the exact scale factor. \
        If tuple or list, the interval from which the scale factor is \
        randomly chosen."),
        FP(name="seed", default=0,
           help_msg="Initializer for the random generator.")
    ]

    def __init__(self, attr=None, scale=1., seed=0):
        """Initializes the filter"""
        super(Scale, self).__init__()
        self.attr = attr or "img"
        self.seed = seed
        self.scale = scale
        self.rng = np.random.RandomState(seed)

    def transform(self, packet, hpacket):
        """Performs scaling

        Parameters
        ----------
        packet : Packet
            expected attributes:
            img : ndarray

        Exceptions
        ----------
        ValueError - raised when input data is invalid
        AttributeError - raised when packet doesn't contain
            the required attributes

        Returns
        -------
        out : [Packet]
                List of Packet with the transformed image.
        """
        try:
            img = getattr(packet, self.attr)

            if isinstance(self.scale, (int, long, float, complex)):
                s_factor = self.scale
            else:
                s_factor = self.rng.uniform(*self.scale)

            rows, cols = img.shape[0:2]
            warp = cv2.getRotationMatrix2D((cols // 2, rows // 2), 0, s_factor)

            setattr(packet, self.attr, cv2.warpAffine(img, warp, (cols, rows)))
            return [packet]

        except ValueError:
            self.log.exception(
                'Value error at %s filter', self.__class__)
            raise
        except AttributeError:
            self.log.exception(
                "Attribute error at %s filter", self.__class__)
            raise

    def __call__(self, packet, hpacket):
        """Returns the transformed image"""
        return self.transform(packet, hpacket)


class TransformMat(Filter):
    """Creates a 3x3 transformation matrix.

    Creates a transformation matrix (numpy array) based on given
    translation, rotation, scale, and projection parameters.
    All the parameters define the partial transform matrices, which are
    multiplied to compute the final transform matrix. These parameters
    are stored in the packet. The filter has to have access to
    the image the transform is computed for (attr_img).
    The filter can compute a transform matrix based on the args stored
    in packet's attribute attr_params. In such a case, the parameters
    are given, not randomly sampled. The rotations in such a case are
    given in radians directly.


    Parameters
    ----------
    attr : str
        attribute name to store the transformation matrix
    args : list of dicts
        list of transformations where every transform is defined as
        a dictionary

        :dict(rotate = ...):
            rotation along z axis
            - val: Clockwise rotation in degrees.
            - [val, val]: Randomly sampled rotation [from, to)

        :dict(scale = ...):
            - val: fixed isotropic (both axes scale the same) scale
            - [c,r] fixed, generally anisotropic scale of [col, row]
            - [[from,to]] randomly sampled isotropic scale
            - [[from,to], [from,to]] randomly sampled anisotropic scale

        :dict(translate = ...):
            - val fixed translation on a diagonal
            - [r,c] fixed translation
            - [[r_from, r_to], [c_from, c_to]] randomly sampled
            translation

        :dict(project = ...):
            - [rot_x, rot_y, rot_z, fov] fixed axis rotations and fov
            - [[from_rot_x, to_rot_x], rot_y, rot_z, fov] intervals or
            fixed values.

    inverse : boolean
        True returns the inverse matrix. Can be handy in a case of
        generating transform to map the target coords to source coords.
        Default is False.
    attr_img : str
        attribute name of the image
    attr_params : str
        attribute name of the sampled transform parameters
        Default is 'transform_params'.
    attr_args : str
        attribute name of the transform parameters defined in args
        Default is 'transform_args'.
    seed : int
        Random number generator seed (default 5).

    Example
    -------
    To first apply projection, scale, then rotate around img center,
    and finally translate the image one needs to construct four matrices:
        1. Projection - randomly rotate along x,y,z and sample a field of view
        2. Scale
        3. Randomly rotate
        4. Randomly translate

    >>> # Generate chessboard image pattern
    >>> img = np.kron([[1.,0.] * 4, [0.,1.] * 4] * 4, np.ones((10,10)))
    >>> tr_stack = [
            dict(project=((-15,15),(-15,15),(-45,45),(30,50))),
            dict(scale=((0.8, 1.2),)),            # Sample isotropic scale
            dict(rotate=(-25, 25)),               # Rotation along z
            dict(translate=[[-15, 15],[-15,15]])] # Translate in range
    >>> tr = ipp.filters.TransformMat( attr="transform_mat", args=tr_stack )
    >>> packet = ipp.filters.Packet(img=img)
    >>> [result_packet] = tr.transform(packet)
    >>> print(result_packet.transform_mat)

    """

    parameters = [
        FP(name="attr", value_type=str, required=False,
           default="transform_mat",
           help_msg="Attribute name to store the transformation matrix."),
        FP(name="args", value_type=collections.Iterable, required=False,
           help_msg="List of transformations."),
        FP(name="inverse", value_type=bool, required=False, default=False,
           help_msg="True returns the inverse matrix. Suitable "
           "in a case of generating transform to map the target coords "
           "to source coords."),
        FP(name="attr_img", value_type=str, required=False,
           default="img",
           help_msg="Attribute name of image to compute center of."),
        FP(name="attr_params", value_type=str, required=False,
           default="transform_params",
           help_msg="attribute name of the sampled transform parameters"),
        FP(name="attr_args", value_type=str, required=False,
           default="transform_args",
           help_msg="attribute name of the transform parameters defined "
           "in args"),
        FP(name="seed", value_type=int, required=False, default=5,
           help_msg="Random number generator seed (default 5).")
    ]

    def __init__(self, attr=None, args=None, attr_img=None, inverse=False,
                 attr_inverse="inverse_matrix", attr_params=None,
                 attr_args=None, seed=5):
        super(TransformMat, self).__init__()
        self.tr_types = ["rotate", "scale", "translate", "project"]
        self.attr = attr if attr is not None else "transform_mat"
        self.args = args
        self.inverse = inverse
        self.attr_inverse = attr_inverse
        self.attr_img = attr_img or "img"
        self.attr_params = attr_params or "transform_params"
        self.attr_args = attr_args or "transform_args"
        self.rng = np.random.RandomState(seed)

        self.transformations = []
        self.tr_params = []
        self.tr_names = []

        self.inter_args = None

    def _init_from_args(self):
        if self.args is None:
            return

        for dict_type in self.args:
            tr_name, param = list(dict_type.items())[0]
            self.tr_names.append(tr_name)
            if tr_name.lower() not in self.tr_types:
                raise ValueError

            # Rotation
            if tr_name.lower() == self.tr_types[0]:
                self.tr_params.append(np.deg2rad(param))
                if isinstance(param, (tuple, list)):
                    self.transformations.append(self._rand_rot_mat)
                else:
                    self.transformations.append(self._rot_mat)
            # Scale
            elif tr_name.lower() == self.tr_types[1]:
                # isotropic scale
                if isinstance(param, (float, int)):
                    self.tr_params.append((param, param))
                    self.transformations.append(self._scale_mat)

                # anisotropic scale
                elif isinstance(param, (list, tuple)):
                    if isinstance(param[0], (float, int)):
                        self.tr_params.append(param)
                        self.transformations.append(self._scale_mat)

                    # random (an)isotropic scale
                    elif isinstance(param[0], (list, tuple)):
                        self.tr_params.append(param)
                        self.transformations.append(self._rand_scale_mat)
                    else:
                        raise ValueError

            # Translation
            elif tr_name.lower() == self.tr_types[2]:

                # Fixed diagonal translation
                if isinstance(param, (float, int)):
                    self.tr_params.append((param, param))
                    self.transformations.append(self._translation_mat)

                # Fixed translation
                elif isinstance(param[0], (float, int)):
                    self.tr_params.append(param)
                    self.transformations.append(self._translation_mat)

                # Random translation
                elif isinstance(param[0], (tuple, list)):
                    self.tr_params.append(param)
                    self.transformations.append(self._rand_translation_mat)
                else:
                    raise ValueError

            # Projection
            elif tr_name.lower() == self.tr_types[3]:
                self.rand_rot_x = isinstance(param[0], (list, tuple))
                self.rand_rot_y = isinstance(param[1], (list, tuple))
                self.rand_rot_z = isinstance(param[2], (list, tuple))
                self.rand_fov = isinstance(param[3], (list, tuple))
                self.img = None
                params_rad = [np.deg2rad(p) for p in param[:3]]
                params_rad.append(param[-1])
                self.tr_params.append(params_rad)
                self.transformations.append(self._projection)
                # Will be used later to reduce mat4 to mat3
                self.cols_id = np.asarray([0, 1, 3])
                self.rows_id = self.cols_id[:, np.newaxis]

        self.inter_args = [{list(tr_type.keys())[0].lower(): tr_param} for
                           tr_type, tr_param in zip(self.args, self.tr_params)]

    def _init_from_packet_params(self, params):
        self.transformations = []
        self.tr_params = []
        self.tr_names = []

        for dict_type in params:
            tr_name, param = list(dict_type.items())[0]
            self.tr_names.append(tr_name)
            if tr_name.lower() not in self.tr_types:
                raise ValueError

            # Rotation
            if tr_name == self.tr_types[0]:
                self.tr_params.append(param)
                self.transformations.append(self._rot_mat)

            # Scale
            elif tr_name == self.tr_types[1]:
                # isotropic scale
                self.tr_params.append(param)
                self.transformations.append(self._scale_mat)

            # Translation
            elif tr_name == self.tr_types[2]:
                self.tr_params.append(param)
                self.transformations.append(self._translation_mat)

            # Projection
            elif tr_name.lower() == self.tr_types[3]:
                self.rand_rot_x = False
                self.rand_rot_y = False
                self.rand_rot_z = False
                self.rand_fov = False
                self.img = None
                self.tr_params.append(param)
                self.transformations.append(self._projection)
                # Will be used later to reduce mat4 to mat3
                self.cols_id = np.asarray([0, 1, 3])
                self.rows_id = self.cols_id[:, np.newaxis]

    def _rot_mat(self, theta):
        cos, sin = np.cos(theta), np.sin(theta)
        return np.array([[cos, -sin, 0], [sin, cos, 0], [0, 0, 1]]), theta

    def _rand_rot_mat(self, intv_theta):
        theta = self.rng.uniform(*intv_theta)
        return self._rot_mat(theta)

    def _scale_mat(self, scale):
        return np.array([[scale[0], 0, 0], [0, scale[1], 0], [0, 0, 1]]), scale

    def _rand_scale_mat(self, intv_scale):
        if len(intv_scale) == 1:
            scale_x = self.rng.uniform(*intv_scale[0])
            scale_y = scale_x
        else:
            scale_x = self.rng.uniform(*intv_scale[0])
            scale_y = self.rng.uniform(*intv_scale[1])

        return self._scale_mat((scale_x, scale_y))

    def _translation_mat(self, translation):
        return np.array([[1, 0, translation[0]],
                         [0, 1, translation[1]],
                         [0, 0, 1]]), translation

    def _rand_translation_mat(self, intv_translation):
        x_translation = self.rng.uniform(*intv_translation[0])
        y_translation = self.rng.uniform(*intv_translation[1])
        return self._translation_mat((x_translation, y_translation))

    def _rot_mat_z(self, theta):
        cos, sin = np.cos(theta), np.sin(theta)
        return np.array([[cos, -sin, 0, 0],
                         [sin, cos, 0, 0],
                         [0, 0, 1, 0],
                         [0, 0, 0, 1]]), theta

    def _rot_mat_y(self, theta):
        cos, sin = np.cos(theta), np.sin(theta)
        return np.array([[cos, 0, sin, 0],
                         [0, 1, 0, 0],
                         [-sin, 0, cos, 0],
                         [0, 0, 0, 1]]), theta

    def _rot_mat_x(self, theta):
        cos, sin = np.cos(theta), np.sin(theta)
        return np.array([[1, 0, 0, 0],
                         [0, cos, -sin, 0],
                         [0, sin, cos, 0],
                         [0, 0, 0, 1]]), theta

    def _projection(self, vec_param):
        """Compute the projection matrix based on angles and FoV

        Current implementation expects the image center is in [0,0].
        """
        max_side = max(self.img.shape[:2])
        # img_center = [side / 2 for side in self.img.shape[:2]]

        theta_x, theta_y, theta_z, fov = vec_param

        if self.rand_fov:
            fov = self.rng.uniform(*fov)

        distance = (max_side / 2.) / np.tan(np.deg2rad(fov / 2.))

        # translate = np.asarray(
        #     [[1, 0, 0, -img_center[1]],
        #      [0, 1, 0, -img_center[0]],
        #      [0, 0, 1, 0],
        #      [0, 0, 0, 1]])

        if self.rand_rot_x:
            theta_x = self.rng.uniform(*vec_param[0])
        if self.rand_rot_y:
            theta_y = self.rng.uniform(*vec_param[1])
        if self.rand_rot_z:
            theta_z = self.rng.uniform(*vec_param[2])

        rot_x, _ = self._rot_mat_x(theta_x)
        rot_y, _ = self._rot_mat_y(theta_y)
        rot_z, _ = self._rot_mat_z(theta_z)

        rot = rot_z.dot(rot_y.dot(rot_x))

        view = np.asarray(
            [[1, 0, 0, 0],
             [0, 1, 0, 0],
             [0, 0, 1, distance],
             [0, 0, 0, 1]])

        # Include the principal point of camera, which in case of opencv
        # has to be moved from [0,0] to the image center
        # camera = np.asarray(
        #     [[distance, 0, img_center[1], 0],
        #      [0, distance, img_center[0], 0],
        #      [0, 0, distance, 0],
        #      [0, 0, 1., 0]])

        camera = np.asarray(
            [[distance, 0, 0, 0],
             [0, distance, 0, 0],
             [0, 0, distance, 0],
             [0, 0, 1, 0]])

        # mat4 = camera.dot(view.dot(rot.dot(translate)))
        mat4 = camera.dot(view.dot(rot))

        # reduce_col = np.asarray([[1, 0, 0], [0, 1, 0], [0, 0, 0], [0, 0, 1]])
        # reduce_row = reduce_col.transpose([1, 0])
        # mat3 = reduce_row.dot(mat4.dot(reduce_col))

        mat3 = mat4[self.rows_id, self.cols_id]

        return mat3, (theta_x, theta_y, theta_z, fov)

    @LazyInit("_init_from_args")
    def transform(self, packet, hpacket=None):
        """Compute the tranform matrix"""

        if self.args is None:
            self.inter_args = getattr(packet, self.attr_args)
            params = getattr(packet, self.attr_params)
            self._init_from_packet_params(params)

        self.img = getattr(packet, self.attr_img)
        img_center = [side / 2. for side in self.img.shape[:2]]

        # Pivot is always in the image center
        to_center_coordinates = np.asarray(
            [[1, 0, -img_center[1]],
             [0, 1, -img_center[0]],
             [0, 0, 1]])

        to_cv_coordinates = np.asarray(
            [[1, 0, img_center[1]],
             [0, 1, img_center[0]],
             [0, 0, 1]])

        # matrix = np.diag(np.ones(3)) to_center_coordinates
        matrix = to_center_coordinates

        # The filter arguments are multiplied from left
        params = []
        for tr_mat, tr_param, key in zip(self.transformations,
                                         self.tr_params, self.tr_names):
            mat, param = tr_mat(tr_param)
            matrix = np.dot(mat, matrix)

            params.append({key: param})

        matrix = to_cv_coordinates.dot(matrix)

        if self.inverse:
            matrix = np.linalg.pinv(matrix)

        # Normalize the transform matrix the have the last element 1
        matrix /= matrix[2, 2]

        setattr(packet, self.attr_params, params)
        setattr(packet, self.attr_args, self.inter_args)
        setattr(packet, self.attr, matrix)
        setattr(packet, self.attr_inverse, self.inverse)

        return [packet]

    def __call__(self, packet, hpacket=None):
        """Runs transform method"""
        return self.transform(packet, hpacket=None)


class Translate(Filter):

    """Filter performing translation of a given image

    Parameters
    ----------
    attr : str
        Attribute name to be shifted.
    shift_x, shift_y : numeric, tuple, list
        If number, it represents the exact shift.
        If tuple or list, the interval from which the shift is
        randomly chosen.
    seed : int
        Random number generator seed.


    """
    parameters = [
        FP(name="attr", default="img", value_type=str,
           help_msg="Attribute name to be shifted."),
        FP(name="shift_x", default=0,
           help_msg="If number, it represents the exact shift in x direction. \
           If tuple or list, the interval from which the shift in x direction is \
           randomly chosen."),
        FP(name="shift_y", default=0,
           help_msg="If number, it represents the exact shift in y direction. \
           If tuple or list, the interval from which the shift in y direction is \
           randomly chosen."),
        FP(name="seed", default=0, value_type=int,
           help_msg="Random number generator seed.")
    ]

    def __init__(self,
                 attr=None,
                 shift_x=0,
                 shift_y=0,
                 seed=0):
        """Initializes the filter"""

        super(Translate, self).__init__()
        self.attr = attr if attr is not None else "img"
        self.seed = seed
        self.shift_x = shift_x
        self.shift_y = shift_y
        self.rng = np.random.RandomState(seed)

    def transform(self, packet, hpacket):
        """Performs translation

        Parameters
        ----------
        packet : Packet
            expected attributes:
            img : ndarray

        Exceptions
        ----------
        ValueError - raised when input data is invalid
        AttributeError - raised when packet doesn't contain
            the required attributes

        Returns
        -------
        out : [Packet]
                List of Packet with the transformed image.
        """
        try:
            img = getattr(packet, self.attr)

            if isinstance(self.shift_x, (int, long, float, complex)):
                x_shift = self.shift_x
            else:
                x_shift = self.rng.uniform(*self.shift_x)

            if isinstance(self.shift_y, (int, long, float, complex)):
                y_shift = self.shift_y
            else:
                y_shift = self.rng.uniform(*self.shift_y)

            rows, cols = img.shape[0:2]
            warp = np.float32([[1, 0, x_shift], [0, 1, y_shift]])
            setattr(packet, self.attr, cv2.warpAffine(img, warp, (cols, rows)))
            return [packet]

        except ValueError:
            self.log.exception(
                'Value error at %s filter', self.__class__)
            raise
        except AttributeError:
            self.log.exception(
                "Attribute error at %s filter", self.__class__)
            raise

    def __call__(self, packet, hpacket):
        """Returns the transformed image"""
        return self.transform(packet, hpacket)


class WarpImg(Filter):
    """
    Warps image based on given 3x3 transform matrix defined in packet.

    Filter to apply the image transformation based on the matrix of
    TransformMat filter.

    Parameters
    ----------
    attr : str
        Attribute name holding the image.
    attr_mat : str
        Attribute name holding the transform matrix.
    attr_out : str
        Saves warped image to packet's attr_out. Default is the same
        as attr.
    border_type : str
        cv2 border type name "BORDER_CONSTANT, BORDER_REPLICATE,..."
        Default is BORDER_CONSTANT
    border_val : int, float, tuple, list
        Border value used in case of BORDER_CONSTANT border type.
        Default is 0.
    flags : list, tuple of str
        Combination of interpolation methods (INTER_LINEAR or
        INTER_NEAREST) and the optional flag WARP_INVERSE_MAP, that
        sets M as the inverse transformation dst -> src ).
        WARP_INVERSE_MAP compute the inverse of transform matrix
        internally.
        Defaul is (INTER_LINEAR,)

    Returns
    -------
    out : [Packet]
            List of Packet with the transformed image.

    """

    parameters = [
        FP(name="attr", default="img", value_type=str,
           help_msg="Attribute name holding the image."),
        FP(name="attr_mat", default="transform_mat", value_type=tuple,
           help_msg="Attribute name holding the transform matrix."),
        FP(name="attr_out", default="img", value_type=str, required=False,
           help_msg="Saves warped image to packet's attr_out. Default is the \
           same as attr."),
        FP(name="border_type", value_type=str, required=False,
           default="BORDER_CONSTANT",
           help_msg="cv2 border type name BORDER_CONSTANT, \
           BORDER_REPLICATE, ..."),
        FP(name="border_val", required=False,
           default=0, help_msg="Border value used in case of BORDER_CONSTANT \
           border type."),
        FP(name="flags", required=False,
           default="[INTER_LINEAR]",
           help_msg="Combination of interpolation methods (INTER_LINEAR or "
           "INTER_NEAREST) and the optional flag WARP_INVERSE_MAP, that "
           "sets M as the inverse transformation dst -> src ). ")
    ]

    def __init__(self, attr=None, attr_mat=None, attr_out=None,
                 border_type="BORDER_CONSTANT", border_val=0,
                 flags=("INTER_LINEAR",)):
        super(WarpImg, self).__init__()
        self.attr = attr or "img"
        self.attr_mat = attr_mat or "transform_mat"
        self.attr_out = attr_out or self.attr
        self.border_type = border_type or "BORDER_CONSTANT"
        self.border_val = border_val
        self.border = getattr(cv2, self.border_type)
        self.flags = np.bitwise_or.reduce(
            [getattr(cv2, flag) for flag in flags])

    def warp(self, packet, hpacket=None):
        """Transforma an image based on matrix and cv2.warPerspective"""
        img = getattr(packet, self.attr)
        mat = getattr(packet, self.attr_mat)

        tr_img = cv2.warpPerspective(
            img.astype(np.float32), mat, (img.shape[1], img.shape[0]),
            flags=self.flags, borderMode=self.border,
            borderValue=self.border_val)

        setattr(packet, self.attr_out, tr_img)

        return [packet]

    def __call__(self, packet, hpacket=None):
        """Runs warp method"""
        return self.warp(packet, hpacket=None)


class Align(Filter):

    """Applies transformation to align an image with another

    Parameters
    ----------
    attr_src : str
        Attribute name of the image which is to be aligned.
    attr_trg : str
        Attribute name of the image the source is aligned with.
    attr_out : str
        Attribute name of the resulting image.
        If not set, the source image is rewritten.
    alignment_strategy : str
        Alignment strategy.
        Possibilities: 'affine', 'rigid', 'perspective'.
    detection_strategy: str
        Feature detection strategy.
        Possibilities: 'descriptor', 'optical_flow'
    settings : dict
        Dictionary of settings of the alignment. Content varies based on
        the chosen strategy.
    """
    parameters = [
        FP(name="attr_src", value_type=str, required=True, default="img_1",
           help_msg="Attribute name of the image, which is aligned."),
        FP(name="attr_trg", value_type=str, required=True, default="img_2",
           help_msg="Attribute name of the image the source is aligned with."),
        FP(name="attr_out", value_type=str, required=False, default=None,
           help_msg="Attribute name of the resulting image. If not set, the source image is rewritten."),
        FP(name="alignment_strategy", value_type=str, required=False, default="rigid",
           help_msg="Alignment strategy. Possibilities: 'affine', 'rigid', 'perspective'"),
        FP(name="detection_strategy", value_type=str, required=False, default="optical_flow",
           help_msg="Feature detection strategy. Possibilities: 'descriptor', 'optical_flow'"),
        FP(name="settings", value_type=dict, required=False, default={},
           help_msg="Dictionary of settings of the alignment. Content varies based on "
                    "the chosen strategies."),
        FP(name="reuse", value_type=bool, required=False, default=True,
           help_msg="Whether the transformation matrix should be remembered and reused"
                    "for the same keypoints.")
    ]

    def __init__(self, attr_src, attr_trg, attr_out=None,
                 alignment_strategy='rigid', detection_strategy='optical_flow',
                 settings={}, reuse=True):
        """Align initialization"""

        super(Align, self).__init__()
        self.attr_source = attr_src
        self.attr_target = attr_trg
        self.attr_out = attr_out or attr_src

        if alignment_strategy not in ['affine', 'rigid', 'perspective']:
            self.log.exception('Value error at %s filter', self.__class__)
            raise ValueError('Unknown alignment strategy: "{}"!'.format(alignment_strategy))
        if detection_strategy not in ['descriptor', 'optical_flow']:
            self.log.exception('Value error at %s filter', self.__class__)
            raise ValueError('Unknown detection strategy: "{}"!'.format(detection_strategy))

        self.detection_strategy = detection_strategy
        self.alignment_strategy = alignment_strategy
        self.settings = settings

        self.reuse = reuse
        if self.reuse:
            self.tm_lookup = {}

    def _get_transform_matrix(self, src_pts, dst_pts):
        if self.reuse:
            key = str(src_pts + dst_pts)
            if key in self.tm_lookup:
                return self.tm_lookup[key]

        matrix = None
        if self.alignment_strategy == 'perspective':
            if len(src_pts) < 4: return None
            matrix, _ = cv2.findHomography(np.array(src_pts), np.array(dst_pts), method=cv2.RANSAC)
        elif self.alignment_strategy == 'affine':
            if len(src_pts) < 3: return None
            matrix = cv2.getAffineTransform(np.array(src_pts[:3], dtype=np.float32),
                                            np.array(src_pts[:3], dtype=np.float32))
        else:
            matrix = cv2.estimateRigidTransform(np.array(src_pts, dtype=np.uint8),
                                                np.array(dst_pts, dtype=np.uint8), False)

        if self.reuse:
            self.tm_lookup[key] = matrix

        return matrix

    def _transform(self, source, target, src_pts, dst_pts):
        assert len(src_pts) == len(dst_pts) and len(src_pts)

        tm = self._get_transform_matrix(src_pts, dst_pts)
        if tm is None:
            return source

        if self.alignment_strategy == 'perspective':
            return cv2.warpPerspective(source, tm, (target.shape[1], target.shape[0]))
        else:
            return cv2.warpAffine(source, tm, (target.shape[1], target.shape[0]))

    @staticmethod
    def _instantiate_descriptor(name):
        name = name.upper()
        if name == 'SURF':
            return cv2.xfeatures2d.SURF_create()
        if name == 'SIFT':
            return cv2.xfeatures2d.SIFT_create()
        if name == 'ORB':
            return cv2.ORB_create()
        if name == 'KAZE':
            return cv2.KAZE_create()
        if name == 'BRISK':
            cv2.BRISK_create()
        return cv2.AKAZE_create()

    def _align_by_descriptor(self, source, target):
        "Alignes the images based on the keypoints from the selected descriptor"

        alg = self._instantiate_descriptor(self.settings.get('descriptor_type', 'AKAZE'))
        kp1, desc = alg.detectAndCompute(source, None)
        kp2, desc2 = alg.detectAndCompute(target, None)

        # TODO matcher settings
        bf = cv2.BFMatcher()
        matches = bf.match(desc, desc2)
        if not len(matches):
            return source
        matches = sorted(matches, key=lambda x: x.distance)

        src, dst = [], []
        for match in matches:
            src.append(kp1[match.queryIdx].pt)
            dst.append(kp2[match.trainIdx].pt)

        return self._transform(source, target, src, dst)

    def _align_by_optical_flow(self, source, target):
        "Aligns the images based on the Harris corners of the source image and the corresponding \
        points of the second"

        features = cv2.goodFeaturesToTrack(source,
                                           self.settings.get('maxCorners', 100),
                                           self.settings.get('qualityLevel', .01),
                                           self.settings.get('minDistance', 10)
        )
        nextPoints, statuses, errors = cv2.calcOpticalFlowPyrLK(source.astype(np.uint8),
                                                                target.astype(np.uint8),
                                                                features, None)

        points_s, points_e = [], []
        for idx, status in enumerate(statuses):
            if not status: continue
            points_s.append(list(features[idx][0]))
            points_e.append(list(nextPoints[idx][0]))
        if not len(points_s): return source

        return self._transform(source, target, points_s, points_e)

    def align(self, packet, hpacket):
        """
        Parameters
        ----------
        packet : Packet
            Packet containing the the two images
        hpacket : Packet
            not used

        Returns
        -------
        out : [Packet]
            List of packet with the aligned image.
        """

        try:
            source = getattr(packet, self.attr_source)
            target = getattr(packet, self.attr_target)

            try:
                if self.detection_strategy == "descriptor":
                    img = self._align_by_descriptor(source, target)
                elif self.detection_strategy == "optical_flow":
                    img = self._align_by_optical_flow(source, target)
            except Exception as exc:
                self.log.exception(
                    'Alignment exception at %s filter: %s!',
                    self.__class__, exc.message)
                img = source

            setattr(packet, self.attr_out, img)
            return [packet]

        except ValueError:
            self.log.exception(
                'Value error at %s filter', self.__class__)
            raise
        except AttributeError:
            self.log.exception(
                "Attribute error at %s filter", self.__class__)
            raise

    def __call__(self, packet, hpacket):
        return self.align(packet, hpacket)


class PtsAugmentation(Filter):
    """ Performs random augmentation of interesting points

    Parameters
    ----------
    packet : Packet
        expected attributes:
        source_pts : array of arrays

    Returns
    -------
    out : [Packet]
            Resulting list of Packets.
    """
    parameters = [
        FP(name="num_pixels", required=False,
           help_msg="maximum number of pixels the interesting point can \
	                 be randomly moved"),
    ]

    def __init__(self, num_pixels=5):
        super(PtsAugmentation, self).__init__()
        self.num_pixels = num_pixels

    def __call__(self, packet, hpacket=None):
        """Returs the packet with random augmentation of
        parsed interesting points"""
        src_pts = list(packet.source_pts)
        src_pts_np = np.array(src_pts)
        rand_mat = np.random.randint(
            low=-self.num_pixels, high=self.num_pixels, size=src_pts_np.shape)
        packet.source_pts = (src_pts_np + rand_mat).tolist()

        return [packet]
