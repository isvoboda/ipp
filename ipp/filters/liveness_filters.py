# coding=utf-8
"""Utility filters for liveness evaluation"""

from __future__ import division
from __future__ import print_function

from .filter_factory import Filter


__all__ = [
    "ToLivenessPrediction",
    "ToLivenessPredictionEyes",
    "ToLivenessGt"
]


class ToLivenessPrediction(Filter):

    """Converts corresponding attributes to liveness prediction"""

    def __init__(self):
        """ToLivenessPrediction constructor"""

        super(ToLivenessPrediction, self).__init__()
        self.truth_mapping = { 0: [7, 0, 1], 1: [0, 1, 2], 2: [1, 2, 3], 3: [2, 3, 4],
                               4: [3, 4, 5], 5: [4, 5, 6], 6: [5, 6, 7], 7: [6, 7, 0], 8: [8] }

    def _compute_score(self, probs, predicted, gt):
        """Computes liveness score based on the prediction and ground truth"""

        if predicted not in self.truth_mapping[gt]:
            return 0
        nidx = list(set(range(9)) - set(self.truth_mapping[gt]))
        return sum(probs[i] for i in self.truth_mapping[gt]) - sum(probs[i] for i in nidx)

    def convert(self, packet, hpacket):
        """Converts the liveness params to score prediction

        Parameters
        ----------
        packet : Packet
        hpacket: not used

        Returns
        -------
        out : [Packet]
              Packet with parameter 'liveness_score' param
        """

        try:
            p = getattr(packet, "predicted")
            gt = getattr(packet, "direction")
            probs = getattr(packet, "probs")
            score = self._compute_score(probs, p, gt)
        except AttributeError:
            self.log.exception("Attribute error at %s filter", self.__class__)
            raise

        setattr(packet, "liveness_score", score)
        return [packet]

    def __call__(self, packet, hpacket):
        return self.convert(packet, hpacket)


class ToLivenessPredictionEyes(ToLivenessPrediction):

    """Converts corresponding attributes to liveness prediction"""

    def __init__(self):
        super(ToLivenessPredictionEyes, self).__init__()

    def convert(self, packet, hpacket):
        """Converts the liveness params to score prediction

        Parameters
        ----------
        packet : Packet
        hpacket: not used

        Returns
        -------
        out : [Packet]
              Packet with parameter 'liveness_score' param
        """

        try:
            gt = getattr(packet, "direction")
            scores = []
            for ptype in ['left', 'right']:
                p = getattr(packet, 'predicted_{}'.format(ptype))
                probs = getattr(packet, 'probs_{}'.format(ptype))
                scores.append(self._compute_score(probs, p, gt))
            score = max(scores)
        except AttributeError:
            self.log.exception("Attribute error at %s filter", self.__class__)
            raise

        setattr(packet, "liveness_score", score)
        return [packet]

    def __call__(self, packet, hpacket):
        return self.convert(packet, hpacket)


class ToLivenessGt(Filter):

    """Converts corresponding attributes to liveness ground truth"""

    def __init__(self):
        """ToLivenessPrediction constructor"""

        super(ToLivenessGt, self).__init__()

    def convert(self, packet, hpacket):
        """Converts the liveness params to ground truth

        Parameters
        ----------
        packet : Packet
        hpacket: not used

        Returns
        -------
        out : [Packet]
              Packet with parameter 'liveness_gt' param
        """

        try:
            direction = getattr(packet, "direction")
            gt_direction = getattr(packet, "gt_direction")
            gt_liveness = int(direction == gt_direction)
        except AttributeError:
            self.log.exception("Attribute error at %s filter", self.__class__)
            raise

        setattr(packet, "liveness_gt", gt_liveness)
        return [packet]

    def __call__(self, packet, hpacket):
        return self.convert(packet, hpacket)