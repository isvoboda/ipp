# coding=utf-8
"""Image writer module based on OpenCv imwrite"""

import os

import cv2

from ..utils import LazyInit
from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP

__all__ = [
    "ImageWriter"
]


class ImageWriter(Filter):

    """
    Writes several types of images via OpenCV.

    Only 8-bit (or 16-bit unsigned (CV_16U) in case of PNG, JPEG 2000,
    and TIFF) single-channel or 3-channel (with BGR channel order)
    images can be saved using this function.

    Parameters
    ----------
    img_attr: str
        Name of packet's image attribute.
    name : str
        Fixed image name.
    name_attr: str
        Packet's attribute holding the image name.
    path : str
        Fixed path to store the image.
    path_attr : str
        Packet's attribute with path.
    filetype : str
        Image type to be stored, ie. "jpg", "png" etc. OpenCv based.
    multiple_cnt : str
        If this parameter is set, it enables to save multiple
    images with the same name or name_attr setting but with
        different numeric suffix. Suffix is incremented after each save
    until this cnt. Later, the images are overwritten.

    Input
    -----
    packet.img : numpy.array
        Image to be written.

    """
    parameters = [
        FP(name="img_attr", required=False,
           help_msg="The packet's image attribute."),
        FP(name="name", required=False,
           help_msg="Fixed image name."),
        FP(name="name_attr", required=False,
           help_msg="Packet's attribute to be used as the image name."),
        FP(name="path", required=False, help_msg="Fixed path to store the \
           image."),
        FP(name="path_attr", required=False,
           help_msg="Packet's attribute with path."),
        FP(name="filetype", required=False,
           help_msg="Image type to be stored, ie. 'jpg', 'png' etc. \
           OpenCv based."),
        FP(name="multiple_cnt", required=False,
           help_msg="If this parameter is set, it enables to save multiple \
           images with the same name or name_attr setting but with \
           different numeric suffix. Suffix is incremented after each save \
           until this cnt. Later, the images are overwritten.")
    ]

    def __init__(self, img_attr=None, name=None, name_attr=None, path=None,
                 path_attr=None, filetype=None, multiple_cnt=None):
        """ Initialize image reader """

        super(ImageWriter, self).__init__()
        self.img_attr = img_attr or "img"
        self.name = name
        self.name_attr = name_attr
        self.path = path
        self.path_attr = path_attr
        self.filetype = filetype or "jpg"

        self.suffix_counter = 0
        self.multiple_cnt = multiple_cnt

    def _lazy_init(self):
        if self.name is not None:
            self.get_name = lambda x: self.name
        elif self.name_attr is not None:
            self.get_name = lambda x: getattr(x, self.name_attr)
        else:
            raise ValueError("Name or name_attr missing.")

        if self.path is not None:
            self.get_path = lambda x: self.path
        elif self.path_attr is not None:
            self.get_path = lambda x: getattr(x, self.path_attr)
        else:
            self.get_path = lambda x: "."

    @LazyInit("_lazy_init")
    def write(self, packet):
        """Writes an image stored in packet

        Parameters
        ----------
        packet: Packet
            expects image containing attribute

        Returns
        -------
        out : [Packet]
            List of Packets
        """
        try:
            img = getattr(packet, self.img_attr)
            name = self.get_name(packet)

            if self.multiple_cnt is not None:
                self.suffix_counter = (
                    self.suffix_counter + 1) % self.multiple_cnt
                name = "{}_{}".format(name, self.suffix_counter)

            filename = ".".join([name, self.filetype])
            path_filename = os.path.join(self.get_path(packet), filename)
            dir_path = os.path.dirname(path_filename)

            self.log.debug("Writing %s, shape %s, type %s",
                           path_filename, img.shape, img.dtype)

            if not os.path.exists(dir_path):
                self.log.info("Creating %s directory", dir_path)
                os.makedirs(dir_path)

            if cv2.imwrite(path_filename, img) is False:
                self.log.info("Failed to write file %s", path_filename)

            return [packet]

        except cv2.error:
            self.log.exception("OpenCv")
            raise
        except IOError:
            self.log.exception("Error writing %r", path_filename)
            raise
        except AttributeError:
            self.log.exception("Attribute error")
            raise

    def __call__(self, packet=None, hpacket=None):
        """
        Returns the image
        """
        return self.write(packet)
