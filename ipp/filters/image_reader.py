# coding=utf-8
"""Image reader based on OpenCV imread"""

# This may produce problems, when default Packet's attribute is defined
# as a string. In python 2, all identifiers have to be ascii
# letters, numbers and several dash like symbols. This, actually, make
# the identifier name an unicode object, which is automatically bytes -
# ascii encoded (in python 2).
# Packet`s __str__ method has to take this into account.

from __future__ import division, unicode_literals

import time
from collections import OrderedDict

import cv2
import numpy as np

from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP
from .filters import ContinueIteration, Packet
from .txt_reader import CSVReader

__all__ = [
    "ImageReader",
    "ImageSampler",
    "ImageGenerator",
    "RoadImageSampler"
]


class ImageReader(Filter):
    """
    Reads several types of images via OpenCV.

    Parameters
    ----------
    path_attr: str
        packet attribute name to look for the image path
    img_attr: str
        Image attribute name
    flag : str
        OpenCv imread flags 'IMREAD_COLOR', 'IMREAD_GRAYSCALE',
        'IMREAD_UNCHANGED'.
    buffer : int
        ImageReader buffer size to store last read image.
        Default is 0.
    multiple : bool
        Flag whether a sequence of images (indexed from 1) or
        a single image should be read.

    Output
    ------
    packet.img: ndarray
        numpy array representation of image

    Throws
    ------
    inno.ipp.filters.filters.ContinueIteration
        Whether image path does not exist.

    """
    parameters = [
        FP(name="path_attr", value_type=str, required=False,
           help_msg="Attribute name storing the image path. Default is \
           'path'.", default="path"),
        FP(name="img_attr", value_type=str, required=False,
           help_msg="Attribute name the loaded image is stored to.",
           default="img"),
        FP(name="flag", required=False,
           default="IMREAD_COLOR", value_type=str,
           help_msg="OpenCv imread flags 'IMREAD_COLOR', 'IMREAD_GRAYSCALE', \
           'IMREAD_UNCHANGED'"),
        FP(name="buf_size", value_type=int, default=0, required=False,
           help_msg="ImageReader buffer size to store last read image."),
        FP(name="multiple", value=bool, default=False, required=False,
           help_msg="Flag whether a sequence of images (indexed from 1) or \
           a single image should be read.")
    ]

    def __init__(self, path_attr=None, img_attr=None, flag=None, buf_size=0,
                 multiple=False):
        """Initialize the image reader"""
        super(ImageReader, self).__init__()
        self.path_attr = path_attr or "path"
        self.img_attr = img_attr or "img"
        self.flag = flag or "IMREAD_COLOR"
        self.imread_flag = getattr(cv2, self.flag)
        self.buf_size = buf_size
        if self.buf_size > 0:
            self.buffer = OrderedDict()
        self.multiple = multiple

    def _read_image(self, path):
        """Returns image at a given path"""
        try:
            with open(path, "rb") as stream:
                if stream is None:
                    self.log.warn("Unable to load image: %s", path)
                    raise ContinueIteration
                bytes_img = bytearray(stream.read())
                array = np.asarray(bytes_img, dtype=np.uint8)
                img = cv2.imdecode(array, self.imread_flag)
        except IOError:
            self.log.warn("IO error loading image at: %s", path)
            raise ContinueIteration

        return img.astype(np.float32)

    def _read(self, path):
        try:
            # Do we use the buffer?
            if self.buf_size > 0:
                value = self.buffer.pop(path, None)

                # Path not in buffer, read new image
                if value is None:
                    # Buffer full? Remove last added item
                    if len(self.buffer) >= self.buf_size:
                        self.buffer.popitem()

                    img = self._read_image(path)

                    # Put copy into buffer
                    self.buffer[path] = np.copy(img)

                # Path in buffer, reuse it and  shift the item to beginning
                else:
                    img = np.copy(value)
                    self.buffer[path] = value
            # We don't use the buffer
            else:
                img = self._read_image(path)

            return img

        except cv2.error:
            self.log.exception("OpenCv error.")
            raise
        except IOError:
            self.log.exception("Error reading %s", path)
            raise

    def read(self, packet, hpacket=None):
        """Reads an image based on the user given packet's
        attribute name

        Returns
        -------
        out : [Packet]
            List of Packet with an img (ndarray) attribute.
        """
        try:
            if self.multiple:
                idx = 1
                while True:
                    c_path = self.path_attr + str(idx)
                    if not hasattr(packet, c_path):
                        break
                    path = getattr(packet, c_path)
                    setattr(packet, self.img_attr + str(idx), self._read(path))
                    idx += 1
            else:
                path = getattr(packet, self.path_attr)
                setattr(packet, self.img_attr, self._read(path))
        except AttributeError:
            self.log.exception("Attribute error")
            raise
        return [packet]

    def __call__(self, packet=None, hpacket=None):
        """
        Returns the image
        """
        return self.read(packet)


class ImageSampler(ImageReader):
    """Sample the image parts"""
    def __init__(self, path=None, sep=None, id_attr=None, path_attr=None,
                 img_attr=None, flag=None, buf_size=1, sample_shape=(224, 224),
                 stride=None, copy=True, sample_random=False, seed=5,
                 multiple=False, start_offset=(0, 0)):

        super(ImageSampler, self).__init__(
            path_attr, img_attr, flag, buf_size, multiple)
        self.id_attr = id_attr or "id"
        self.sample_shape = np.array(sample_shape)
        self.offsets = self.sample_shape // 2
        self.stride = np.array(stride)
        self.start_offset = np.array(start_offset)
        self.copy = copy
        self.sample_random = sample_random
        self.rng = np.random.RandomState(seed)

        self.csv_reader = CSVReader(path=path, sep=sep, seed=seed)

        self.sample_coords = None
        self.sample_iter = None
        self.sample_img = None
        self.sample_img_path = None

        self._id = 0

    def _init_sampling(self, img):
        img_shape = img.shape[0:2]

        # Check the sample shape is not bigger than source image
        if any([dim[0]+dim[2] > dim[1] for dim
                in zip(self.sample_shape, img_shape, self.start_offset)]):
            raise ValueError("Sample shape bigger compared to sampled image.")

        offset_begin = self.offsets
        offset_end = [dim_img - dim_offset
                      for dim_img, dim_offset in zip(img_shape, offset_begin)]

        ranges = zip(offset_begin+self.start_offset, offset_end, self.stride)
        indices = [range(start, stop, step) for start, stop, step in ranges]
        self.sample_coords = [np.array([y, x])
                              for y in indices[0] for x in indices[1]]

        self.sample_iter = iter(self.sample_coords)

    def _sample(self, img, sample_coord):
        """Returns sample"""
        begin = sample_coord - self.offsets
        end = begin + self.sample_shape
        sample = img[begin[0]:end[0], begin[1]:end[1]]
        # cv2.putText(sample, u"{: 6}".format(self._id), (0, sample.shape[1]),
        #             fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=0.5,
        #             color=(255, 255, 255))
        if self.copy:
            return np.copy(sample)

        return sample

    def _read_img(self):
        """Returns tuple of image and its path """
        [packet] = self.csv_reader()
        path = getattr(packet, self.path_attr)
        self.sample_img_path = path
        return super(ImageSampler, self)._read(path)

    def _next_sample(self):
        """Returns next sample"""
        if self.sample_img is None:
            self.sample_img = self._read_img()
            self._init_sampling(self.sample_img)
            return None, None

        sample_coord = next(self.sample_iter, None)
        if sample_coord is None:
            # store_path = ".".join([self.sample_img_path, u"marked.png"])
            # cv2.imwrite(store_path.encode("utf-8"), self.sample_img)
            self.sample_img = None
            return None, None

        sample = self._sample(self.sample_img, sample_coord)
        return sample, sample_coord

    def sample(self, packet=None):
        """Returns sample"""
        sample, sample_center = self._next_sample()
        while sample is None:
            sample, sample_center = self._next_sample()

        # begin = sample_center - self.offsets
        # end = begin + self.sample_shape
        # img = self.sample_img.copy()
        # cv2.rectangle(img, tuple([ begin[1], begin[0]]), tuple([end[1], end[0]]), color=(0, 255, 0), thickness=1)
        # cv2.namedWindow("Preview", cv2.WINDOW_NORMAL)
        # cv2.imshow("Preview", img/255.)
        # cv2.waitKey(0)

        # sample = self.sample_img[begin[0]:end[0], begin[1]:end[1]]

        packet = Packet()

        packet.sample_shape = self.sample_shape
        packet.sample_center = sample_center
        packet.path = self.sample_img_path
        setattr(packet, self.img_attr, sample)
        setattr(packet, self.id_attr, self._id)
        self._id += 1

        return [packet]

    def __call__(self, packet=None, hpacket=None):
        return self.sample()


class ImageGenerator(Filter):
    """Generate numpy ndarray

    Generate image-like data. Chessboard, randomly colored image, or
    constant color image is generated where the default is chessboard.

    Parameters
    ----------
    attr : str
    shape : tuple
        image shape
    dtype : str
    itype : str
        image type with possible values
        - chessboard
        - random
        - constant
    seed : int
        Random number generator seed.
    itype_kwargs : dict
        additional parameters related to itype image
        - color : tuple, list
            defined constant color
    """

    def __init__(self, attr="img", shape=(2048, 2048, 3), dtype="float64",
                 itype="chessboard", seed=5, **itype_kwargs):
        super(ImageGenerator, self).__init__()
        self.attr = attr
        self.shape = shape
        self.dtype = dtype
        self.seed = seed
        self.color = 0.5 if len(shape) == 2 else (0.5, 0.5, 0.5)
        kwarg_keys = ["color"]
        self.__dict__.update((k, v) for k, v in itype_kwargs.items()
                             if k in kwarg_keys)

        self.rng = np.random.RandomState(seed)
        self.itype = itype
        self.img_types = {"chessboard": self._chessboard_img,
                          "random": self._random_img,
                          "constant": self._constant_img}

    def _chessboard_img(self):
        shape = [np.int(np.ceil(val / 8)) for val in self.shape[:2]]
        max_shape = [np.max(shape)] * 2
        pattern = np.asarray([[1., 0.] * 4, [0., 1., ] * 4] * 4)
        img = np.ones(max_shape)
        img = np.kron(pattern, img)
        img = img[:self.shape[0], :self.shape[1]]

        if len(self.shape) > 2:
            channels = self.shape[2]
            img = img[:, :, np.newaxis] * \
                np.ones(channels)[np.newaxis, np.newaxis, :]

        return img.astype(getattr(np, self.dtype))

    def _random_img(self):
        channels = self.shape[-1] if len(self.shape) > 2 else None
        color = self.rng.uniform(0, 1, channels)
        img = np.ones(self.shape)
        if channels is not None:
            img = img * color[np.newaxis, np.newaxis, :]
        else:
            img *= color

        return img.astype(getattr(np, self.dtype))

    def _constant_img(self):
        channels = self.shape[-1] if len(self.shape) > 2 else None
        img = np.ones(self.shape)
        if channels is not None:
            color = np.asarray(self.color)
            img = img * color[np.newaxis, np.newaxis, :]
        else:
            img *= color

        return img.astype(getattr(np, self.dtype))

    def __call__(self, packet=None, hpacket=None):
        img = self.img_types[self.itype]()
        if not packet:
            return [Packet(**{self.attr: img})]

        setattr(packet, self.attr, img)
        return [packet]


class RoadImageSampler(ImageReader):

    """
    Generates random samples.

    Parameters
    ----------
    path: str
        path to the text file with image paths,
        the csv file has to have a packet name as the first row
        which is typically 'path'
    sep: str
        items separator
    path_attr: str
        packet attribute name to look for the image path
    img_attr: str
        Image attribute name
    flag : str
        OpenCv imread flags 'IMREAD_COLOR', 'IMREAD_GRAYSCALE',
        'IMREAD_UNCHANGED'.
    buf_size : int
        ImageReader buffer size to store last read image.
        Default is 0.
    sample_shape: [int, int]
        sampled image shape
    rotate_nth: int
        number of samples after the buffer is rotated
    buf_sample_size: int
        Size of the sampled buffer
    shuffle: bool
        True if shuffle the file image paths are read from.
        Default is False
    copy: bool
        Choose between a shallow or a deep copy of samples.
        Default is True, ie. make deep copy.
    seed: int
        random number generator seed
    multiple : bool
        Flag whether a sequence of images (indexed from 1) or
        a single image should be read.
    loop: bool
        number of samples after the buffer is rotated
    n_samples: int
        Number of all sampled samples. If set, filter throws
        StopIteration when n_samples+1 is started to read.

    Throws
    ------
    ipp.filters.ContinueIteration
        Whether image path does not exist.

    """

    parameters = [
        FP(name="path", value_type=str, required=False,
           help_msg="path to the text file with image paths,"
           "the csv file has to have a packet name as the first row"
           "which is typically 'path'.", default="path"),
        FP(name="sep", value_type=str, required=False,
           help_msg="items separator", default=" "),
        FP(name="path_attr", value_type=str, required=False,
           help_msg="packet attribute name to look for the image path",
           default="path"),
        FP(name="img_attr", value_type=str, required=False,
           help_msg="Image attribute name", default="img"),
        FP(name="flag", required=False,
           default="IMREAD_COLOR", value_type=str,
           help_msg="OpenCv imread flags 'IMREAD_COLOR', 'IMREAD_GRAYSCALE', "
           "'IMREAD_UNCHANGED'"),
        FP(name="buf_size", value_type=int, default=0, required=False,
           help_msg="ImageReader buffer size to store last read image."
           "Default is 0."),
        FP(name="sample_shape", value_type=list, default=0, required=False,
           help_msg="sampled image shape"),
        FP(name="rotate_nth", value_type=int, default=5, required=False,
           help_msg="number of samples after the buffer is rotated"),
        FP(name="buf_sample", value_type=int, default=5, required=False,
           help_msg="Size of the sampled buffer"),
        FP(name="shuffle", value_type=bool, default=None, required=False,
           help_msg="True if shuffle the file image paths are read from. "
           "Default is False"),
        FP(name="copy", value_type=int, default=True, required=False,
           help_msg="choose between a shallow or a deep copy of samples"),
        FP(name="seed", value_type=int, default=5, required=False,
           help_msg="random number generator seed"),
        FP(name="multiple", value=bool, default=False, required=False,
           help_msg="Flag whether a sequence of images (indexed from 1) or "
           "a single image should be read."),
        FP(name="loop", value_type=bool, default=True, required=False,
           help_msg="number of samples after the buffer is rotated"),
        FP(name="n_samples", value_type=int, default=None, required=False,
           help_msg="Number of all sampled samples. If set, filter throws "
           "StopIteration when n_samples+1 is started to read.")
    ]

    def __init__(self, path=None, sep=None, path_attr=None, img_attr=None,
                 flag=None, buf_size=0, sample_shape=(224, 224),
                 rotate_nth=5, buf_sample=5, shuffle=None, copy=True, seed=5,
                 multiple=False, loop=True, n_samples=None):

        super(RoadImageSampler, self).__init__(
            path_attr, img_attr, flag, buf_size, multiple)
        self.msec = 1000
        self.sample_shape = np.array(sample_shape)
        self.copy = copy
        self.offsets = self.sample_shape // 2  # p2 and p3 compatibility, see import
        self.seed = seed
        self.rng = np.random.RandomState(seed)
        self.loop = loop
        self.csv_reader = CSVReader(
            path=path, shuffle=shuffle, sep=sep, loop=loop, seed=seed)
        self._id = 0
        self.rotate_nth = rotate_nth
        self.buf_sample = buf_sample
        self.n_samples = n_samples

    def _init_buffer(self, buffer_size):
        """Initializes source image buffer"""
        t_start = time.time()
        img_buffer = []
        for i_image in range(buffer_size):
            self.log.debug_filter("Initializing sampling buffer %d/%d",
                                  i_image, buffer_size - 1)
            [packet] = self.csv_reader()
            path = getattr(packet, self.path_attr)
            self.sample_img_path = path
            img = super(RoadImageSampler, self)._read(path)

            img_buffer.append((img, packet))  # insert into buffer

        t_stop = time.time()
        self.log.time_filter("Initialization took: %f ms",
                             (t_stop - t_start) * self.msec)
        return img_buffer

    def _rotate_buffer(self):
        """Updates source image buffer"""
        t_start = time.time()
        self.image_buffer.pop(0)  # pop image from the front of the buffer

        [packet] = self.csv_reader()
        path = getattr(packet, self.path_attr)
        img = super(RoadImageSampler, self)._read(path)

        self.image_buffer.append((img, packet))  # append into the buffer

        self.log.time_filter("Rotating sample buffer took: %f ms",
                             (time.time() - t_start) * self.msec)

    def _get_random_sample(self, img):
        """Returns sample randomly cut out of the img"""

        begin = [self.rng.randint(0, img.shape[i] - self.sample_shape[i])
                 for i in range(2)]
        end = [begin[i] + self.sample_shape[i] for i in range(2)]

        sample = img[begin[0]:end[0], begin[1]:end[1]]

        if self.copy:
            return np.copy(sample), begin

        return sample, begin

    def read_sample(self):
        """Returns randomly selected ndarray of images"""

        if self.n_samples is not None and self.n_samples == self._id:
            raise StopIteration

        # Get buffer size and initialize if not initialized yet
        try:
            n_image_buffer = len(self.image_buffer)
        except AttributeError:
            self.image_buffer = self._init_buffer(self.buf_sample)
            n_image_buffer = len(self.image_buffer)

        # randomly cut out num = batch_size images and return as an numpy array
        rnd_num = self.rng.randint(0, n_image_buffer)
        sample_img, csv_packet = self.image_buffer[rnd_num]

        # cut random image at image_buffer[rnd_num]
        sample, bbox_begin = self._get_random_sample(sample_img)

        packet = Packet(**csv_packet.__dict__)
        packet.id = self._id
        packet.sample_shape = self.sample_shape
        packet.sample_center = bbox_begin + self.offsets

        setattr(packet, self.img_attr, sample)

        self._id += 1
        if self._id % self.rotate_nth == 0:
            self._rotate_buffer()

        return [packet]

    def __call__(self, packet=None, hpacket=None):
        return self.read_sample()
