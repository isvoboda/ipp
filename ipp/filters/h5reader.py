# coding=utf-8
"""H5 file readers"""

from __future__ import division, print_function

import itertools
import sys

# import h5py
import numpy as np

import tables

from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP
from .filters import Packet

# pylint: disable=R0903
# pylint: disable=C0302


__all__ = [
    "PyTabH5Reader"
    # "H5SampleReader"
    # "H5Reader"
]

#: Support of python2 and python3 bytes to Unicode (P2) or
#: bytes to str (P3) conversion.
PY2 = sys.version_info[0] == 2
if PY2:
    str = unicode


class PyTabH5Reader(Filter):

    """
    Reads the HDF5 data file based on PyTables

    Parameters
    ----------
    h5files : tuple, list
        List of paths to the HDF5 files.
    data_paths : tuple, list
        List of group paths to data (/table/table) within the hdf
        files.
    columns : tuple, list
        List of list of relevant columns. If None is given, all
        columns are used.
    loop : bool
        Loop through the files. Default is False.
    shuffle : bool
        Shuffle data.
    conditions : tuple, list
        List of conditions strings per h5file.
        See more
        http://www.pytables.org/usersguide/condition_syntax.html
        This implementation uses get_where_list method from PyTables
    rng : Numpy Random Generator
        Random number generator.
    probability: string
        Column which defines probability of rows.
    file_driver : string
        Low-level drivers, which map the logical HDF5 address space
        to different storage mechanisms. Default is "H5FD_SEC2".
        Possible values :
            - 'H5FD_SEC2'
                this driver uses POSIX file-system functions like
                read and write to perform I/O to a single, permanent
                file on local disk with no system buffering. This
                driver is POSIX-compliant and is the default file
                driver for all systems.
            - 'H5FD_DIRECT'
                this is the H5FD_SEC2 driver except data is written
                to or read from the file synchronously without being
                cached by the system.
            - 'H5FD_WINDOWS'
                this driver was modified in HDF5-1.8.8 to be a
                wrapper of the POSIX driver, H5FD_SEC2. This change
                should not affect user applications.
            - 'H5FD_STDIO'
                this driver uses functions from the standard C
                stdio.h to perform I/O to a single, permanent file
                on local disk with additional system buffering.
            - 'H5FD_CORE'
                with this driver, an application can work with a
                file in memory for faster reads and writes. File
                contents are kept in memory until the file is
                closed. At closing, the memory version of the file
                can be written back to disk or abandoned.

        see: http://www.pytables.org/usersguide/parameter_files.html#hdf5-driver-management
    h5file_attr : str, optional
        Set the packet attribute name to store the actual h5file
        path in. Default is 'h5file'.

    Raises
    ------
    ValueError
    StopIteration
        Raised when all rows in all h5 files were read.

    """
    parameters = [
        FP(name="h5files", required=True, value_type=list,
           help_msg="List of h5 files path"),
        FP(name="data_paths", required=True, value_type=list,
           help_msg="List of data paths in h5 files"),
        FP(name="columns", required=False,
           help_msg="List of Selects only given columns"),
        FP(name="loop", required=False, help_msg="Flag to loop over h5 file"),
        FP(name="shuffle", required=False, help_msg="Flag to shuffle h5 file"),
        FP(name="conditions", required=False,
           help_msg="Conditioned rows selection."),
        FP(name="seed", required=False,
           help_msg="Random number generator."),
        FP(name="probability", required=False,
           help_msg="Column which defines probability of rows."),
        FP(name="file_driver", required=False, help_msg="Name of a low level \
            HDF5 file driver. See \
            http://www.pytables.org/usersguide/parameter_files.html#hdf5-driver-management",
            default="H5FD_SEC2"),
        FP(name="h5file_attr", required=False, help_msg="Set the packet \
        attribute name to store the actual h5file path in. \
        Default is 'h5file'.")

    ]

    def __init__(self, h5files=None, data_paths=None, columns=None, loop=None,
                 shuffle=None, conditions=None, seed=5, probability=None, file_driver=None,
                 h5file_attr=None):
        """Initializes PyTabH5Reader"""
        super(PyTabH5Reader, self).__init__()
        self.h5files = h5files
        self.data_paths = data_paths
        self.columns = [None] * len(h5files) if columns is None else columns
        self.loop = loop
        self.shuffle = shuffle
        self.conditions = [None] * \
            len(h5files) if conditions is None else conditions
        self.rng = np.random.RandomState(seed)
        self.probability = probability
        self.file_driver = file_driver
        self.h5file_attr = h5file_attr or "h5file"
        self.h5desc = None

        self.i_h5file = 0
        # self._init_reader(self.h5files[self.i_h5file],
        #                   self.data_paths[self.i_h5file],
        #                   self.columns[self.i_h5file],
        #                   self.conditions[self.i_h5file])

    def _init_reader(self,
                     h5file=None,
                     data_path=None,
                     columns=None,
                     condition=None):

        try:
            self.h5store = tables.open_file(
                filename=h5file, mode='r', driver=self.file_driver,
                driver_core_backing_store=False)

            self.table = self.h5store.get_node(where=data_path)

            self.indices = None
            if condition is not None:
                self.indices = self.table.get_where_list(condition)
            else:
                self.indices = np.arange(self.table.nrows)

            if self.shuffle:
                self.rng.shuffle(self.indices)

            self.iter = iter(self.indices)

            if columns is not None:
                cols = [(i_col, col) for i_col, col in enumerate(
                    self.table.dtype.names) if col in columns]
            else:
                cols = [(i_col, col) for i_col, col in enumerate(
                    self.table.dtype.names)]

            cols.sort(key=lambda tup: tup[0])
            self.col_id, self.col_names = zip(*cols)

            if self.probability is not None:
                self.probability_id = self.col_names.index(self.probability)

        except tables.NoSuchNodeError:
            self.log.exception(
                "No %s node exists in %s.", data_path, h5file)
            raise

        except tables.HDF5ExtError:
            self.log.exception("Can not open file %s", h5file)
            raise

        except ValueError:
            self.log.exception("Failed to read: %s with a group %s",
                               h5file, data_path)
            if self.h5store is not None:
                self.h5store.close()
            raise

    # pylint: disable=W0613
    def read(self, packet, hpacket=None):
        """Reads a row in the h5file

        Raises
        ------
        StopIteration
            Raised if EOF and loop is False.

        Returns
        -------
        out : [Packet]
            List of packet with all user selected h5 columns mapped to
            attributes.

        """
        idx = 0
        try:
            if self.probability is None:
                idx = next(self.iter)
            else:
                while True:
                    idx = next(self.iter)
                    row = self.table[idx]
                    p = row[self.probability_id]
                    if self.rng.rand(1)[0] < p:
                        break
        except AttributeError:
            # Lazy initialization
            self._init_reader(self.h5files[self.i_h5file],
                              self.data_paths[self.i_h5file],
                              self.columns[self.i_h5file],
                              self.conditions[self.i_h5file])

        except StopIteration:
            self.log.info("End of %s", self.h5files[self.i_h5file])
            self.i_h5file += 1
            if self.i_h5file >= len(self.h5files):
                if self.loop:
                    self.i_h5file = 0
                else:
                    self.h5store.close()
                    raise

            self.log.info("Continue with %s", self.h5files[self.i_h5file])
            self._init_reader(self.h5files[self.i_h5file],
                              self.data_paths[self.i_h5file],
                              self.columns[self.i_h5file],
                              self.conditions[self.i_h5file])

            idx = next(self.iter)

        row = self.table[idx]

        # P2 & P3 Unicode string compatibility
        attrs = dict()
        for i, i_col in enumerate(self.col_id):
            if isinstance(row[i_col], np.string_):
                # str method is reasinged to str in case of P3 or
                # unicode in case of P2
                attrs[self.col_names[i]] = str(row[i_col], "utf-8")

        packet = Packet(**attrs)
        setattr(packet, self.h5file_attr, self.h5files[self.i_h5file])

        return [packet]

    def __call__(self, packet, hpacket=None):
        return self.read(packet, hpacket)


class H5SampleReader(Filter):

    """Reads the HDF5 data file based on PyTables

    Parameters
        ----------
        h5files : tuple, list
            List of paths to the HDF5 files.
        data_paths : tuple, list
            List of group paths to data (/table/table) within
            the hdf files.
        columns : tuple, list
            List of list of relevant columns. If None is given, all
            columns are used.
        loop : bool
            Loop through the files. Default is False.
        shuffle : bool
            Shuffle data.
        conditions : tuple, list
            List of conditions strings per h5file.
            See more
            http://www.pytables.org/usersguide/condition_syntax.html
            This implementation uses get_where_list method from PyTables
        seed : int, optional
            Random number generator seed
        sample_col : str
            The column which according are the data sampled
        bins : list
            A list which specifies ranges based on which data is
            sampled. [0,10,25,50] defines 3 intervals
            [0,10), [10,25), [25,50)
        bins_weight: list
            A list defining related category sampling probability. It is
            always normalized.
            [1,2,3] defines probabilities [1/6, 1/3, 1/2].
            If not defined, default is uniform distribution [1,1,...,1]
        rng : Numpy Random Generator
            Random number generator.
        file_driver : string
            Low-level drivers, which map the logical HDF5 address space
            to different storage mechanisms.
            Possible values :
                'H5FD_SEC2'
                    this driver uses POSIX file-system functions like
                    read and write to perform I/O to a single, permanent
                    file on local disk with no system buffering. This
                    driver is POSIX-compliant and is the default file
                    driver for all systems.
                'H5FD_DIRECT'
                    this is the H5FD_SEC2 driver except data is written
                    to or read from the file synchronously without
                    being cached by the system.
                'H5FD_WINDOWS'
                    this driver was modified in HDF5-1.8.8 to be a
                    wrapper of the POSIX driver, H5FD_SEC2. This change
                    should not affect user applications.
                'H5FD_STDIO'
                    this driver uses functions from the standard
                    C stdio.h to perform I/O to a single, permanent
                    file on local disk with additional system buffering.
                'H5FD_CORE'
                    with this driver, an application can work with a
                    file in memory for faster reads and writes. File
                    contents are kept in memory until the file is
                    closed. At closing, the memory version of the file
                    can be written back to disk or abandoned.

            see: http://www.pytables.org/usersguide/parameter_files.html#hdf5-driver-management
        h5file_attr : str, optional
            Set the packet attribute name to store the actual h5file
            path in. Default is 'h5file'.

    Raises
    ------
    ValueError
    StopIteration
        Raised when all rows in all h5 files were read.

    Notes
    -----
    Known bug can occur in case no data in a specific range of actual
    dataset are available. In such a case the only several samples or
    None are read.

    ToDo Reimplement the sampling to avoid the numpy.choice.

    """
    parameters = [
        FP(name="h5files", required=True, value_type=list,
           help_msg="List of h5 files path"),
        FP(name="data_paths", required=True, value_type=list,
           help_msg="List of data paths in h5 files"),
        FP(name="columns", required=False,
           help_msg="List of Selects only given columns"),
        FP(name="loop", required=False, help_msg="Flag to loop over h5 file"),
        FP(name="shuffle", required=False, help_msg="Flag to shuffle h5 file"),
        FP(name="conditions", required=False,
           help_msg="Conditioned rows selection."),
        FP(name="sample_col", required=True,
           help_msg="The column which according are the data sampled"),
        FP(name="bins", required=True,
           help_msg="A list which specifies ranges based on which data is \
           sampled.[0,10,25,50] defines 3 intervals [0,10), [10,25), [25,50)"),
        FP(name="bins_weight", required=False,
           help_msg="A list defining related category sampling probability. \
           It is always normalized. [1,2,3] defines probabilities \
           [1/6, 1/3, 1/2]. If not defined, default is uniform \
           distribution [1,1,...,1]"),
        FP(name="seed", required=False,
           help_msg="Random number generator."),
        FP(name="file_reader", required=False, help_msg="Name of a low level \
        HDF5 file reader. See \
        http://www.pytables.org/usersguide/parameter_files.html#hdf5-driver-management"),
        FP(name="h5file_attr", required=False, help_msg="Set the packet \
        attribute name to store the actual h5file path in. \
        Default is 'h5file'.")

    ]

    def __init__(self, h5files=None, data_paths=None, columns=None, loop=None,
                 shuffle=None, conditions=None, sample_col=None, bins=None,
                 bins_weight=None, seed=5, file_driver=None, h5file_attr=None):
        """Initializes PyTabH5Reader"""
        super(H5SampleReader, self).__init__()
        self.h5files = h5files
        self.data_paths = data_paths
        self.columns = self._init_lists(columns)
        self.loop = loop
        self.shuffle = shuffle
        self.conditions = self._init_lists(conditions)
        self.sample_col = sample_col
        self.bins = self._init_lists(bins)
        self.bins_weight = self._init_lists(bins_weight)
        self.rng = np.random.RandomState(seed)
        self.file_driver = file_driver
        self.h5file_attr = h5file_attr if h5file_attr is not None else "h5file"
        self.h5desc = None

        self.i_h5file = 0
        self._init_reader(self.h5files[self.i_h5file],
                          self.data_paths[self.i_h5file],
                          self.columns[self.i_h5file],
                          self.conditions[self.i_h5file],
                          self.bins[self.i_h5file],
                          self.bins_weight[self.i_h5file])

    def _init_lists(self, list_param):
        n_datasets = len(self.h5files)
        if list_param is None:
            return [None] * n_datasets
        if isinstance(list_param, (list,)) and len(list_param) == n_datasets:
            return list_param
        else:
            return [list_param[0]] * n_datasets

    def _init_reader(self, h5file=None, data_path=None, columns=None,
                     condition=None, bins=None, bins_weight=None):
        """
        Initializes the reader

        Raises
        ------
        ValueError

        Notes
        -----
            ToDo : Provide better failure information, eg. unknown columns.

        """
        try:
            self.h5store = tables.open_file(
                filename=h5file, mode='r', driver=self.file_driver,
                driver_core_backing_store=False)
            self.table = self.h5store.get_node(where=data_path)

            if condition is not None:
                self.data = self.table.read_where(condition)
            else:
                self.data = self.table.read()

            self.size = len(self.data)
            col = self.data[self.sample_col]

            # Get the sampled column as ndarray
            split_col = np.digitize(col, bins)

            self.indices = [(split_col == (b + 1)).nonzero()[0] for b in
                            range(len(bins) - 1)]

            if self.shuffle:
                for i in range(len(self.indices)):
                    self.rng.shuffle(self.indices[i])

            self.iter_samples = [itertools.cycle(idx) for idx in self.indices]

            # Weighted choice of bin class
            if bins_weight is not None:
                sum_bins = np.sum(bins_weight)
                bins_weight = [weight / sum_bins for weight in bins_weight]

            self.list_iter_bins = np.random.choice(
                a=len(self.iter_samples), size=self.size, replace=True,
                p=bins_weight)

            self.iter = iter(self.list_iter_bins)

            if columns is not None:
                cols = [(i_col, col) for i_col, col in enumerate(
                    self.data.dtype.names) if col in columns]
            else:
                cols = [(i_col, col) for i_col, col in enumerate(
                    self.data.dtype.names)]

            cols.sort(key=lambda tup: tup[0])
            self.col_id, self.col_names = zip(*cols)

            self.log.info("%s size: %d", h5file, self.size)

        except tables.NoSuchNodeError:
            self.log.exception(
                "No %s node exists in %s.", data_path, h5file)
            raise

        except tables.HDF5ExtError:
            self.log.exception("Can not open file %s", h5file)
            raise

        except ValueError:
            self.log.exception("Failed to read: %s with a group %s",
                               h5file, data_path)
            if self.h5store is not None:
                self.h5store.close()
            raise

    # pylint: disable=W0613
    def read(self, packet, hpacket=None):
        """Reads a row in the h5file"""

        idx = None
        try:
            iter_bin = self.iter_samples[next(self.iter)]
            idx = next(iter_bin)

        except StopIteration:
            self.log.info("End of %s", self.h5files[self.i_h5file])
            self.i_h5file += 1
            if self.i_h5file >= len(self.h5files):
                if self.loop:
                    self.i_h5file = 0
                else:
                    self.h5store.close()
                    raise

            self.log.info("Continue with %s", self.h5files[self.i_h5file])
            self._init_reader(self.h5files[self.i_h5file],
                              self.data_paths[self.i_h5file],
                              self.columns[self.i_h5file],
                              self.conditions[self.i_h5file],
                              self.bins[self.i_h5file],
                              self.bins_weight[self.i_h5file])

            iter_bin = self.iter_samples[next(self.iter)]
            idx = next(iter_bin)

        row = self.data[idx]
        attrs = {self.col_names[i_col]: row[i_col]
                 for i_col in self.col_id}
        packet = Packet(**attrs)
        setattr(packet, self.h5file_attr, self.h5files[self.i_h5file])
        return [packet]

    def __call__(self, packet, hpacket=None):
        return self.read(packet, hpacket)


# class H5Reader(Filter):

#     """
#     Reads the HDF5 data file based on h5py

#     Attributes
#     ----------
#     log
#         Logger handler.
#     h5file : str
#         Path to a h5 file.
#     data_path : str
#         Path do a dataset in a h5 file.
#     columns : list
#         List of column names to be selected from a dataset.
#     loop : boolean
#         Flag to cycle through the dataset, True: cycle,
#         None/False : single pass
#     shuffle : boolean
#         Flag to shuffle the dataset (Actually only access indices are
#         shuffled).
#     rng : Random Generator
#         Random number generator used.
#     file_driver : str
#         File driver for a h5 file manipulation.
#         http://docs.h5py.org/en/latest/high/file.html#file-drivers
#     h5desc : File descriptor
#         h5 file descriptor.

#     Raises
#     ------
#     ValueError
#     StopIteration
#         Raised when all rows wear read.

#     """
#     parameters = [
#         FP(name="h5file", required=True, value_type=str,
#            help_msg="Path to h5 file"),
#         FP(name="data_path", required=True, value_type=str,
#            help_msg="Data path in h5 file"),
#         FP(name="columns", required=False,
#            help_msg="Selects only given columns"),
#         FP(name="loop", required=False, help_msg="Flag to loop over h5 file"),
#         FP(name="shuffle", required=False, help_msg="Flag to shuffle h5 file"),
#         FP(name="file_reader", required=False, help_msg="Name of a HDF5 file \
#         reader. \
#         See http://docs.h5py.org/en/latest/high/file.html#file-drivers")
#     ]

#     def __init__(self, h5file=None, data_path=None, columns=None, loop=None,
#                  shuffle=None, rng=None, file_driver=None):
#         """Initializes H5Reader

#         Parameters
#         ----------
#         file_path : str
#             Path to the HDF5 file.
#         data_path : str
#             Path to the table within the hdf file.
#         columns : list
#             List of relevant columns. If None is given, all columns
#             are used.
#         loop : bool
#             Cyclic data read.
#         shuffle : bool
#             Shuffle data.
#         rng : Numpy Random Generator
#             Random number generator.
#         file_driver : string
#             Low-level drivers, which map the logical HDF5 address space
#             to different storage mechanisms.
#             Possible values :
#                 None:
#                     Standard HDF5 driver appropriate for the current
#                     platform. On UNIX, this is the H5FD_SEC2 driver on
#                     Windows, it is H5FD_WINDOWS.
#                 'sec2':
#                     Unbuffered, optimized I/O using standard POSIX
#                     functions.
#                 'stdio':
#                     Buffered I/O using functions from stdio.h.
#                 'core':
#                     Memory mapped(file is buffered into a memory).

#         """
#         super(H5Reader, self).__init__()
#         self.h5file = h5file
#         self.data_path = data_path
#         self.columns = columns
#         self.loop = loop
#         self.shuffle = shuffle
#         self.rng = rng if rng is not None else np.random.RandomState(5)
#         self.file_driver = file_driver
#         self.h5desc = None

#         self._init_reader(self.h5file, self.data_path)

#     def _init_reader(self, data=None, data_path=None):
#         """
#         Initializes the reader

#         Raises
#         ------
#         ValueError

#         Notes
#         -----
#             ToDO : Provide better failure information,
#             eg. unknown columns.
#         """
#         try:
#             self.h5desc = h5py.File(data, "r", driver=self.file_driver,
#                                     backing_store=False)
#             self.dataset = self.h5desc[data_path]
#             self.indices = np.arange(len(self.dataset))

#             if self.columns is not None:
#                 columns = [(i_col, col) for i_col, col in enumerate(
#                     self.dataset.dtype.names) if col in self.columns]
#             else:
#                 columns = [(i_col, col) for i_col, col in enumerate(
#                     self.dataset.dtype.names)]

#             columns.sort(key=lambda tup: tup[0])
#             self.col_id, self.col_names = zip(*columns)

#             if self.shuffle:
#                 self.rng.shuffle(self.indices)

#             # if self.loop is True:
#             #     self.iter = cycle(self.dataset)
#             # else:
#                 # self.iter = iter(self.dataset[self.col_names]) # forces the
#                 # entire dataset to be read in memory first
#             self.iter = iter(self.indices)

#         except ValueError:
#             self.log.exception("Failed to read: %s with a record %s",
#                                self.h5file, self.data_path)
#             if self.h5desc is not None:
#                 self.h5desc.close()
#             raise

#     # pylint: disable=W0613
#     def read(self, packet, hpacket=None):
#         """Reads a row in the h5file

#         Parameters
#         ----------
#         packet : Packet
#             not used
#         hpacket not used

#         Raises
#         ------
#         StopIteration
#             Raised if EOF and loop is False.

#         Returns
#         -------
#         out : [Packet]
#             List of packet with all user selected h5 columns mapped
#             to attributes.

#         """
#         idx = 0
#         try:
#             idx = next(self.iter)

#         except StopIteration:
#             self.log.info("Read end %s", self.h5file)
#             if self.loop:
#                 self.h5desc.close()
#                 raise
#             else:
#                 self.log.info("Shuffling data %s", self.h5file)
#                 self.rng.shuffle(self.indices)
#                 self.iter = iter(self.indices)
#                 idx = next(self.iter)

#         row = self.dataset[idx]
#         packet = Packet()
#         for i_col in self.col_id:
#             setattr(packet, self.col_names[i_col], row[i_col])

#         return [packet]

#     def __call__(self, packet, hpacket=None):
#         return self.read(packet, hpacket)
