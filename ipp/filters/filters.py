# coding=utf-8
"""Filters definitions
Use the __all__ variable to list the filters which shall be exported.

ToDo: use a metaclass to modify Filter init parameters according
the parameters attribute.
"""

# This may produce problems, when default Packet's attribute is defined
# as string. In python 2, all identifiers have to be ascii representing
# letters, numbers and several dash like symbols. This, actually, make
# the identifier name an unicode object, which is automatically bytes -
# ascii encoded. Packet`s __str__ method has to take this into account.
from __future__ import division, print_function, unicode_literals

import copy
import numbers
import os
import random
import string
import sys
import time

import cv2
import numpy as np

from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP

# pylint: disable=R0903
# pylint: disable=C0302


__all__ = [
    "Packet",
    "ContinueIteration",
    "SimpleFilter",
    "AccumulateAttrs",
    "AccumulatePackets",
    "BBox2ShapeCenter",
    "Collapse",
    "Copy",
    "DepositHorizontal",
    "Expand",
    "ExpandAttr",
    "Label",
    "Lambda",
    "LambdaDoom",
    "Merge",
    "OneHotEncoding",
    "OpMeanImg",
    "Pass",
    "PathExpander",
    "PathRelativizer",
    "Picker",
    "PreviewImage",
    "PrintPacket",
    "SelectAttr",
    "SetAttr",
    "SplitPacket",
    "ThrowAway",
    "ToCaffeBlob"
]

PY2 = sys.version_info[0] == 2

def python_2_unicode_compatible(klass):
    """
    A decorator that defines __unicode__ and __str__ methods under Python 2.
    Under Python 3 it does nothing.
    To support Python 2 and 3 with a single code base, define a __str__ method
    returning text and apply this decorator to the class.

    This is stolen from six package.
    """
    if PY2:
        if '__str__' not in klass.__dict__:
            raise ValueError("@python_2_unicode_compatible cannot be applied "
                             "to %s because it doesn't define __str__()." %
                             klass.__name__)
        klass.__unicode__ = klass.__str__
        klass.__str__ = lambda self: self.__unicode__().encode('utf-8')
    return klass

@python_2_unicode_compatible
class Packet(object):

    """Packet is a class (more like a C++ struct) equivalent to message

    Notes
    -----
    For the design pattern behind this approach, see
    http://python-3-patterns-idioms-test.readthedocs.io/en/latest/Messenger.html

    Also have a look at http://stackoverflow.com/a/610923 about:
    "easier to ask for forgiveness than permission" (EAFP) rather than
    "look before you leap" (LBYL) when dealing with the Packet
    attributes.

    """

    def __init__(self, **kwargs):
        """
        Packet initialization - set the attributes

        Parameters
        ----------
        **kwargs
            Arbitrary keyword arguments.

        """
        self.__dict__ = kwargs

    def __contains__(self, attr):
        return attr in self.__dict__

    def __str__(self):
        return "\n".join("{}: {}".format(key.decode("utf-8") if PY2 else key,
                                         self.__dict__[key])
                         for key in sorted(list(self.__dict__.keys())))

    def __sizeof__(self):
        sum_size = object.__sizeof__(self)
        for attr in self.__dict__.values():
            if isinstance(attr, (np.ndarray, np.generic)):
                sum_size += attr.nbytes
            else:
                sys.getsizeof(attr)
        return sum_size


class ContinueIteration(Exception):

    """Allows to continue the pipeline"""

    def __init__(self, data=None):
        """Constructor of a ContinueIteration

        Parameters
        ----------
        data : Arbitrary data the exception can carry
        """

        super(ContinueIteration, self).__init__()
        self.data = data


class SimpleFilter(Filter):

    """SimpleFilter

    Parameters
    ----------
    message : string
        Message to be printed.
    data : int
        Data the filter operates with.
    not_parsed
        Example of an unexpected parameter

    Notes
    -----
    See a base class Filter for what functionality is directly
    available.

    """
    # Argparse equivalent, if defined, the parameters of __init__
    # are validated against these definition, see  FilterParameter (FP)
    # for available opts.
    parameters = [
        FP(name="message", value_type=str, help_msg="A message to be printed",
           required=False),
        FP(name="data", value_type=int, help_msg="Some numerical data",
           required=True)
    ]

    def __init__(self, message=None, data=None, not_parsed=None):
        super(SimpleFilter, self).__init__()
        self.message = message
        self.data = data

    def __call__(self, packet, horizontal_packet=None):
        """Print an its and packet content.

        Parameters
        ----------
        packet: Packet
            Arbitrary content.
        horizontal_packet: Packet
            Contains a packet from a previous filter in the same layer.

        Returns
        -------
        packet : [Packet]
            List with forwarded packet.

        Notes
        -----
        Called when the instance is called as a function

        """
        print(self)
        print(packet)

        return [packet]


class AccumulateAttrs(Filter):

    """AccumulateAttr

    Parameters
    ----------
    size : int
        Size of accumulation buffer. Minimum is 1.
    attrs : list
        List of packet attributes to accumulate. If None, accumulate
        all.

    Returns
    -------
    out : [Packet]
        List of single packet.

    Exceptions
    ----------
    ValueError
        No accumulate size, nor hpacket with accumulate size.
    ContinueIteration
        Continue processing till accumulated size.
    """

    def __init__(self, size=1, attrs=None):
        """Constructor of a AccumulateAttr """
        super(AccumulateAttrs, self).__init__()
        self.size = size
        if size < 1:
            raise ValueError("Parameter size has to be >= 1.")

        self.attrs = attrs

        self.select_attrs = self._all_attrs if attrs is None \
            else self._select_attrs

        self.buf = {}
        self.counter = 0

    def _all_attrs(self, packet):
        return packet.__dict__

    def _select_attrs(self, packet=None):
        return self.attrs

    def __call__(self, packet, hpacket=None):

        if self.counter == (self.size):
            acc_packet = Packet(**self.buf)
            self.buf = {}

            for attr in self.select_attrs(packet):
                self.buf[attr] = [getattr(packet, attr)]
            self.counter = 1

            return [acc_packet]

        else:
            for attr in self.select_attrs(packet):
                try:
                    self.buf[attr].append(getattr(packet, attr))
                except KeyError:
                    self.buf[attr] = [getattr(packet, attr)]
            self.counter += 1

            raise ContinueIteration()


class AccumulatePackets(Filter):

    """AccumulatePackets

    Parameters
    ----------
    size : int
        How many packets to accumulate.
    attr : str
        Attribute name the packets are stored to.

    Returns
    -------
    out : [Packet]
        List of single packet.

    Exceptions
    ----------
    ValueError
        No accumulate size, nor hpacket with accumulate size.
    ContinueIteration
        Continue processing till blob size.
    """

    def __init__(self, size=None, attr="packets"):
        """Constructor of a AccumulateData """
        super(AccumulatePackets, self).__init__()
        self.size = size
        self.attr = attr
        self.buf = []

    def __call__(self, packet, hpacket=None):
        if len(self.buf) == self.size:
            acc_packet = Packet(packets=self.buf)
            self.buf = [packet]
            return [acc_packet]
        else:
            self.buf.append(packet)
            raise ContinueIteration()


class BBox2ShapeCenter(Filter):

    """Transforms bounding box to shape and center representation

    Parameters
    ----------
    attr : str
        Attribute name holding the bounding box
    shape_attr : str
        Attribute name to store the shape.
    center_attr : str
        Attribute name to store the center.

    """
    parameters = [
        FP(name="attr", value_type=str, required=True,
           help_msg="Attribute with the bounding box."),
        FP(name="shape_attr", value_type=str, required=True,
           help_msg="Attribute name to store the shape."),
        FP(name="shape_attr", value_type=str, required=True,
           help_msg="Attribute name to store the center.")
    ]

    def __init__(self, attr, shape_attr, center_attr):
        super(BBox2ShapeCenter, self).__init__()
        self.attr = attr
        self.shape_attr = shape_attr
        self.center_attr = center_attr

    def __call__(self, packet, hpacket=None):
        bbox = getattr(packet, self.attr)

        shape = [bbox[2] - bbox[0], bbox[3] - bbox[1]]
        center = [(shape[0] / 2) + bbox[0], (shape[1] / 2) + bbox[1]]

        setattr(packet, self.shape_attr, shape)
        setattr(packet, self.center_attr, center)

        return [packet]


class Collapse(Filter):

    """Creates a single packet from packets

    Creates a single packet with a single attribute packets which
    includes all the horizontally passed packets in a previous layer.

    Input
    -----
    packet : Packet()
        expected attributes:
        packet.packets : list of all horizontally passed packets in
        a previous layer.

    Returns
    -------
        packet : Packet
            with a single attribute packet.packets which is a list of
            all horizontally passed packets in a previous layer.

    Raises
    ------
        AttributeError
    """

    def __init__(self):
        """Collapse"""

        super(Collapse, self).__init__()

    def __call__(self, packet, hpacket=None):
        """Creates a packet with a single attribute packet.packets

        Parameters
        ----------
        packet : Packet
            Takes the packet.packets attribute only.
        hpacket : Packet
            Not used.

        Returns
        -------
        out : [Packet]
            List of single packet.

        Raises
        ------
        AttributeError

        """
        # try:
        collapsed_packet = Packet(packets=packet.packets)
        return [collapsed_packet]
        # except AttributeError:
        #     self.log.exception("Missing an attribute packet.packets")
        #     raise


class Copy(Filter):

    """Copy a packet to a user defined number of copies

    Input
    -----
    packet : Packet()

    Attributes
    ----------
    n_copies : int
        Number of copies.

    Returns
    -------
    out : list
        [packet, ...] a user defined number of packets.
    """

    parameters = [
        FP(name="n_copies", value_type=int,
           help_msg="The number of packet copies", required=True)
    ]

    def __init__(self, n_copies=None):
        """Constructor of Copy filter

        Parameters
        ----------
        n_copies : int
            Number of copies to produce.

        """

        super(Copy, self).__init__()
        self.n_copies = n_copies

    def __call__(self, packet, hpacket=None):
        """Copy a packet

        Parameters
        ----------
        packet : Packet
            Expands the packet.packets.
        hpacket : Packet
            Not used.

        Returns
        -------
        out : [packet, ...]
             User defined number of packets.

        """
        return [copy.deepcopy(packet) for _ in range(self.n_copies)]


class DepositHorizontal(Filter):

    """
    DEPRECATED

    Deposits a packet into a packet.packets which is shared from a
    previous packet
    """

    def __init__(self):
        """Constructor of a DepositHorizontal"""
        super(DepositHorizontal, self).__init__()

    def __call__(self, packet, hpacket=None):
        """Appends a packet into a packet.packets

        packet.packets is shared between all the previous packets.
        All the layer packets share a packet.packets attribute.

        Parameters
        ----------
        packet : Packet
        hpacket : Packet

        Returns
        -------
        out : Packet
            packet.packets : all the previously added packets with
            an actual one in a single processing layer.
        """
        if hasattr(hpacket, "packets"):
            packet.packets = hpacket.packets
        else:
            packet.packets = []
        packet.packets.append(packet)

        # self.log.debug("Deposit packets: %d", len(packet.packets))

        return [packet]


class Expand(Filter):

    """Expands a packet.packets list into a list of packets

    should be applied after Collapse filter.

    Input
    -----
    packet : Packet()
        expected attributes:
        packet.packets : list of all horizontally passed packets in
        a previous layer.

    Return
    ------
    out : [packet, ...]

    Raises
    ------
    AttributeError

    """

    def __init__(self):
        """Constructor of Expand filter"""

        super(Expand, self).__init__()

    def __call__(self, packet, hpacket=None):
        """Expand packet.packets into a list of packets

        Parameters
        ----------
        packet : Packet
            Expands the packet.packets.
        hpacket : Packet
            Not used.

        Returns
        -------
        out : [Packet, ...]
            List of packets.

        Raises
        ---------
        AttributeError

        """
        return packet.packets


class ExpandAttr(Filter):

    """Expands packet's dictionary attribute values into attributes

    The keys are used as new Packet's attribute names. If Packet's
    attribute name is the same as dictionary's key, packet's attribute
    value is overwritten by the value from the dictionary.

    Parameters
    ----------
    attr : str
        Attribute to expand as packets attributes.

    """

    def __init__(self, attr=None):
        super(ExpandAttr, self).__init__()
        self.attr = attr

    def __call__(self, packet, hpacket=None):
        dict_attr = getattr(packet, self.attr)
        for key, val in dict_attr.items():
            setattr(packet, key, val)

        return [packet]


class Label(Filter):

    """Label a packet with user defined name and set in_net attribute

    in_net attribute defines data used as a network input

    Parameters
    ----------
    name : str or list, tuple
        Label name.
    in_net : str or list, tuple
        Attribute name used as a model input.

    Note
    ----
    Label filter sets two packet's attributes.
    First, the packet name, which can be directly a string or a list,
    tuple of strings which should bind to a model input layer of the
    same name.
    Second, the in_net attribute defines the attribute holding the data
    used as the model input.

    """

    parameters = [
        FP(name="name", value_type=(str, list, tuple),
           help_msg="Label name", required=True),
        FP(name="in_net", value_type=(str, list, tuple),
           help_msg="Attribute name used as a model input", required=True)
    ]

    def __init__(self, name=None, in_net=None):
        """Constructor of Label filter

        Parameters
        ----------
        name : str
            Label name.
        in_net : str
            Attribute name used as a nn input.

        """

        super(Label, self).__init__()
        self.name = name
        self.in_net = in_net

    def __call__(self, packet, hpacket=None):
        """Copy a packet

        Parameters
        ----------
        packet : Packet
        hpacket : Packet
            Not used.

        Returns
        -------
        out : [Packet]
            List of packet with packet.label and packet.in_net attributes.

        """
        packet.label = self.name
        if isinstance(self.in_net, (list, tuple)):
            packet.in_net = [getattr(packet, attr) for attr in self.in_net]
        else:
            packet.in_net = getattr(packet, self.in_net)
        return [packet]


class Lambda(Filter):

    """Provides a lambda function with selected packet attribute

    Parameters
    ----------
    attr : string
        Attribute name to apply the lambda function to
    out_attr : str
        Packet's attribute name, where the computed value should be saved.
        If None is provided, attr will be used.
    lmbd : string
        Definition of lambda function.

    Raises
    ------
    AttributeError

    Example
    -------
        Lambda(attr="img", lmbd="lambda x: x+32")
            Adds 32 to a packet attribute img.

    Notes
    -----
        Dangerous, based on eval.

    """

    parameters = [
        FP(name="attr", value_type=str,
           help_msg="Attribute to apply labmda function on", required=True),
        FP(name="lmbd", value_type=str,
           help_msg="Lambda function", required=True),
        FP(name="out_attr", value_type=str,
           help_msg="Attribute to store the lambda function result",
           required=False),
    ]

    def __init__(self, attr=None, lmbd=None, out_attr=None):
        """Constructor of a Lambda """
        super(Lambda, self).__init__()
        self.attr = attr
        self.out_attr = out_attr if out_attr else attr
        self.lmbd = lmbd

    def __call__(self, packet, hpacket=None):
        """Provides a lambda function

        Parameters
        ----------
        packet : Packet
        hpacket : Packet
            Not used.

        Returns
        -------
        out : [Packet]
            List of packet.

        Raises
        ------
        AttributeError

        """
        attr = getattr(packet, self.attr)
        try:
            attr = self.lmbd_fce(attr)
        except AttributeError:
            # Lazy lambda initialization
            self.lmbd_fce = eval(self.lmbd)  # pylint: disable=W0123
            attr = self.lmbd_fce(attr)

        setattr(packet, self.out_attr, attr)
        return [packet]


class LambdaDoom(Filter):

    """Provides a lambda function with a direct access to entire world.

    Parameters
    ----------
    lmbd : string
        Definition of lambda function called with a packet as parameter.

    Raises
    ------
    AttributeError

    Notes
    -----
        Dangerous, based on eval.

    """

    parameters = [
        FP(name="lmbd", value_type=str,
           help_msg="LambdaDoom function", required=True)
    ]

    def __init__(self, lmbd=None):
        """Constructor of a LambdaDoom"""
        super(LambdaDoom, self).__init__()
        self.lmbd = lmbd

    def __call__(self, packet, hpacket=None):
        """Provide a lambda function

        Parameters
        ----------
        packet : Packet
        hpacket : Packet
            Not used.

        Returns
        -------
        out : [Packet]
            List of packet.

        Raises
        ------
        AttributeError

        """
        try:
            self.lmbd_fce(packet)
        except AttributeError:
            # Lazy lambda initialization
            self.lmbd_fce = eval(self.lmbd)  # pylint: disable=W0123
        self.lmbd_fce(packet)

        return [packet]


class Merge(Filter):
    """Merges all previous (left) packets in the same layer

    Creates a single packet with all the left packets attributes.
    In a case of attribute name conflict, the attribute is rewritten
    with the last same attribute value.
    All packets to left from the Merge filter are cleared.

    Example:
    -----
    packet_0 = Packet(img=image_0, meta_data=data)
    packet_1 = Packet(img=image_1, path="path")

    msg_packet = Packet(packets=[packet_0, packet_1])

    merge = Merge()

    [merged_packet] = merge(msg_packet)

    merged_packet contains:
        img=img_1, meta_data=data, path="path"

    """

    def __init__(self):
        super(Merge, self).__init__()

    def merge(self, packet, hpacket=None):
        """Merge several packets into single packet"""
        packets = [packet]
        if hpacket is not None:
            packets = hpacket + packets
            del hpacket[:]

        merged_dict = {}
        for pckt in packets:
            merged_dict = dict(merged_dict, **pckt.__dict__)

        merged_packet = Packet(**merged_dict)
        return [merged_packet]

    def __call__(self, packet, hpacket=None):
        return self.merge(packet, hpacket)


class OneHotEncoding(Filter):

    """Creates a OneHotEncoding vector where ith class is set to one.

    Parameters
    ----------
    attr : str
        Attribute name to be searched for in the packet to identify
        a class index.
    n_class : int
        Number of classes.
    onehotencode_attr : str
        Attribute name to store the onehotencode in. Default is
        'onehotencode'.

    Notes
    -----
    Five different classes Y are defined as [0, ..., 4]
    particular class y = 3 is written as SoftMaxLabel = [0,0,0,1,0]
    """

    def __init__(self, attr, n_class, onehotencode_attr=None):
        super(OneHotEncoding, self).__init__()
        self.attr = attr
        self.n_class = n_class
        self.onehotencode_attr = onehotencode_attr if \
            onehotencode_attr is not None else "onehotencode"

    def __call__(self, packet, hpacket):
        class_id = getattr(packet, self.attr)
        softmax_label = np.zeros(self.n_class)
        softmax_label[class_id] = 1
        setattr(packet, self.onehotencode_attr, softmax_label)
        return [packet]


class OpMeanImg(Filter):

    """ Provide an simple addition or subtraction of mean image

    Parameters
    ----------
    path : str
        Mean image path.
    op : str
        Defines the operation to provide. It could be 'plus' or '+' for
        addition and 'minus' or '-' for subtraction.
    attr : str
        Attribute to apply the operation on. Default is 'img'

    """

    def __init__(self, path=None, op=None, attr=None):
        super(OpMeanImg, self).__init__()
        self.path = path
        self.op_str = op

        self.mean_img = np.load(file=self.path)

        if self.op_str == "+" or self.op_str.lower() == "plus":
            self.op = lambda x: x + self.mean_img  # pylint: disable=C0103
        elif self.op_str == "-" or self.op_str.lower() == "minus":
            self.op = lambda x: x - self.mean_img

        self.attr = "img" if attr is None else attr

    def __call__(self, packet, hpacket=None):
        img = getattr(packet, self.attr)
        img = self.op(img)
        setattr(packet, self.attr, img)

        return [packet]


class Pass(Filter):

    """Dummy filter to pass a packet to a next layer"""

    def __init__(self):
        """Pass

        """
        super(Pass, self).__init__()

    def __call__(self, packet, hpacket=None):
        """Forwards an input packet

        Parameters
        ----------
        packet : Packet
            Packet to by forward.
        hpacket : Packet
            Not used.

        Returns
        -------
        out : [Packet]
            List of forwarded packet.
        """
        return [packet]


class PathExpander(Filter):

    """"Updates paths string in given attributes"""

    parameters = [
        FP(name="base_path", value_type=str, required=False,
           help_msg="Path to be used as a prefix"),
        FP(name="attrs", value_type=list, required=True, default=None,
           help_msg="List of relevant attributes, that has to be prefixed \
           with selected path"),
        FP(name="base_path_attr", value_type=list, required=False,
           default=None, help_msg="The base path taken from a packet \
           attribute. If specified, shadows the base_path.")
    ]

    def __init__(self, attrs=None, base_path=None, base_path_attr=None):
        """Initializes PathExpander

        Parameters
        ----------
        attrs : list
            List of attributes to change the base-path.
        base_path : str,opt
            Can be shadowed by a base_path_attr.
        base_path_attr : str, opt
            The base path taken from a packet attribute. If specified,
            shadows the base_path.

        """
        super(PathExpander, self).__init__()
        self.attrs = attrs
        self.base_path = base_path
        self.base_path_attr = base_path_attr
        if self.base_path is None and self.base_path_attr is None:
            raise ValueError

    def __call__(self, packet, hpacket=None):
        """Changes a base path

        Parameters
        ----------
        packet : Packet
        hpacket : Packet
            Not used.

        Returns
        -------
        out : [Packet]
            List of single packet with changed base path of selected
            attributes.

        Raises
        ------
            AttributeError
                In a case an attribute is missing in a packet.

        """
        base_path = None
        if self.base_path_attr is None:
            base_path = self.base_path
        else:
            base_path = os.path.dirname(getattr(packet, self.base_path_attr))

        for attr in self.attrs:
            file_path = getattr(packet, attr)
            full_path = os.path.join(base_path, file_path)
            setattr(packet, attr, full_path)
        return [packet]


class PathRelativizer(Filter):

    """"Updates paths in given attributes to be relative"""

    parameters = [
        FP(name="base_path", value_type=str, required=True,
           help_msg="Path to be used as a prefix"),
        FP(name="attrs", value_type=list, required=True, default=None,
           help_msg="List of relevant attributes, that has to be prefixed \
           with selected path")
    ]

    def __init__(self, base_path, attrs):
        """Initializes PathRelativizer

        Parameters
        ----------
        base_path : str
        attrs : list
            List of attributes to change the base-path.

        """
        super(PathRelativizer, self).__init__()
        self.base_path = base_path
        self.attrs = attrs

    def __call__(self, packet, hpacket=None):
        """Changes a base path

        Parameters
        ----------
        packet : Packet
        hpacket : Packet
            Not used.

        Returns
        -------
        out : [Packet]
            List of single packet with changed base path of selected
            attributes.

        Raises
        ------
            AttributeError
                In a case an attribute is missing in a packet.

        """
        for attr in self.attrs:
            abs_path = getattr(packet, attr)
            rel_path = os.path.relpath(abs_path, self.base_path)

            setattr(packet, attr, rel_path)
        return [packet]


class Picker(Filter):

    """Picks every nth packet and calls arbitrary filter

    Packet could be picked based on count of packets (every nth)  or
    time duration (every ms). If both nth and ms are set, nth
    precedes the ms.

    Parameters
    ----------
    filter : Filter
        Filter object to be used on picked packet.
    nth : int
        Apply the filter on every nth packet. Precedes ms parameter if
        both set.
    ms : int
        Apply the filter every ms milliseconds. Nth is prior to ms.
    """

    parameters = [
        FP(name="filter", required=True, help_msg="Filter object to be used \
            on an incoming packet."),
        FP(name="nth", required=False,
           help_msg="Picks every nth packet, priors to ms if both set"),
        FP(name="ms", required=False, help_msg="Picks a packet every ms \
            milliseconds. nth priors to ms if both set.")
    ]

    def __init__(self, filter=None, nth=None, ms=None):
        super(Picker, self).__init__()
        self.filter = filter
        self.nth = nth
        self.nth_counter = 0
        self.ms = ms
        self.ms_start = 0

        if nth is not None:
            self.pick_condition = self._nth_condition
        elif ms is not None:
            self.pick_condition = self._ms_condition
        else:
            raise AttributeError

    def _nth_condition(self):
        self.nth_counter += 1
        if self.nth_counter == self.nth:
            self.nth_counter = 0
            return True
        return False

    def _ms_condition(self):
        ms_stop = time.time()
        dms = (ms_stop - self.ms_start) * 1000
        if dms > self.ms:
            self.ms_start = ms_stop
            return True
        return False

    def pick(self, packet, hpacket=None):
        """Picks a packet and calls a filter if nth or ms condition is
        fulfilled
        """

        if self.pick_condition():
            vpackets = self.filter(packet)
            for pckt in vpackets:
                pckt.picket = True
            return vpackets
        else:
            packet.picked = False
            return [packet]

    def __call__(self, packet, hpacket=None):
        return self.pick(packet, hpacket)


class PreviewImage(Filter):

    """Previews an image via OpenCV

    The preview is shown for a user given time interval or until
    a key is pressed.

    Parameters
    ----------
    attr : str
        Attribute to look for the image. Default is 'img'
    wait : int
        Duration of image preview in milliseconds (ms)
        wait = 0 preview till a key is pressed
    name : str
        name of the preview window
    name_attr : str
        Name of packet attribute to look for a preview window name
    val_scale : float
        Number to scale the image values with. Default is '1', ie.
        no scaling.
    Notes
    -----
        If no name or name_attr is given a random MD5 hash is used for a
        name.
        If both name and name_attr are defined, the precedence is
        name > name_attr.

    """

    parameters = [
        FP(name="attr", required=False, help_msg="Attribute to look for the \
        image. Default is 'img'."),
        FP(name="wait", required=False, help_msg="Defines a time interval to\
        preview an image in milliseconds (ms)"),
        FP(name="name", required=False, help_msg="Defines a window name an \
        image is previewed in"),
        FP(name="name_attr", required=False, help_msg="Defines a packet's \
        attribute name from which a window name should be read"),
        FP(name="val_scale", required=False, help_msg="Number to scale the \
        image values with. Default is '1', ie. no scaling.")
    ]

    def __init__(self, attr=None, wait=200, name=None, name_attr=None,
                 val_scale=1.):
        super(PreviewImage, self).__init__()

        self.wait = wait if wait is not None else 200

        if name is not None:
            self.name = name
        else:
            self.name = name if name_attr is not None \
                else "".join([random.choice(string.ascii_letters) for _ in range(8)])

        self.attr = "img" if attr is None else attr
        self.name_attr = name_attr
        self.val_scale = val_scale

        self.count = 0

    def preview(self, packet, hpacket=None):
        """Preview the packet's img attribute as an image

        In a case the attribute represents list of images, the first one
        (at index 0) is displayed.

        Parameters
        ----------
        packet: Packet
            expected attributes
            img : An image to preview.
        hpacket : Packet
            Not used.

        Returns
        -------
        out : [Packet]
            List of forwarded packet in.

        Raises
        ------
        AttributeError

        """

        # try:
        attr = getattr(packet, self.attr)
        if self.name is not None:
            name = self.name
        else:
            name = str(getattr(packet, self.name_attr))

        img = attr
        if isinstance(attr, (list,)):
            img = attr[0]

        cv2.imshow(name, img / self.val_scale)
        cv2.waitKey(self.wait)

        # except AttributeError:
        # self.log.exception(
        #     "Missing attribute in a packet: %s", self.name_attr)
        # raise

        return [packet]

    def __call__(self, packet, hpacket=None):
        return self.preview(packet, hpacket)


class PrintPacket(Filter):

    """Prints an incoming packet and pass it further

    Parameters
    ----------
    attrs : list, tuple
        Attributes to print. If none, all attributes are printed.
        Default is None.
    file_name : str
        File to print the content to.
        If None, prints to stdout, default is None.
    mode :
        File mode with possible values 'w', write, and 'a', append.

    """

    parameters = [
        FP(name="attrs", required=False, help_msg="Attributes to print. \
        If none, all attributes are printed. Default is None."),
        FP(name="file_name", required=False, help_msg="File to print the \
        content to. If None, prints to stdout, default is None."),
        FP(name="mode", required=False, help_msg="File mode with possible \
        values 'w', write, and 'a', append.")
    ]

    def __init__(self, attrs=None, file_name=None, mode="w"):
        """PrintPacket ctor"""
        super(PrintPacket, self).__init__()
        self.attrs = attrs
        self.file_name = file_name
        self.mode = mode
        self.file_handler = None

    def __call__(self, packet, horizontal_packet=None):
        """Prints an incoming packet and forward it further

        Parameters
        ----------
        packet : Packet
            Printed packet.
        hpacket : Packet
            Not used.

        Returns
        -------
        out : [Packet]

        """

        if self.file_name is not None and self.file_handler is None:
            self.file_handler = open(self.file_name, mode=self.mode)
        elif self.file_name is None:
            self.file_handler = sys.stdout

        if self.attrs is None:
            packet_str = str(packet)
        else:
            str_list = [": ".join([attr, str(getattr(packet, attr))])
                        for attr in self.attrs]
            packet_str = "\n".join(str_list)

        print(packet_str, file=self.file_handler)
        self.file_handler.flush()

        return [packet]

    def dispose(self):
        if self.file_handler is not None and self.file_handler is not sys.stdout:
            self.file_handler.flush()
            self.file_handler.close()


class SelectAttr(Filter):

    """Keep given attributes only

    Parameters
    ----------
    attrs : tuple, list
        Attributes to keep.
    """

    def __init__(self, attrs=None):
        super(SelectAttr, self).__init__()
        self.attrs = attrs

    def select(self, packet):
        """Selects given attributes

        Parameters
        ----------
        packet : Packet
        """
        packet = Packet(**{attr: getattr(packet, attr) for attr in self.attrs})
        return [packet]

    def __call__(self, packet, hpacket=None):
        return self.select(packet)


class SetAttr(Filter):

    """Set packet's attribute

    Parameters
    ----------
    attr : str
        The attribute name.
    val :
        The value of attribute to be set.

    """
    parameters = [
        FP(name="attr", value_type=str, required=True,
           help_msg="The attribute name."),
        FP(name="val", required=True,
           help_msg="The value of attribute to be set.")
    ]

    def __init__(self, attr, val):
        super(SetAttr, self).__init__()
        self.attr = attr
        self.val = val

    def __call__(self, packet, hpacket=None):
        setattr(packet, self.attr, self.val)

        return [packet]


class SplitPacket(Filter):

    """Split a packet to original one and others based on a list of
    attribute lists

    Input
    -----
    packet : Packet()

    Attributes
    ----------
    split_attributes : list

    Returns
    -------
    out : list
        [packet, ...]

    """

    parameters = [
        FP(name="attributes", value_type=list,
           help_msg="List of list of attributes, every inner list defines a \
           packet based on selected attributes.", required=True)
    ]

    def __init__(self, attributes=None):
        """Constructor of SplitPacket filter

        Parameters
        ----------
        attributes : list
            List of attributes list based on which a packet is splitted.
            [[attr1, attr2], [attr3, attr4]]
            -> [packet_orig, packet_1, packet_2]

        """

        super(SplitPacket, self).__init__()
        self.attributes = attributes

    def __call__(self, packet, hpacket=None):
        """Copy a packet

        Parameters
        ----------
        packet : Packet
        hpacket : Packet
            Not used.

        Returns
        -------
        out : list
            [packet, ...l

        Raises
        ------
        AttributeError

        """
        packets = [packet]

        for list_attributes in self.attributes:
            keys = set(list_attributes).intersection(packet.__dict__)
            packet_content = {key: packet.__dict__[key] for key in keys}
            packets.append(Packet(**packet_content))

        return packets


class ThrowAway(Filter):
    """Throw packet away ie. do not send it further"""

    def __init__(self):
        super(ThrowAway, self).__init__()

    def __call__(self, packet, hpacket=None):
        return []


class ToCaffeBlob(Filter):

    """Reshape an ndarray to 3D caffe blob.

    Notes
    -----
    Image data shape [x,y,z] reshapes to [z,y,x]
    Image data shape [x,y] reshapes to [1,y,x]
    Vector data shape [x] reshapes to [1,1,x]
    Scalar number reshapes to a shape [1,1,1]

    Caffe blob is defined as [batch_num, channels, height, width], here
    only the part [channels, height, width] is considered.

    Parameters
    ----------
    attr : str
        Name of the data attribute
    out_attr : str
        Attribute name to store the transposed data to. Default is
        the attr.

    Out
    ---
    packet: Packet
        blob : ndarray
        Blob of a shape [z,y,x].

    Raises
    ------
    ValueError
        packet.attr can not be transformed to blob.
    AttributeError
    """
    parameters = [
        FP(name="attr", value_type=str, required=True,
           help_msg="Data attribute"),
        FP(name="out_attr", value_type=str, required=False,
           help_msg="Attribute name to store the transposed data to. "
           "Default is the attr.")
    ]

    def __init__(self, attr=None, out_attr=None):
        """ImageToBlob constructor"""

        super(ToCaffeBlob, self).__init__()
        self.attr = attr
        self.out_attr = out_attr if out_attr is not None else self.attr

    def __call__(self, packet, hpacket=None):

        data = getattr(packet, self.attr)
        dims = None
        if isinstance(data, numbers.Number):
            data = np.asarray([data]).reshape([1, 1, -1])
            dims = 1
        elif isinstance(data, np.ndarray):
            dims = len(data.shape)
            for _ in range(3 - len(data.shape)):
                # Add a leftmost dimension
                data = np.expand_dims(data, -1)
        else:
            raise ValueError

        blob = None
        if dims == 1:
            blob = data
        else:
            blob = data.transpose([2, 0, 1])

        setattr(packet, self.out_attr, blob)

        return [packet]
