# coding=utf-8
"""LMDB reader"""

from __future__ import division, print_function

import itertools

import numpy as np

import caffe
import lmdb
from ipp.filters import Filter
from ipp.filters import FilterParameter as FP
from ipp.filters import Packet

# pylint: disable=R0903
# pylint: disable=C0302


__all__ = [
    "LMDBReader"
]


class LMDBReader(Filter):

    """
    Reads the lmdb data

    Parameters
    ----------
    lmdbfiles : tuple, list
        List of paths to the lmdb files.
    loop : bool
        Loop through the files. Default is False.
    shuffle : bool
        Shuffle data. Should be avoided because of non sequential reading.
    attr : str
        Data attribute name.
    attr_label : str
        Label attribute name.

    Raises
    ------
    ValueError
    StopIteration
        Raised when all rows in all lmdb files were read.

    Note
    ----
    Unfortunately, this is related to protobuf definitions serializations
    in caffe, which is the reason of caffe_pb2.py
    (python compiled caffe.proto) dependency on caffe. A solution is
    to have a de-serializer component as a parameter of LMDBReader.

    """
    parameters = [
        FP(name="lmdbfiles", required=True, value_type=list,
           help_msg="List of lmdb environment (directory) paths"),
        FP(name="loop", required=False, help_msg="Flag to loop over lmdb file",
           default=False),
        FP(name="shuffle", required=False, default=False,
           help_msg="Flag to shuffle lmdb file"),
        FP(name="attr", required=False,
           help_msg="Data attribute name", default="img"),
        FP(name="label_attr", required=False,
           help_msg="Label attribute name", default="img_label"),
        FP(name="key_attr", required=False,
           help_msg="LMDB key attribute name", default="lmdb_key")
    ]

    def __init__(self, lmdbfiles=None, loop=False, shuffle=False, attr=None,
                 label_attr=None, key_attr=None):
        """Initializes LMBDReader"""
        super(LMDBReader, self).__init__()
        self.lmdbfiles = lmdbfiles
        self.loop = loop
        if loop:
            self.iter_lmdb = itertools.cycle(self.lmdbfiles)
        else:
            self.iter_lmdb = iter(self.lmdbfiles)

        self.shuffle = shuffle
        self.attr = attr if attr is not None else "img"
        self.label_attr = label_attr if label_attr is not None else "img_label"
        self.key_attr = key_attr if key_attr is not None else "lmdb_key"

        self.seed = 5
        self.rng = np.random.RandomState(self.seed)
        self.lmdb_file = next(self.iter_lmdb)
        self._init(self.lmdb_file)

    def _init(self, lmdbfile):
        # Environment represents the directory with data.mdb and lock.mdb
        self.log.info("Opening %s", lmdbfile)
        self.lmdb_env = lmdb.open(lmdbfile)
        self.entries = self.lmdb_env.stat()["entries"]
        # Concurrent operations
        self.lmdb_transaction = self.lmdb_env.begin()
        self.lmdb_cursor = self.lmdb_transaction.cursor()

        if self.shuffle:
            # Potentially a problem - memory overflow
            self.keys = [key for key, _ in self.lmdb_transaction.cursor()]
            self.rng.shuffle(self.keys)
            self.iter = iter(self.keys)
            self.next = self._get_shuffle
        else:
            self.iter = iter(self.lmdb_cursor)
            self.next = lambda: next(self.iter)

    def _get_shuffle(self):
        key = next(self.iter)
        return(key, self.lmdb_cursor.get(next(self.iter)))

    def read(self, packet):
        """Reads data from lmdb

        Parameters
        ----------
        packet : Packet
            not used
        hpacket not used

        Raises
        ------
        StopIteration
            Raised if EOF and loop is False.

        Returns
        -------
        out : [Packet]
            List of packet with all user selected h5 columns
            mapped to attributes.

        """

        try:
            key, value = self.next()
        except StopIteration:
            # If loop == false, automatically raises StopIteration
            self.lmdb_file = next(self.iter_lmdb)
            self.dispose()
            self._init(self.lmdb_file)
            key, value = self.next()

        datum = caffe.io.caffe_pb2.Datum()
        datum.ParseFromString(value)
        data = caffe.io.datum_to_array(datum)

        packet = Packet()
        setattr(packet, self.label_attr, datum.label)
        setattr(packet, self.key_attr, key)
        setattr(packet, self.attr, data)

        return [packet]

    def dispose(self):
        if hasattr(self, "lmdb_cursor"):
            self.lmdb_cursor.close()
        if hasattr(self, "lmdb_env"):
            self.lmdb_env.close()

        self.log.info("Closing %s", self.lmdb_file)

    def __call__(self, packet, hpacket=None):
        return self.read(packet)
