# coding=utf-8
"""Defines a single parameter class"""


class FilterParameter(object):

    """Encapsulates a single filter parameter including its name, type,
    default value, value, help string, and a flag if required.
    """

    def __init__(self, name=None, value_type=None, default=None, value=None,
                 required=False, help_msg=None):
        """Initialize parameter.

        Parameters
        ----------
        name : str
            Parameter name.
        value_type : typename
            Type of parameter value.
        default : arbitrary, optional
            Is the default value.
        value : arbitrary
            Parameter value.
        required : bool
            Flag if the parameter is required.
        help_msg str
            Parameter description.
        """
        self.name = name
        self.type = value_type
        self.default = default
        self.value = value if value is not None else default
        self.help_msg = help_msg
        self.required = required

    def parse(self, parameters_dict):
        """ Parse the dictionary of key: value, where key relates to
        self.name and value to self.value. If type is defined the value
        is cast. If the required flag is true, an exception is raised
        in a case the parameter is missing.

        Parameters
        ----------
        parameter_dict : dict
            All given parameters, where key corresponds to self.name
            and value.

        Raises
        ------
        AttributeError
            If a parameter is missing while it's required.
        TypeError
            In a case the provided value is not cast-able to self.type
        """
        if self.name in parameters_dict:
            # if self.type is not None:
            #     self.value = self.type(parameters_dict[self.name])
            # else:
                # self.value = parameters_dict[self.name]
            self.value = parameters_dict[self.name]
        elif self.required:
            exc_message = "Required {} is missing".format(self.name)
            raise AttributeError(exc_message)

    def __str__(self):
        """Returns a string representation"""

        return ("name: {}, value_type: {}, default: {}, value: {}, "
                "required: {}, help_msg: {}".
                format(self.name, self.type, self.default, self.value,
                       self.required, self.help_msg))

        # This does not print the attributes in a same sort as they are defined
        # return " ".join("{}: {}".format(key, value)
        #                 for key, value in self.__dict__.items())
