# coding=utf-8
"""Defines metaclass of Filters -- MetaFilter which registers the new
Filter type in a factory of filters whenever the filter or its module
is imported.

Caution: The __metaclass__ hook probably works in python 2.x only
Update: Python 3 use the class argument metaclass instead of
__metaclass__

Note:
See python metaclass programming:
http://python-3-patterns-idioms-test.readthedocs.io/en/latest/Metaprogramming.html
to understand the concept of metaclass creating class creating instance.

"""
from __future__ import print_function

import logging
from logging import NOTSET, addLevelName, getLoggerClass, setLoggerClass

from ..utils import Singleton
from ..utils.loging_levels import DEBUG_FILTER, TIME_FILTER, TIME_TRANSFER


class FilterLogger(getLoggerClass()):
    """FilterLogger provide specific logging levels """

    def __init__(self, name, level=NOTSET):
        super(FilterLogger, self).__init__(name, level)

        addLevelName(TIME_TRANSFER, "time_transfer")
        addLevelName(TIME_FILTER, "time_filter")
        addLevelName(DEBUG_FILTER, "debug_filter")

    def time_transfer(self, msg, *args, **kwargs):
        """Logging readers time level"""
        if self.isEnabledFor(TIME_TRANSFER):
            self._log(TIME_TRANSFER, msg, args, **kwargs)

    def time_filter(self, msg, *args, **kwargs):
        """Logging filter time level"""
        if self.isEnabledFor(TIME_FILTER):
            self._log(TIME_FILTER, msg, args, **kwargs)

    def debug_filter(self, msg, *args, **kwargs):
        """Logging filter debug level"""
        if self.isEnabledFor(DEBUG_FILTER):
            self._log(DEBUG_FILTER, msg, args, **kwargs)


setLoggerClass(FilterLogger)

LOGGER = logging.getLogger(__name__.split(".")[0] + ".MetaRegister")


def add_metaclass(metaclass):
    """Class decorator for creating a class with a metaclass.
    Taken from https://github.com/benjaminp/six
    """
    def wrapper(cls):
        """Class decorator"""
        orig_vars = cls.__dict__.copy()
        slots = orig_vars.get('__slots__')
        if slots is not None:
            if isinstance(slots, str):
                slots = [slots]
            for slots_var in slots:
                orig_vars.pop(slots_var)
        orig_vars.pop('__dict__', None)
        orig_vars.pop('__weakref__', None)
        return metaclass(cls.__name__, cls.__bases__, orig_vars)
    return wrapper


@add_metaclass(Singleton)
class FilterFactory(object):  # , metaclass=Singleton):

    """Filter factory registers and creates filters.

    Implemented as singleton --- provided by a metaclass
    """
    # __metaclass__ = Singleton

    def __init__(self):
        factory_name = type(self).__name__
        self.log = logging.getLogger(
            ".".join([__name__.split(".")[0], factory_name]))
        self._filters = {}

    def register(self, filter_name, filter_cls):
        """Register a filter name"""
        if filter_name not in self._filters:
            self.log.debug("Register %s of type %s",
                           filter_name, filter_cls)
            self._filters[filter_name] = filter_cls

    def create(self, filter_name, **kwargs):
        """Constructs a filter based on its filter_name and initializes
        it with provided parameters **kwargs. Method raises an exception
        in a case of an attempt to create a non-registered filter.

        Parameters
        ----------
        filter_name : str
            Filter name
        **kwargs
            Filter initialization parameters

        Exceptions
        ----------
        TypeError - the required filter is not registered
        """

        if filter_name in self._filters:
            return self._filters[filter_name](**kwargs)
        else:
            raise TypeError(filter_name)

    def __str__(self):
        return "\n".join([key for key in iter(self._filters)])


class MetaRegister(type):

    """Defines a metaclass which includes an automatic factory
    registration of objects inheriting from subclass Filter.
    The registration is done during importing the Filter -- creating its
    class.
    """
    def __new__(mcs, name, bases, nmspc):
        """Creates a class with the type, ie type(name, bases, dict)
        returning a class named name, inheriting from bases with
        attributes defined in dict

        Parameters:
        -----------
        name : string
            Class name
        bases : tuple
            All the classes the created class inherits from
        nmspc : dictionary
            Contains class body definitions
        """
        new_class = super(MetaRegister, mcs).__new__(mcs, name, bases, nmspc)
        LOGGER.debug("creates %s", name)
        factory = FilterFactory()
        factory.register(name, new_class)
        return new_class

    def __call__(cls, *args, **kwargs):
        """
        Copies the class attribute protected _parameters into instance
        attribute parameters.

        Note: Be aware of python name mangling when atr name starts with
        two underscores __
        """

        ipp_filter = super(MetaRegister, cls).__call__(*args, **kwargs)

        for parameter in ipp_filter.parameters:
            parameter.parse(kwargs)

        return ipp_filter


@add_metaclass(MetaRegister)
class Filter(object):  # , metaclass=MetaRegister):

    """Abstract Filter class every filter should inherit from

    __metaclass__ provides the autonomous Factory registration
    Defines and initializes the filter attributes.
    """
    # __metaclass__ = MetaRegister

    parameters = []

    def __init__(self, name=None):
        self.filter_name = name or type(self).__name__
        self._cache = {}

    @property
    def log(self):
        """Lazy initialized logger"""
        try:
            return self._cache["log"]
        except KeyError:
            package_name = __name__.split(".")[0:-1]
            package_name.append(self.filter_name)
            self._cache["log"] = logging.getLogger().getChild(
                ".".join(package_name))
        return self._cache["log"]

    def help(self):
        """Prints Filter's help for its parameters."""
        parameters = "\n".join(str(param) for param in self.parameters)
        return type(self).__name__ + "\n" + parameters

    def __str__(self):
        return "\n".join("{}: {}".format(key, value)
                         for key, value in self.__dict__.items())

    def dispose(self):
        """Routines needed to dispose an object

        Note
        ----
            Strictly managed by a Pipeline Context Manager.
            May change in the future.
        """
        pass
