# coding=utf-8
"""Ipt str parser"""

import logging
import numpy as np
import cv2
from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP
import inno.ml_tools.ipt_face as ipt_face


__all__ = [
    "IPTStrParser"
]


class IPTStrParser(Filter):
    """
    Filter performing parsing of specific coordinates
    from ipt_str to packet.source_pts (they are stored in list)
    """

    parameters = [
        FP(name="pts_to_extract", required=True,
           help_msg="Points to be extracted from ipt string."),
        FP(name="attr", help_msg="Packet's ipt string attribute", required=False),
        FP(name="out_attr", help_msg="Packet's attribute with the points of interest",
           required=False)
    ]

    def __init__(self, pts_to_extract, attr=None, out_attr=None):
        """Initializes the filter

        Parameters:
        ----------
            pts_to_extract: list of point identifiers
                Points to be extracted from ipt string

        """
        super(IPTStrParser, self).__init__()

        self.attr = attr if attr is not None else "ipt_str"
        self.out_attr = out_attr if out_attr is not None else "source_pts"
        if len(pts_to_extract) == 0:
            exc_message = "pts_to_extract parameter is wrong"
            raise AttributeError(exc_message)

        self.pts_to_extract = pts_to_extract

    def extract(self, packet, hpacket=None):
        """
        TODO: add packet description
        """
        try:
            ipt_str = getattr(packet, self.attr)

            ipt = ipt_face.IptFace()
            ipt.from_string(ipt_str)

            source_pts = [ipt.get(ipt_pt) for ipt_pt in self.pts_to_extract]
            setattr(packet, self.out_attr, source_pts)

            return [packet]

        except ValueError:
            self.log.exception(
                'Value error at {} filter'.format(self.__class__))
            raise
        except AttributeError:
            self.log.exception(
                "Attribute error at {} filter".format(self.__class__))
            raise

    def __call__(self, packet, hpacket=None):
        """Returns the transformed image"""
        return self.extract(packet, hpacket)
