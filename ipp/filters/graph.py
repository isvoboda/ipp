# coding=utf-8
"""Graph plotting filter"""

from __future__ import division, print_function

from collections import deque
from itertools import cycle

import matplotlib  # pylint: disable=c0411

# To disable X-backend
# Once in the future it is probably better to use :
# http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.switch_backend

# matplotlib.use("Agg")

import matplotlib.pyplot as plt  # pylint: disable=c0411,c0413
import numpy as np

from .filter_factory import Filter  # pylint: disable=c0413
from .filter_parameter import FilterParameter as FP  # pylint: disable=c0413


__all__ = [
    "Graph"
]


class Graph(Filter):

    """Graph plotting filter

    Parameters
    ----------
    name : str
        Graph name and file name without suffix.
    attr_y : str
        Attribute name used as y value and y axis label as well.
    attr_x : str
        Attribute name used as x value and x axis label as well.
    attr_label : str
        Packet's attribute name with which the plotted data are labeled.
    nth : int, optional
        Graph is redrawn every nth sample.
    limit_y : tuple, optional
        A tuple of minimum and maximum y of y axis shown in a graph.
        If it is not provided, the axis is dynamically scaled.
        Default is None.
    limit_x : tuple, optional
        A tuple of minimum and maximum x of x axis shown in a graph.
        If it is not provided, the axis is dynamically scaled.
        Default is None.
    scale_y : str, optional
        Set the scale of y axis. Default is 'linear'. Options are:
         'linear', 'log', 'logit', 'symlog'
    scale_x : str, optional
        Set the scale of x axis. Default is 'linear'. Options are:
         'linear', 'log', 'logit', 'symlog'
    mavg : list, tuple
        A list of moving average sizes. Size per curve. Default size
        is 10.
    filetype : str, optional
        Suffix the graph is saved with. It could be: 'pdf', 'png'.
        Default is 'pdf'
    plot_size : tuple, optional
        Plot size in inches. Default is (8,5).
    ticks_loc_y : list, tuple
        List of y ticks locations
    ticks_label_y : list, tuple
        List of y ticks labels
    ticks_loc_x : list, tuple
        List of x ticks locations
    ticks_label_x : list, tuple
        List of x ticks labels
    grid_y : bool
        If True, draws y grid. Default is None.
    dpi : int, optional
        Dots per inch.

    """

    parameters = [
        FP(name="name", required=True,
           help_msg="Graph name and file name without suffix."),
        FP(name="attr_y", required=True,
           help_msg="Attribute name used as y value and y axis label \
           as well."),
        FP(name="attr_x", required=True,
           help_msg="Attribute name used as x value and x axis label \
           as well."),
        FP(name="attr_label", required=True,
           help_msg="Packet's attribute name with which the plotted data are \
           labeled."),
        FP(name="nth", required=False,
           help_msg="Graph is redrawn every nth sample."),
        FP(name="limit_y", required=False,
           help_msg="A tuple of minimum and maximum y of y axis shown in a \
           graph."),
        FP(name="limit_x", required=False,
           help_msg="A tuple of minimum and maximum x of x axis shown in a \
           graph."),
        FP(name="scale_y", required=False,
           help_msg="Set the scale of y axis. Default is 'linear'. Options \
            are: 'linear', 'log', 'logit', 'symlog'"),
        FP(name="scale_x", required=False,
           help_msg="Set the scale of x axis. Default is 'linear'. Options \
           are: 'linear', log', 'logit', 'symlog'"),
        FP(name="mavg", required=False,
           help_msg="A moving average size, ie. how many samples is used to \
           compute the average from. Default is 10."),
        FP(name="filetype", required=False,
           help_msg="Suffix the graph is saved with. It could be 'pdf', \
           'png'. Default is 'pdf'", default="pdf"),
        FP(name="plot_size", required=False, default=(8, 5),
           help_msg="Plot size in inches. Default is (8,5)."),
        FP(name="ticks_loc_y", required=False,
           help_msg="List of y ticks locations."),
        FP(name="ticks_label_y", required=False,
           help_msg="List of y ticks labels."),
        FP(name="ticks_loc_x", required=False,
           help_msg="List of x ticks locations."),
        FP(name="ticks_label_x", required=False,
           help_msg="List of x ticks labels."),
        FP(name="grid_y", required=False, default=None,
           help_msg="If True, draws y grid. Default is None."),
        FP(name="logging", required=False, default=True,
           help_msg="Whether a log of the plotted values should be saved.")
    ]

    def __init__(self, name=None, attr_y=None, attr_x=None, attr_label=None,
                 nth=1, limit_y=(None, None), limit_x=(None, None),
                 scale_y=None, scale_x=None, mavg=None,
                 filetype=None, plot_size=(8, 5), ticks_loc_y=None,
                 ticks_label_y=None, ticks_loc_x=None, ticks_label_x=None,
                 grid_y=None, dpi=None, logging=True):

        super(Graph, self).__init__()

        self.name = name

        self.figure = plt.figure(num=None, figsize=plot_size, dpi=dpi,
                                 facecolor=None, edgecolor=None)

        self.ax = self.figure.add_subplot(111)  # pylint: disable=c0103

        self.attr_y = attr_y
        self.attr_x = attr_x
        self.attr_label = attr_label

        self.nth = nth

        self.limit_y = limit_y
        self.limit_x = limit_x

        self.scale_y = "linear" if scale_y is None else scale_y
        self.scale_x = "linear" if scale_x is None else scale_x

        self.default_mavg = 10
        self.mavg = [self.default_mavg] if mavg is None else mavg
        self.it_mavg = iter(self.mavg)

        self.filetype = filetype if filetype is not None else "pdf"
        self.graph_name = ".".join([self.name, self.filetype])
        self.logging = logging

        self.ticks_loc_y = ticks_loc_y
        self.ticks_label_y = ticks_label_y
        self.ticks_loc_x = ticks_loc_x
        self.ticks_label_x = ticks_label_x

        self.grid_y = grid_y

        self.colors_it = cycle("bgrcmk")
        self.register_label = {}

        self.ith = 0


    def plot(self, packet):
        """Plots accumulated data"""

        val = getattr(packet, self.attr_y)
        if np.isinf(val):
            self.log.error("%s received in %s", val, self.attr_y)
            return

        i_val = getattr(packet, self.attr_x)
        label = getattr(packet, self.attr_label)

        if label not in self.register_label:
            self.register_label[label] = {"color": next(self.colors_it)}
            self.register_label[label]["history"] = []
            try:
                mavg = next(self.it_mavg)
            except StopIteration:
                mavg = self.default_mavg
            self.register_label[label]["last_data"] = deque(maxlen=mavg)

        color = self.register_label[label]["color"]
        last_data = self.register_label[label]["last_data"]
        history = self.register_label[label]["history"]

        last_data.append(val)
        mean_val = np.mean(last_data)
        history.append((i_val, mean_val))

        if self.ith % self.nth == 0:
            self.ax.cla()
            self.ax.set_xlim(
                left=self.limit_x[0], right=self.limit_x[1],
                auto=self.limit_x[0] is None)
            self.ax.set_ylim(
                bottom=self.limit_y[0], top=self.limit_y[1],
                auto=self.limit_y[0] is None)

            self.ax.set_yscale(self.scale_y)
            self.ax.set_xscale(self.scale_x)

            self.ax.set_title(self.name)

            self.ax.set_ylabel(self.attr_y)
            self.ax.set_xlabel(self.attr_x)

            if self.ticks_loc_y is not None:
                self.ax.set_yticks(self.ticks_loc_y)
            if self.ticks_label_y is not None:
                self.ax.set_yticklabels(self.ticks_label_y)
            if self.ticks_loc_x is not None:
                self.ax.set_xticks(self.ticks_loc_x)
            if self.ticks_label_x is not None:
                self.ax.set_xticklabels(self.ticks_label_x)
            if self.grid_y is True:
                self.ax.get_yaxis().grid(True)

            loggables = []
            for key in self.register_label.iterkeys():
                color = self.register_label[key]["color"]
                history = self.register_label[key]["history"]
                (x, y) = zip(*history)  # pylint: disable=c0103
                mavg = len(self.register_label[key]["last_data"])
                self.ax.plot(
                    x, y, color, label="_".join([key, str(mavg), "mavg"]))
                loggables.append((key, x, y))

            self.ax.legend()
            self.figure.savefig(self.graph_name)

            if self.logging:
                with open('{}.log'.format(self.name), 'w+') as logfile:
                    for (lbl, x, y) in loggables:
                        logfile.write('{}\n{}\n{}\n'.format(
                            lbl, str(x)[1:-1], str(y)[1:-1])
                        )


        self.ith += 1

    def dispose(self):
        """Closes the figure"""
        plt.close(self.figure)
        self.log.info("Plot %s closed", self.graph_name)

    def __call__(self, packet, hpacket=None):
        if hasattr(packet, self.attr_y):
            self.plot(packet)
        return [packet]
