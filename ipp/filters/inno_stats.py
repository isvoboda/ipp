# coding=utf-8
"""Filters for generating inno statistics reports"""

from __future__ import division
from __future__ import print_function

import logging
import numpy as np
from terminaltables import AsciiTable

from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP

import inno.evaluation.defs as defs
import inno.ml_tools.inno_dict as idict
import inno.evaluation.testers.classification_tester as classification_tester
import inno.evaluation.testers.regression_tester as regression_tester
import inno.evaluation.reports as rep


__all__ = [
    "InnoStatistics",
    "RegressionStatistics",
    "ClassificationStatistics",
    "MCCConfusionMatrix"
]


class InnoStatistics(Filter):

    """Filter for creating statistics report"""

    parameters = [
        FP(name="gt", value_type=str, required=True,
           help_msg="Name of the ground truth data attribute."),
        FP(name="predicted", value_type=str, required=True,
           help_msg="Name of the predicted data attribute."),
    ]

    def __init__(self, gt, predicted):
        """RegressionStatistics constructor

        Parameters
        ----------
        gt : str
            Name of the ground truth data attribute.
        predicted : str
            Name of the predicted data attribute.
        """

        super(InnoStatistics, self).__init__()

        self.gt = gt
        self.predicted = predicted
        self.gts, self.scores = [], []

    def update_data(self, packet, hpacket):
        """Updates the ground truth and predicted data list

        Parameters
        ----------
        packet : Packet
        hpacket: not used

        Returns
        -------
        out : [Packet]
              Unchanged packet
        """
        try:
            self.gts.append(getattr(packet, self.gt))
            self.scores.append(getattr(packet, self.predicted))
        except AttributeError:
            self.log.exception("Attribute error at %s filter", self.__class__)
            raise

        return [packet]

    def __call__(self, packet, hpacket):
        return self.update_data(packet, hpacket)


class RegressionStatistics(InnoStatistics):

    """Filter for creating regression statistics report"""

    def dispose(self):
        """Calls the statistics evaluation"""
        if len(self.gts) > 0:
            data_non_predictable_stat, data_regression_stat = regression_tester.evaluate(
                self.gts, self.scores, levels=[2, 5, 10, 15, 20, 25, 30],
                include_non_predictable=False)
            d = defs.test()
            report = idict.InnoDict()
            report.set_at_path(d.non_predictable_stat,
                               data_non_predictable_stat)
            report.set_at_path(d.regression_stat, data_regression_stat)

            rep.std_out(report)


class ClassificationStatistics(InnoStatistics):

    """Filter for creating classification statistics report"""

    parameters = [
        FP(name="gt", value_type=str, required=True,
           help_msg="Name of the ground truth data attribute."),
        FP(name="predicted", value_type=str, required=True,
           help_msg="Name of the predicted data attribute."),
        FP(name="pairs", value_type=list, required=True,
           help_msg="Pairs which will be used in classification tester."),
        FP(name="far_levels", value_type=list, required=False, default=[5, 10, 100, 500, 1000],
           help_msg="Far levels which will be computed and printed"),
    ]

    def __init__(self, gt, predicted, pairs, far_levels=None):
        """ClassificationStatistics constructor

        Parameters
        ----------
        gt : str
            Name of the ground truth data attribute.
        predicted : str
            Name of the predicted data attribute.
        pairs: list
            Pairs which will be used in classification tester.
        far_levels : list
            Far levels which will be computed and printed.
        """

        super(ClassificationStatistics, self).__init__(gt, predicted)
        self.pairs = pairs
        self.far_levels = far_levels if far_levels is not None else [5, 10, 100, 500, 1000]

    def dispose(self):
        """Calls the statistics evaluation"""
        if len(self.gts) > 0:
            non_predictable_stat, classify_frr_far_stat, dual_class_stat, conf_matrix = \
                classification_tester.evaluate(self.gts, self.scores, self.far_levels, self.pairs, True)
            d = defs.test()
            report = idict.InnoDict()
            report.set_at_path(d.non_predictable_stat, non_predictable_stat)
            report.set_at_path(d.dual_class_stat, dual_class_stat)
            report.set_at_path(d.conf_matrix_stat, conf_matrix)
            report.set_at_path(d.classify_frr_far_stat, classify_frr_far_stat)
            rep.std_out(report)


class MCCConfusionMatrix(InnoStatistics):

    "Filter for creating a multi-correct-class confusion matrix"

    parameters = [
        FP(name="gt", value_type=str, required=True,
           help_msg="Name of the ground truth data attribute."),
        FP(name="predicted", value_type=str, required=True,
           help_msg="Name of the predicted data attribute."),
        FP(name="mapping", value_type=dict, required=True,
           help_msg="Dictionary for mapping between the predictions and gt - "
                    "key represents the class to be replaced with the collection of correct alternatives."),
    ]

    def __init__(self, gt, predicted, mapping):
        """ClassificationStatistics constructor

        Parameters
        ----------
        gt : str
            Name of the ground truth data attribute.
        predicted : str
            Name of the predicted data attribute.
        mapping: dict
            Dictionary for mapping between the prediction and gt.
        """

        super(MCCConfusionMatrix, self).__init__(gt, predicted)
        self.mapping = mapping

    def dispose(self):
        """Calls the statistics evaluation"""
        class_num = max(max(self.gts), max(self.scores))+1
        cmatrix = [[0]*class_num for _ in range(class_num)]

        ok = 0
        for idx, gt in enumerate(self.gts):
            correct = self.mapping[gt] if gt in self.mapping else [gt]
            if self.scores[idx] in correct:
                ok += 1
                cmatrix[gt][gt] += 1
            else:
                cmatrix[gt][self.scores[idx]] += 1

        assert np.sum(cmatrix) == len(self.gts)

        npc, last_row = np.array(cmatrix), ['SUM']
        cmatrix.insert(0, ['GT\\P']+list(range(class_num))+['SUM'])
        for i in range(class_num):
            total = sum(npc[i,:])
            cmatrix[i+1] = map(lambda x: '{} ({:.1f}%)'.format(x, x/total*100), cmatrix[i+1])
            cmatrix[i+1] = [i]+cmatrix[i+1]+[total]
            last_row.append(sum(npc[:,i]))
        cmatrix.append(last_row)
        cmatrix[-1].append(len(self.gts))

        table = AsciiTable(cmatrix)
        table.title = 'MCC confusion'
        table.inner_footing_row_border = True
        print(table.table)
        print('Accuracy: {:.2f}%'.format(ok/len(self.gts)*100))
