# coding=utf-8
"""Crop filters"""

from __future__ import division, print_function, unicode_literals

import numbers
import sys

import numpy as np

from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP
from .filters import ContinueIteration

# import cv2

__all__ = [
    "Crop"
]

# pylint: disable=E0602,W0613,W0622,C0103

# ToDo: taken from SIX lib, SIX should be integrated directly
PY3 = sys.version_info[0] == 3

if PY3:
    string_types = (str,)
else:
    string_types = (basestring,)


class Crop(Filter):

    """Crops an image

    Filter is based on numpy.pad function.

    Parameters
    ----------
    attr : str
        Packet's attribute to crop
    shape : list, tuple, str
        Defines a size of a crop:
        (height, width) : fixed crop size
        ((height, width), (height, width)) : random generated
            crop (from, to)
        "square" : max inscribed square of an image
    center : list, tuple, str
        Defines a crop center:
        (y,x) : fixed coordinates of crop center
        "random" : random center coordinates to fit the crop size
            in an image
        "center" : always take a crop from an image center
    shape_attr : str
        Defines a packet attribute to look for a crop shape.
        Is mutual with crop an center.
    center_attr : str
        Defines a packet attribute to look for a crop center.
        Is mutual with crop an center.
    pad_behavior : str
        Behavior of the padding. There are these possible values:
          "no_pad_exception" - padding is not applied. When the crop
          requires padding, the exception is thrown
          "no_pad_continue" - padding is not applied. When the crop
          requires padding, the processing continues with next packet
          "pad" - padding is applied
    pad_mode : str
        Padding mode. for available settings see:
        https://docs.scipy.org/doc/numpy/reference/generated/numpy.pad.html
        Default is mode='edge'
    pad_kwargs : dict
        Additional padding settings, for available settings see:
        https://docs.scipy.org/doc/numpy/reference/generated/numpy.pad.html
        Default is None
    inherit : bool
        Flag to define if packet shall use a crop size and coordinates
        from a previous filter in the same layer.
    seed : int
        Random number generator seed.

    Raises
    ------
    ValueError
        Raised if incompatible or missing settings.

    """

    parameters = [
        FP(name="attr", help_msg="Packet's attribute to crop", required=False),
        FP(name="out_attr", help_msg="Packet's attribute to store cropped "
           "image", required=False),

        FP(name="shape", help_msg="Crop shape. Can be defined as a list or "
           "tuple of given size, or list or tuple of [from, to) sizes, "
           "or as a string square (maximal inscribed square int the image).",
           required=False),

        FP(name="center", help_msg="Crop center coordinates. Can be defined "
           "as a list/tuple of given coordinates, or by a string 'random', or "
           "'center', ie. always crop from the image center.", required=False),

        FP(name="shape_attr", help_msg="Defines a packet attribute to look "
           "for a crop shape. Is mutual with crop an center.", required=False),

        FP(name="center_attr", help_msg="Defines a packet attribute to look "
           "for a crop center. Is mutual with crop an center.", required=False),

        FP(name="pad_behavior", help_msg="Behavior of the padding. There are "
           "these possible values: 'no_pad_exception' - padding is not "
           "applied. When the crop requires padding, the exception is thrown. "
           "'no_pad_continue' - padding is not applied. When the crop "
           "requires padding, the processing continues with next packet. "
           "'pad' - padding is applied", required=False),

        FP(name="inherit", help_msg="Flag to use crop size and coordinates "
           "from a previous crop filter in the same layer.", required=False),

        FP(name="pad_mode", help_msg="Padding mode, see "
           "https://docs.scipy.org/doc/numpy/reference/generated/numpy.pad.html "
           "for available modes. Default is mode='edge'", required=False),

        FP(name="pad_kwargs", help_msg="Additional padding settings, see "
           "https://docs.scipy.org/doc/numpy/reference/generated/numpy.pad.html "
           "for available settings. Default is None", required=False),

        FP(name="seed", help_msg="A seed value to initialize random number "
           "generator.", required=False)
    ]

    def __init__(self, attr=None, shape=None, center=None, out_attr=None,
                 shape_attr=None, center_attr=None,
                 pad_behavior="no_pad_exception", pad_mode=None,
                 pad_kwargs=None, inherit=False, seed=5):
        """Crop constructor"""

        super(Crop, self).__init__()

        self.attr = attr if attr is not None else "img"
        self.out_attr = out_attr if out_attr is not None else self.attr

        # Given crop shape and center
        self.shape = shape
        self.center = center

        # Crop shape and center attribute names
        self.shape_attr = shape_attr
        self.center_attr = center_attr

        # Pad behavior
        possible_pad_behavior = ["pad", "no_pad_exception", "no_pad_continue"]
        if pad_behavior not in possible_pad_behavior:
            self.log.error(
                "Unknown padding behavior, it can be one of %s", possible_pad_behavior)
            raise ValueError

        self.pad_behavior = pad_behavior

        self.pad_mode = pad_mode if pad_mode is not None else "edge"
        self.pad_kwargs = pad_kwargs

        # To be send horizontally
        self.crop_center = None
        self.crop_shape = None
        self.inherit = inherit

        self.rng = np.random.RandomState(seed)

        # Shape attr & center attr given
        if shape_attr is not None and center_attr is not None:
            self.from_packet = True

        # Shape & center given
        elif shape is not None and center is not None:
            self.from_packet = False
            if shape is not None and isinstance(shape, (list, tuple)):
                if isinstance(shape[0], (list, tuple)):
                    self.get_shape = self._get_random_size
                elif isinstance(shape[0], numbers.Number):
                    self.get_shape = self._get_size
                else:
                    self.log.error("Wrong shape[0] type: %s", type(shape[0]))
                    raise ValueError
            elif shape is not None and isinstance(shape, str):
                if shape == "square":
                    self.get_shape = self._get_square_size
                else:
                    self.log.error("Wrong shape type: %s", type(shape))
                    raise ValueError
            else:
                raise ValueError

            if isinstance(center, (tuple, list)):
                if isinstance(center[0], numbers.Number):
                    self.get_center = self._get_fixed_center
                else:
                    self.log.error("Wrong center[0] type: %s", type(center[0]))
                    raise ValueError
            elif isinstance(center, string_types):
                if center == "center":
                    self.get_center = self._get_center
                elif center == "random":
                    self.get_center = self._get_random_center
            else:
                self.log.error("Wrong center type: %s", type(center))
                raise ValueError

        else:
            self.log.error("Wrong initialization.")
            raise ValueError

    def _get_size(self, img=None):
        """Returns user defined crop size"""
        return self.shape

    def _get_square_size(self, img):
        """Returns a largest inscribed square crop

        Parameters
        ----------
        img : ndarray
            An image to find a largest inscribed image in.

        Returns
        -------
        out : tuple
            Max inscribed square.
        """

        val = img.shape[0] if img.shape[0] < img.shape[1] else img.shape[1]
        return (val, val)

    def _get_random_size(self, img=None):
        """Returns a random crop size

        Parameters
        ----------
        img : ndarray
            Not used.
        """

        crop_from = self.shape[0]  # min width and height
        crop_to = self.shape[1]  # max width and height

        random_shape = self._get_random_coord(crop_from, crop_to)
        return random_shape

    def _get_center(self, img, shape=None):
        """Returns an image center

        Parameters
        ----------
        img : ndarray
            An image to compute the center from.
        shape : tuple, list
            Not used.

        Returns
        -------
        out : tuple
            Image center. It is always a truncate int.
        """

        center = [val // 2 for val in img.shape]

        return center

    def _get_fixed_center(self, img=None, shape=None):
        """Returns user defined center coordinates

        Parameters
        ----------
        img : ndarray
            Not used
        shape : tuple, list
            Not used

        Returns
        -------
        out : tuple
            User given center.

        """
        return self.center

    def _get_random_center(self, img, shape):
        """Returns random center coordinates

        Parameters
        ----------
        img: ndarray
            An image to set an interval to sample a crop center of
            defined size from so that it fits to this image.
        shape: tuple, list
            Crop shape.

        Returns
        -------
        out: tuple
            Random sampled center of crop which fits to the image.

        """

        first = [shape[i] // 2 for i in range(2)]
        second = [img.shape[i] - (shape[i] - first[i]) for i in range(2)]

        center = self._get_random_coord(first, second)
        return center

    def _check_crop_fits_to_image(self, img_size, crop_size):
        """Checks a crop fits to an image

        Parameters
        ----------
        img_size : tuple, list
        crop_size : tuple, list

        Raises
        ------
        ValueError
        If crop does not fit to an image.

        """
        if crop_size[0] > img_size[1] or crop_size[1] > img_size[1]:
            self.log.error("Crop [%d, %d] is bigger compared to the "
                           "image [%d, %d]", crop_size[0], crop_size[1],
                           img_size[0], img_size[1])
            raise ValueError

    def _get_random_coord(self, coord_from, coord_to):
        """Returns randomly sampled coordinates with an uniform
        distribution from a given interval.

        Parameters
        ----------
        coord_from : tuple, list
            Beginning closed interval.
        coord_to : tuple, list
            End open interval.

        Returns
        -------
        out : tuple
            Randomly sampled coordinates from a given interval with
            an uniform distribution.

        """
        rand_x = self.rng.randint(coord_from[0], coord_to[0])
        rand_y = self.rng.randint(coord_from[1], coord_to[1])
        return (rand_x, rand_y)

    def crop(self, img=None, center=None, shape=None):
        """Crops an image with a given crop center and shape.

        Parameters
        ----------
        img : ndarray
            An image to crop.
        center : tuple, list
            Center crop.
        shape : tuple, list
            Crop size.

        Returns
        -------
        out : ndarray
            An image crop.

        """

        shape_half = [(shape[i] // 2) for i in range(2)]
        start = [center[i] - shape_half[i] for i in range(2)]
        end = [start[i] + shape[i] for i in range(2)]

        pad_l_u = [val * -1 if val < 0 else 0 for val in start]
        pad_r_d = [val - img.shape[i] if val > (img.shape[i]) else 0
                   for i, val in enumerate(end)]

        for i in range(2):
            if pad_l_u[i] > 0:
                start[i] = 0
                end[i] = end[i] + pad_l_u[i]

        want_pad = self.pad_behavior == "pad"

        if ((any(val_pad > 0 for val_pad in pad_l_u) or
             any(val_pad > 0 for val_pad in pad_r_d)) and
                want_pad is False):
            self.log.error("Crop [%s, %s is out of image %s and padding is \
                not set.", start, end, img.shape)

            if self.pad_behavior == "no_pad_exception":
                raise ValueError
            if self.pad_behavior == "no_pad_continue":
                raise ContinueIteration

        padding = list(zip(pad_l_u, pad_r_d))

        for _ in range(len(img.shape) - len(padding)):
            padding.append((0, 0))

        pad_img = np.pad(img,
                         padding,
                         mode=self.pad_mode,
                         **self.pad_kwargs if self.pad_kwargs is not None
                         else {})
        crop = pad_img[start[0]:end[0], start[1]:end[1]]
        return crop

    def crop_img(self, packet, hpacket):
        """Crops an image in a given packet

        Parameters
        ----------
        packet : Packet
            Packet with an image attribute to crop from.abs
        hpacket : Packet
            Packet from the previous crop operation, if it contains
            a crop coordinates hpacket.crop_size and hpacket.crop_center
            these can be used if inherit is True.

        Returns
        -------
        out : [Packet]
            List of packet with a cropped image in packet.img attribute.

        Raises
        ------
        ValueError

        """

        shape = None
        center = None

        img = getattr(packet, self.attr)

        if self.inherit:
            if hasattr(hpacket, "crop_shape") \
                    and hasattr(hpacket, "crop_center"):
                shape = hpacket.crop_shape
                center = hpacket.crop_center
            else:
                self.log.warning("No crop parameters from a previous crop \
                filter, Not defined operation.")
        elif self.from_packet:
            shape = getattr(packet, self.shape_attr)
            center = getattr(packet, self.center_attr)
        else:
            shape = self.get_shape(img)
            center = self.get_center(img, shape)

        packet.crop_shape = shape
        packet.crop_center = center

        crop = self.crop(img=img, center=center, shape=shape)
        #packet.img = crop
        setattr(packet, self.out_attr, crop)

        return [packet]

    def __call__(self, packet, hpacket=None):
        return self.crop_img(packet, hpacket)
