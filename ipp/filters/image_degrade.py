# coding=utf-8
"Filters for degrading image quality"

from __future__ import division, print_function, unicode_literals

import cv2
import numpy as np

from ..utils import LazyInit
from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP

__all__ = [
    "Blur",
    "Brightness",
    "GaussianNoise",
    "SaltPepperNoise",
    "ToColor",
    "ToGray"
]

# pylint: disable=W0613

class Blur(Filter):

    """Filter blurring input image using gaussian blur

    Note
    ----
    In a case the img attribute contains a list or tuple, all the items
    are blurred.

    Parameters
    ----------
    kernel : int, tuple,list
        Single number or interval to sample square kernel from.
        It is always odd.
    sigma : int, tuple, list
        Single number or interval to sample sigma from.
    seed : int
    """
    parameters = [
        FP(name="kernel", value_type=tuple,
           help_msg="Gaussian kernel size interval - positive, \
           sampled odd integer"),
        FP(name="sigma", value_type=tuple,
           help_msg="Gaussian kernel standard deviation interval to \
           sample from."),
        FP(name="seed", value_type=tuple,
           help_msg="Random number generator seed."),
        FP(name="attr", default="img", value_type=str,
           help_msg="Attribute of the package containing the image."),
        FP(name="attr_out", default="img", value_type=str,
           help_msg="Attribute to store the blurred image to.")
    ]

    def __init__(self, kernel=5, sigma=0, seed=5, attr="img", attr_out=None):
        """Initializes the filter"""

        super(Blur, self).__init__()
        self.kernel = kernel
        self.sigma = sigma
        self.rng = np.random.RandomState(seed)

        self.attr = attr
        self.attr_out = attr_out or attr

    def _lazy_init(self, kernel, sigma):
        if isinstance(kernel, (tuple, list)):
            self.sample_kernel = self._sample_kernel_odd
        else:
            self.sample_kernel = lambda x: x

        if isinstance(sigma, (tuple, list)):
            self.sample_sigma = lambda x: self.rng.uniform(x[0], x[1])
        else:
            self.sample_sigma = lambda x: x


    def _sample_kernel_odd(self, kernel):
        sample = self.rng.randint(low=kernel[0], high=kernel[1])
        sampled_kernel = sample if sample % 2 is 1 else sample + 1
        return sampled_kernel

    @LazyInit("_lazy_init")
    def blur(self, packet, hpacket=None):
        """Performs gaussian blur on input image

        Parameters
        ----------
        packet : Packet
        expected attributes:
            img : ndarray
                Input image

        Raises
        ------
        ValueError
            raised when input data is invalid
        AttributeError
            raised when packet doesn't contain the required attributes

        Returns
        -------
        out : [Packet]
              List of Packet with the transformed image.
        """
        try:

            try:
                kernel = self.sample_kernel(self.kernel)
            except AttributeError:
                self._lazy_init(self.kernel, self.sigma)
                kernel = self.sample_kernel(self.kernel)

            sigma = self.sample_sigma(self.sigma)
            # self.log.debug(
            #     "Sampled kernel: %d, sampled sigma: %f", kernel, sigma)
            in_img = getattr(packet, self.attr)

            if isinstance(in_img, (list, tuple)):
                out_img = []
                for item_img in in_img:
                    out_img.append(cv2.GaussianBlur(item_img, (kernel, kernel), sigma))
                    kernel = self.sample_kernel(self.kernel)
                    sigma = self.sample_sigma(self.sigma)
            else:
                out_img = cv2.GaussianBlur(in_img, (kernel, kernel), sigma)

            setattr(packet, self.attr_out, out_img)
            return [packet]

        except ValueError:
            self.log.exception('Value error at %s filter', self.__class__)
            raise
        except AttributeError:
            self.log.exception("Attribute error at %s filter", self.__class__)
            raise

    def __call__(self, packet, hpacket):
        """Returns the transformed image"""
        return self.blur(packet, hpacket)


class Brightness(Filter):

    """Filter adjusting the brightness of the input image"""
    parameters = [
        FP(name="gamma", default=.5, value_type=tuple,
           help_msg="Gamma correction value"),
        FP(name="seed", value_type=tuple, default=5,
           help_msg="Random number generator seed."),
        FP(name="attr", default="img", value_type=str,
           help_msg="Attribute of the package containing the image.")
    ]

    def __init__(self, gamma=.5, seed=5, attr="img"):
        """Initializes the filter"""
        super(Brightness, self).__init__()

        self.rng = np.random.RandomState(seed)
        self.gamma = gamma
        self.attr = attr

    def _lazy_init(self, gamma):
        if isinstance(gamma, (tuple, list)):
            self.sample_gamma = lambda x: self.rng.uniform(gamma[0], gamma[1])
        else:
            self.sample_gamma = lambda x: x

    @LazyInit("_lazy_init")
    def transform(self, packet, hpacket):
        """Performs brightness operation on input image.

        Parameters
        ----------
        packet : Packet
        expected attributes:
            img : ndarray
                Input image, consisting of integer-typed values

        Exceptions
        ----------
        ValueError - raised when input data is invalid
        AttributeError - raised when packet doesn't contain
        the required attributes

        Returns
        -------
        out : [Packet]
              List of Packet with the transformed image
        """
        try:
            try:
                gamma = self.sample_gamma(self.gamma)
            except AttributeError:
                self._lazy_init(self.gamma)
                gamma = self.sample_gamma(self.gamma)

            lookup = np.array([((i/255) ** gamma) * 255
                               for i in range(0, 256)]).astype(np.uint8)
            img = cv2.LUT(getattr(packet, self.attr), lookup)
            setattr(packet, self.attr, img)
            return [packet]

        except ValueError:
            self.log.exception(
                'Value error at {} filter'.format(self.__class__))
            raise
        except AttributeError:
            self.log.exception(
                "Attribute error at {} filter".format(self.__class__))
            raise

    def __call__(self, packet, hpacket):
        """Returns the transformed image"""
        return self.transform(packet, hpacket)


class GaussianNoise(Filter):
    """Filter adding Gaussian noise

    Parameters
    ----------
    attr : str
        Attribute name to apply the noise to. Default is "img"
    std : float
        Standard deviation of distribution to sample the noise from.
    mean : float
        Mean of Gaussian-Normal distribution to sample noise from.
    clip: tuple
        If set, clip the values in a given interval. Default is None,
        ie do not clip.
    seed: int
    """

    parameters = [
        FP(name="attr", default="img",
           help_msg="Attribute name to apply the noise to."),
        FP(name="std", default=.04, value_type=float,
           help_msg="Standard deviation of Gaussian noise."),
        FP(name="mean", default=0., value_type=float,
           help_msg="Mean of Gaussian noise."),
        FP(name="clip", default=False, value_type=tuple,
           help_msg="If set, clip the image values into given interval."),
        FP(name="seed", default=0,
           help_msg="Initializer for the random generator."),
    ]

    def __init__(self, attr=None, std=0.04, mean=0., clip=None, seed=5):
        super(GaussianNoise, self).__init__()
        self.attr = attr or "img"
        self.std = std
        self.mean = mean
        self.clip = clip
        self.rng = np.random.RandomState(seed)

    def noise(self, packet, hpacket=None):
        """Add Gaussion noise"""
        img = getattr(packet, self.attr)
        noise = self.rng.normal(loc=self.mean, scale=self.std, size=img.shape)

        img += noise

        if isinstance(self.clip, (list, tuple)):
            np.clip(img, self.clip[0], self.clip[1], img)

        return [packet]

    def __call__(self, packet, hpacket):
        return self.noise(packet, hpacket)


class SaltPepperNoise(Filter):

    """Filter adding salt&pepper noise to input image"""
    parameters = [
        FP(name="attr", default="img",
           help_msg="Attribute name to apply the noise to."),
        FP(name="seed", default=0,
           help_msg="Initializer for the random generator. \
           Can be any hashable object."),
        FP(name="noise_ratio", default=.04, value_type=float,
           help_msg="Amount of the noise in relation to the pixel count"),
        FP(name="salt_pepper_ratio", default=.5, value_type=float,
           help_msg="Ratio between the usage of the two noise types")

    ]

    def __init__(self, attr, noise_ratio=.05, salt_pepper_ratio=.5,
                 salt_pepper_val=None, seed=0):
        """Initializes the filter"""
        super(SaltPepperNoise, self).__init__()
        self.attr = attr or "img"
        self.noise_ratio = noise_ratio
        self.salt_pepper_ratio = salt_pepper_ratio
        self.salt_pepper_val = salt_pepper_val or (0, 255)
        self.rng = np.random.RandomState(seed)

    def _add_noise(self, img):
        n_salt_coord = int(
            self.noise_ratio * img.size * self.salt_pepper_ratio)
        salt_coord =tuple(self.rng.randint(0, i - 1, n_salt_coord)
                      for i in img.shape[:2])

        n_pepper_coord = int(
            self.noise_ratio * img.size * (1 - self.salt_pepper_ratio))
        pepper_coord = tuple(self.rng.randint(0, i - 1, n_pepper_coord)
                        for i in img.shape[:2])

        img[salt_coord] = self.salt_pepper_val[1]
        img[pepper_coord] = self.salt_pepper_val[0]

        return img

    def transform(self, packet, hpacket):
        """Adds s&p noise to input image

        Parameters
        ----------
        packet : Packet
        expected attributes:
            img : ndarray
                Input image

        Exceptions
        ----------
        ValueError - raised when input data is invalid
        AttributeError - raised when packet doesn't contain
        the required attributes

        Returns
        -------
        out : [Packet]
              List of Packet with the transformed image
        """
        try:
            img = getattr(packet, self.attr)

            if isinstance(img, (list, tuple)):
                img_list = []
                for item in img:
                    img_list.append(self._add_noise(item))
                setattr(packet, self.attr, img_list)
            else:
                img_out = self._add_noise(img)
                setattr(packet, self.attr, img_out)

            return [packet]

        except ValueError:
            self.log.exception(
                'Value error at %s filter', self.__class__)
            raise
        except AttributeError:
            self.log.exception(
                "Attribute error at %s filter", self.__class__)
            raise

    def __call__(self, packet, hpacket):
        """Returns the transformed image"""
        return self.transform(packet, hpacket)


class ToColor(Filter):

    """Filter will change the number of color channels of the input
    images - the output image will have three channels

    Parameters
    ----------
    attr: str
    Attribute name to apply the ToGray() to. Default is "img".
    """

    parameters = [
        FP(name="attr", default="img",
           help_msg="Attribute name to apply the operation to.")
    ]

    def __init__(self, attr="img"):
        """Initializes the filter"""
        super(ToColor, self).__init__()
        self.attr = attr

    def transform(self, packet, hpacket):
        """Changes the number of color channels of the input image -
           the output image will have three channels

        Parameters
        ----------
        packet : Packet
        expected attributes:
            img : ndarray
                Input image

        Exceptions
        ----------
        ValueError - raised when input data is invalid
        AttributeError - raised when packet doesn't contain the required
        attributes

        Returns
        -------
        out : [Packet]
              List of Packet with the transformed image
        """
        try:
            img = getattr(packet, self.attr)

            if len(img.shape) == 2 or img.shape[2] == 1:
                img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
                setattr(packet, self.attr, img)
            elif len(img.shape) == 3:
                img = cv2.cvtColor(img, cv2.COLOR_RGBA2RGB)
                setattr(packet, self.attr, img)
            return [packet]

        except ValueError:
            self.log.exception(
                'Value error at {} filter'.format(self.__class__))
            raise
        except AttributeError:
            self.log.exception(
                "Attribute error at {} filter".format(self.__class__))
            raise

    def __call__(self, packet, hpacket):
        """Returns the transformed image"""
        return self.transform(packet, hpacket)


class ToGray(Filter):
    """Filter will change the number of color channels of the input
    images - the output image will have only one channel

    Parameters
    ----------
    attr: str
    Attribute name to apply the ToGray() to. Default is "img".

    """
    parameters = [
        FP(name="attr", default="img",
           help_msg="Attribute name to apply the operation to.")
    ]

    def __init__(self, attr="img"):
        """Initializes the filter"""
        super(ToGray, self).__init__()
        self.attr = attr

    def transform(self, packet, hpacket):
        """Changes the number of color channels of the input image -
           the output image will have three channels

        Parameters
        ----------
        packet : Packet
        expected attributes:
            img : ndarray
                Input image

        Exceptions
        ----------
        ValueError - raised when input data is invalid
        AttributeError - raised when packet doesn't contain
        the required attributes

        Returns
        -------
        out : [Packet]
              List of Packet with the transformed image
        """
        try:
            img = getattr(packet, self.attr)

            if len(img.shape) == 3:
                if img.shape[2] == 3:
                    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
                    setattr(packet, self.attr, img)
                elif img.shape[2] == 4:
                    img = cv2.cvtColor(img, cv2.COLOR_RGBA2GRAY)
                    setattr(packet, self.attr, img)
            return [packet]

        except ValueError:
            self.log.exception(
                'Value error at {} filter'.format(self.__class__))
            raise
        except AttributeError:
            self.log.exception(
                "Attribute error at {} filter".format(self.__class__))
            raise

    def __call__(self, packet, hpacket):
        """Returns the transformed image"""
        return self.transform(packet, hpacket)
