# coding=utf-8
"""PyZMQ reader and writer

Based on http://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/patterns/pushpull.html
"""

# pylint: disable=R0903,C0103

from __future__ import division

import copy
import sys
import time
from collections import deque

import msgpack
import numpy as np
import zmq

from ..utils import LazyInit
from ..utils.loging_levels import TIME_TRANSFER
from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP
from .filters import Packet

__all__ = [
    "ZMQReader",
    "ZMQWriter"
]


class ZMQReader(Filter):
    """Reads packets from a ZeroMQ socket

    Parameters
    ----------
    address : str
        String in the form of protocol://endpoint
        see http://api.zeromq.org/2-1:zmq-connect for more details.
        Default is "tcp://127.0.0.1:8521". On POSIX systems one can use
        inter-process protocol "ipc:///tmp/ipp"
    con : str
        Define connection type, 'connect' or 'bind'. Default is
        'bind' to allow many writers and single reader. Use 'connect'
        in the opposite architecture, ie single writer many readers.
    timeout : int
        Milliseconds to wait if flag zmq.NOBLOCK is set. If nothing
        received, throws StopIteration exception. Default is 5000ms.
        If timeout=None the read is blocking.
    flags : int or tople of str or list of str
        zmq flags recv related flags. If str, the flag is obtained
        by getattr(zmq, flag_str)
        - 0 blocks the reading till received message
        - zmq.NOBLOCK or 'NOBLOCK' related to defined timeout
    cp : boolean
        Copy, see the note, default is True
    track : boolean
        See the note, default is False
    hwm : int
        High water mark see http://api.zeromq.org/2-1:zmq-setsockopt
        Maximum number of messages the socket can enqueue

    Note
    ----
        ZMQ allows to avoid copying numpy data but only within *single*
        process. This allows almost zero cost transfer between writer
        and reader.

    Throws
    ------
        StopIteration
            If flag zmq.NOBLOCK is set and no message received in given
            timeout.
    """

    parameters = [
        FP(name="address", help_msg="A socket to read data from.",
           required=False)
    ]

    def __init__(self, address=None, con=None, timeout=5000, flags=0, cp=True,
                 track=False, hwm=64):
        """Constructor of ZMQReader"""
        super(ZMQReader, self).__init__()
        self.address = address or "tcp://127.0.0.1:8521"  # "ipc:///tmp/ipp"
        self.con = con or "bind"
        self.timeout = timeout

        if isinstance(flags, (int,)):
            self.flags = flags
        elif isinstance(flags, (list, tuple)):
            self.flags = np.bitwise_or.reduce(
                [getattr(zmq, flag) for flag in flags])
        assert self.con == "bind" or self.con == "connect"

        self.cp = cp
        self.track = track
        self.hwm = hwm

        self.inner_timeout = 500
        self.mavg_win = 100

    def _connect(self):
        # Logging related
        self.verbosity = self.log.getEffectiveLevel()
        self.mavg_deq = deque(maxlen=self.mavg_win)
        # Socket related
        self.ctx = zmq.Context()
        self.socket = self.ctx.socket(zmq.PULL)
        self.socket.set_hwm(self.hwm)
        self.socket.linger = 0  # period for socket shutdown; 0 - immediately

        if self.con == "bind":
            self.log.info("Bind to: %s", self.address)
            self.socket.bind(self.address)
        else:
            self.log.info("Connect to: %s", self.address)
            self.socket.connect(self.address)
        self.poller = zmq.Poller()
        self.poller.register(self.socket, zmq.POLLIN)

    def _parse_to_packets(self, message):
        """Message parsing into packet

        message: [packet, [header_d], d, packet, [header_d], d, d]

        Returns list of packets.
        """
        packets = []
        i_msg = 0

        while message[i_msg:]:
            packet_dict = msgpack.unpackb(message[i_msg], encoding="utf-8")
            i_msg += 1
            d_header = msgpack.unpackb(message[i_msg], encoding="utf-8")
            i_msg += 1
            for header in d_header:
                data = np.frombuffer(
                    message[i_msg], dtype=header[1]).reshape(header[2])
                i_msg += 1
                packet_dict[header[0]] = data

            packet = Packet(**packet_dict)
            packets.append(packet)

        return packets

    def _read(self):
        """Poller based socket read"""
        self.wait_time = 0
        while not self.poller.poll(self.inner_timeout):
            self.wait_time += self.inner_timeout
            if self.timeout and self.wait_time >= self.timeout:
                self.log.error("read timeout (%d ms)", self.timeout)
                raise StopIteration

        msg = self.socket.recv_multipart(self.flags, self.cp, self.track)
        return msg

    @LazyInit("_connect")
    def read(self, packet=None, left_packets=None):
        """Reads and de-serialize message from a ZMQ socket"""
        if self.verbosity <= TIME_TRANSFER:
            start = time.time()

        packets = self._parse_to_packets(self._read())

        if self.verbosity <= TIME_TRANSFER:
            time_transfer = time.time() - start
            if len(self.mavg_deq) < self.mavg_win:
                self.mavg_deq.append(time_transfer)
                self.avg_time_transfer = sum(self.mavg_deq)/len(self.mavg_deq)
            else:
                self.avg_time_transfer += time_transfer/self.mavg_win - \
                    self.mavg_deq.popleft()/self.mavg_win
                self.mavg_deq.append(time_transfer)

            try:
                self.log.time_transfer(
                    "Transfer speed: %f GB/s",
                    self.packets_size / self.avg_time_transfer)
            except AttributeError:
                self.packets_size = np.sum(
                    [sys.getsizeof(p) for p in packets]) / 1073741824.
                self.log.time_transfer(
                    "Transfer speed: %f GB/s",
                    self.packets_size / self.avg_time_transfer)

        return packets

    def __call__(self, packet=None, left_packets=None):
        return self.read(packet, left_packets)

    def dispose(self):
        try:
            self.poller.unregister(self.socket)
            if self.con == "bind":
                self.socket.unbind(self.address)
                self.log.info("Socket %s unbind", self.address)
            else:
                self.socket.disconnect(self.address)
                self.log.info("Socket %s disconnected", self.address)
            # Use specific IPPLoggerLevels TIME_READER level
            # pylint: disable=E1101
        except AttributeError:
            self.log.info("Disposing")


class ZMQWriter(Filter):

    """Writes packet into ZMQ socket

    Parameters
    ----------
    address : str
        String in the form of protocol://endpoint
        see http://api.zeromq.org/2-1:zmq-connect for more details.
        Default is "tcp://127.0.0.1:8521". On POSIX systems one can use
        inter-process protocol "ipc:///tmp/ipp"
    timeout : int
        Timeout in milliseconds to raise StopIteration. Default is
        None, i.e. no timeout.
    con : str
        Define connection type, 'connect' or 'bind'. Default is
        'connect' to allow many writers and single reader. Use 'bind'
        in the opposite architecture, ie single writer many readers.
    flags : int, list of str
        ZMQ flags, see zmq docs for details.
    cp : boolean
        Copy, see the note, default is True
    track : boolean
        See the note, default is False
    hwm : int, optional
        High water mark see http://api.zeromq.org/2-1:zmq-setsockopt
        Maximum number of messages the socket can enqueue

    Note
    ----
        ZMQ allows to avoid copying numpy data but only within *single*
        process. This allows almost zero cost transfer between writer and
        reader.

    """

    parameters = [
        FP(name="address", help_msg="A queue to write data in.",
           required=False),
        FP(name="hwm", help_msg="HWM parameter of zmq socket.",
           required=False)
    ]

    def __init__(self, address=None, con=None, timeout=None, flags=0, cp=True,
                 track=False, hwm=64):
        """Constructor of ZMQWriter"""
        super(ZMQWriter, self).__init__()
        self.address = address if address is not None else "ipc:///tmp/ipp"
        self.con = "connect" if con is None else con
        assert self.con == "bind" or self.con == "connect"
        self.timeout = timeout

        if isinstance(flags, (int,)):
            self.flags = flags
        elif isinstance(flags, (list, tuple)):
            self.flags = np.bitwise_or.reduce(
                [getattr(zmq, flag) for flag in flags])

        self.cp = cp
        self.track = track
        self.hwm = hwm

        self.inner_timeout = 500

    def _connect(self):
        # Logging related
        self.verbosity = self.log.getEffectiveLevel()
        # Socket related
        self.ctx = zmq.Context()
        self.socket = self.ctx.socket(zmq.PUSH)
        self.socket.set_hwm(self.hwm)
        self.socket.linger = 0  # period for socket shutdown; 0 - immediately

        if self.con == "bind":
            self.log.info("Bind to: %s", self.address)
            self.socket.bind(self.address)
        else:
            self.log.info("Connect to: %s", self.address)
            self.socket.connect(self.address)

        self.poller = zmq.Poller()
        self.poller.register(self.socket, zmq.POLLOUT)

    def _parse_to_message(self, packet):
        """Parse packet into message

        Note
        ----
            The multipart message include packet header (not pickled)
            representing the dict of packet attributes and corresponding
            values except the numpy ndarray. Numpy ndarrays are
            represented by data_header (tuple) with the
            (key, dtype, shape) information, and the data itself.

            The message has the following structure:
            '(packet, [data_header], data, data, ...)'
            where packet is dict, data_header is tuple, and data is
            numpy ndarray.

        """
        data_header = []
        data = []
        # Make a shallow copy of packet header
        m_packet = copy.copy(packet.__dict__)
        for key in m_packet:
            item = m_packet[key]
            if isinstance(item, np.ndarray):
                d_header = (key.encode("utf-8"),
                            str(item.dtype).encode("utf-8"), item.shape)
                data_header.append(d_header)
                data.append(item)
                m_packet[key] = None

        return (m_packet, data_header, data)

    @LazyInit("_connect")
    def write(self, packet=None, left_packets=None):
        """Send a list of packets in a form of message to ZMQ socket.

        Parameters
        ----------
        packet : Packet
        left_packets: list of Packet

        Returns
        -------
        packet : [Packet]
            List of forwarded Packet.

        """
        packets = []
        if left_packets:
            packets.extend(left_packets)
        packets += [packet]

        message = []

        for pckt in packets:
            m_packet, h_data, data = self._parse_to_message(pckt)
            bm_packet = msgpack.packb(m_packet, encoding="utf-8")
            bh_data = msgpack.packb(h_data)
            message += [bm_packet] + [bh_data] + data

        self.wait_time = 0
        while not self.poller.poll(self.inner_timeout):
            self.wait_time += self.inner_timeout
            if self.timeout and self.wait_time >= self.timeout:
                self.log.error("write timeout (%d ms)", self.timeout)
                raise StopIteration
        try:
            self.socket.send_multipart(
                message, self.flags, self.cp, self.track)
        except zmq.Again:
            self.log.error("Failed to write packets to socket")
            raise StopIteration

        return [packet]

    def __call__(self, packet=None, left_packets=None):
        return self.write(packet, left_packets=left_packets)

    def dispose(self):
        try:
            self.poller.unregister(self.socket)
            if self.con == "bind":
                self.socket.unbind(self.address)
                self.log.info("Socket %s unbind", self.address)
            else:
                self.socket.disconnect(self.address)
                self.log.info("Socket %s disconnected", self.address)
        except AttributeError:
            self.log.info("Disposing")
