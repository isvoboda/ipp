# coding=utf-8
"""TXT file readers"""

from __future__ import division, print_function, unicode_literals

import codecs
import itertools
import re
# from builtins import str

import numpy as np

from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP
from .filters import Packet

__all__ = [
    "CSVReader"
]

# accepts: [number] or [number,number,...,number] where number may
# represent both, int or float
RELIST = re.compile(r"\[(\d+(\.\d*)?)(,\s*\d+(\.\d*)?)*\]", re.UNICODE)

class CSVReader(Filter):
    """Reads CSV file

    Parameters
    ----------
    path : str
        CSV file path.
    loop : bool
        Flag to loop over file. Default is False.
    shuffle : bool
        Flag to shuffle file. Default is False.
    seed : int
        Initializer for the random generator. Can be any hashable object.
    file_path_attr : str
        Attribute name the csv file path is stored to.
    sep : str
        Column separator, default is None which yields to python's
        default parameter of string.split method.
    encoding : str
        Defines encoding the file's content is encoded with.
        Default is utf-8.

    Exceptions
    ----------
    StopIteration
        End of file and loop is False.

    """

    parameters = [
        FP(name="path", required=True, help_msg="CSV file path."),
        FP(name="loop", required=False, help_msg="Flag to loop over file. \
            Default is False."),
        FP(name="shuffle", required=False, help_msg="Flag to shuffle file. \
            Default is False."),
        FP(name="seed", help_msg="Initializer for the random generator. \
            Can be any hashable object."),
        FP(name="file_path_attr", help_msg="Attribute name the csv file path is \
            stored to.", default="csv_file_path"),
        FP(name="sep", help_msg="Column separator, default is None which \
            corresponds to python's default parameter of string.split method.",
           default=None),
        FP(name="encoding", help_msg="Defines encoding the file's content is \
            encoded with. Default is utf-8.", default="utf-8", value_type=str)
    ]

    def __init__(self, path=None, loop=None, shuffle=None, seed=5,
                 file_path_attr="csv_file_path", sep=None, encoding="utf-8"):
        super(CSVReader, self).__init__()
        self.path = path
        self.loop = loop
        self.shuffle = shuffle
        self.seed = seed
        self.file_path_attr = file_path_attr
        self.rng = np.random.RandomState(seed)
        self.sep = sep
        self.encoding = encoding

        self.lines = []
        self.log.info("Loading data from %s", self.path)
        with codecs.open(self.path, "r", encoding=encoding) as desc:
            line = desc.readline()
            self.col_names = [col.strip() for col in line.split(self.sep)]
            gen = (line for line in desc if line not in "\n")
            for line in gen:
                parsed_line = {}
                for i_col, col in enumerate(line.split(self.sep)):
                    if RELIST.match(col) is not None:
                        parsed_line[self.col_names[i_col]] = [
                            float(val) for val in col[1:-1].split(",")]
                    else:
                        parsed_line[self.col_names[i_col]] = col.strip()

                self.lines.append(parsed_line)

        self.indices = list(range(len(self.lines)))
        if self.shuffle:
            self.rng.shuffle(self.indices)
        self.iter = itertools.cycle(
            self.indices) if loop else iter(self.indices)

    def __call__(self, packet=None, hpacket=None):
        ind = next(self.iter)
        packet = Packet(**self.lines[ind])
        setattr(packet, self.file_path_attr, self.path)

        return [packet]
