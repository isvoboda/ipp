# coding=utf-8
"""VectorImage reader"""

import logging
import inno.imgtrans as imgtrans
from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP


__all__ = [
    "IVecReader"
]


class IVecReader(Filter):

    """
    Reads an image from ImageVector

    Input
    -----
    packet.vector_path str
        defines a path to ImageVector
        or the attribute is defined as a filter parameter
        image_vector_path_attr
    packet.vector_idx str
        is an image index in ImageVector
        or the attribute is defined as a filter parameter
        image_vector_idx_attr

    Output
    ------
    packet.img ndarray
        An image from image_vector.
    """
    parameters = [
        FP(name="vector_path_attr", value_type=str, required=False,
           help_msg="The packet attribute name the ImageVector path is \
           stored in."),
        FP(name="image_idx_attr", value_type=str, required=False,
           help_msg="The packet attribute name the Image index in ImageVector \
           is stored in."),
        FP(name="in_memory", required=False,
           help_msg="Flag to load the whole ivec file into memory.")
    ]

    def __init__(self,
                 vector_path_attr=None,
                 image_idx_attr=None,
                 in_memory=False):
        """Initialize IVecReader

        Parameters
        ----------
        vector_path_attr : str
            packet attribute name to look for the vector path
        image_idx_attr : str
            packet attribute name to select the image idx
        in_memory : bool
            Flag to load the whole ivec file into memory.

        """
        super(IVecReader, self).__init__()
        self.opened_vectors = {}
        self.vector_path_attr = "vector_path" if vector_path_attr is None \
            else vector_path_attr
        self.image_idx_attr = "vector_idx" if image_idx_attr is None \
            else image_idx_attr
        self.in_memory = in_memory

    def read(self, packet):
        """Loads the image from ImageVector

        Parameters
        ----------
        packet : Packet

        Returns
        -------
        out : [Packet]
            List of Packet with loaded image (ndarray) packet.img.

        """

        try:
            path = getattr(packet, self.vector_path_attr)
            idx = getattr(packet, self.image_idx_attr)
            packet.img = None

            if path not in self.opened_vectors:
                self.opened_vectors[path] = imgtrans.ImageVector(
                    vector_path=path, in_memory=self.in_memory)

            image_vector = self.opened_vectors[path]
            packet.img = image_vector.get(idx)

            return [packet]

        except IOError:
            self.log.exception("Error reading %r", path)
            raise
        except AttributeError:
            self.log.exception("Attribute error")
            raise

    def __call__(self, packet, horizontal_packet=None):
        """Returns the image"""
        return self.read(packet)
