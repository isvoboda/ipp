# coding=utf-8
"""Queue readers and writers filters"""

# pylint: disable=R0903

import sys
import time

import numpy as np

from ..utils.loging_levels import TIME_FILTER, TIME_TRANSFER
from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP

try:
    from queue import Empty
except ImportError:
    from Queue import Empty


__all__ = [
    "QueReader",
    "QueWriter"
]


class QueReader(Filter):

    """Reads packet from a multiprocessing queue

    Read is a blocking operation with none timeout.

    Parameters
    ----------
    queue : multiprocessing.Queue
        A queue which from a packet is read.
    block : bool
        Flag defining blocking non-blocking queue reading.
    timeout : int
        Timeout to block reading.

    Raises
    ----------
    Queue.Empty
        When a sentinel is received.
    """

    parameters = [
        FP(name="queue", help_msg="A queue to read data from.", required=True),
        FP(name="block", help_msg="Flag if reading is blocking \
        (Default: True).", default=True, required=False),
        FP(name="timeout", help_msg="Timeout to block [Default: None].",
           required=False)
    ]

    def __init__(self, queue=None, block=True, timeout=None):
        """Constructor of QueReader"""

        super(QueReader, self).__init__()
        self.queue = queue
        self.block = block
        self.timeout = timeout
        self.sum_time = 0
        self.read_count = 0

    def read(self):
        """Reads a packet from a multiprocessing queue

        Read is a blocking operation with none timeout

        Returns
        -------
        packet : [Packet]
            List of Packet.

        Raises
        ------
        Queue.Empty

        """
        start = None
        try:
            if self.verbosity <= TIME_TRANSFER:
                start = time.time()

        except AttributeError:
            self.verbosity = self.log.getEffectiveLevel()
            if self.verbosity <= TIME_TRANSFER:
                start = time.time()

        self.read_count += 1

        packets = self.queue.get(block=self.block, timeout=self.timeout)

        if self.verbosity <= TIME_TRANSFER:
            stop = time.time()
            self.sum_time += (stop - start)

            try:
                self.log.time_transfer("Transfer speed: %f GB/s",
                                       self.packets_size /
                                       (self.sum_time / self.read_count))

            except AttributeError:
                self.packets_size = np.sum(
                    [sys.getsizeof(p) for p in packets]) / 1073741824.
                self.log.time_transfer("Transfer speed: %f GB/s",
                                       self.packets_size /
                                       (self.sum_time / self.read_count))

        if packets is None:
            raise Empty

        self.log.debug_filter("number of packets read %d", self.read_count)

        return packets

    def __call__(self, packet=None, hpacket=None):
        """Reads a packet from a multiprocessing queue

        Read is a blocking operation with none timeout

        Parameters
        ----------
        packet : Packet
            Not used.
        hpacket : packet
            Not used.

        Returns
        -------
        packet : [Packet]
            List of Packet.

        Raises
        ------
        Queue.Empty

        """

        return self.read()

    def dispose(self):
        try:
            while True:
                val = self.queue.get_nowait()
                if val is None:
                    break
        except (Empty, EOFError):
            self.log.info("Queue should be empty now")
        finally:
            self.log.info("Disposed")


class QueWriter(Filter):

    """Writes packet into multiprocessing queue

    Write is a blocking operation with none timeout.

    Parameters
    ----------
    queue : multiprocessing.Queue
        A queue which from a packet is written.
    block : bool
        Flag defining blocking non-blocking queue writing.
    timeout : int
        Timeout to block writing.

    """

    parameters = [
        FP(name="queue", help_msg="A queue to write data in.", required=True),
        FP(name="block", help_msg="Flag if writing is blocking \
        (Default: True).", default=True, required=False),
        FP(name="timeout", help_msg="Timeout to block [Default: None].",
           required=False)
    ]

    def __init__(self, queue=None, block=True, timeout=None):
        """Constructor of QueWriter"""
        super(QueWriter, self).__init__()
        self.queue = queue
        self.block = block
        self.timeout = timeout

    def write(self, packets):
        """Writes a list of packets into a multiprocessing queue.

        Write is a blocking operation with none timeout

        Parameters
        ----------
        packets : list
        Is a list of packets [packet,...] to be send.
        """

        self.queue.put(packets, block=self.block, timeout=self.timeout)

    def __call__(self, packet, hpacket=None):
        """Writes a list of packet into a multiprocessing queue.

        Write is a blocking operation with none timeout

        Parameters
        ----------
        packet : Packet

        Returns
        -------
        packet : [Packet]
            List of forwarded Packet.

        """
        que_packets = []
        if hpacket is not None and hasattr(hpacket, "packets"):
            que_packets.extend(hpacket.packets)
        que_packets.append(packet)

        self.write(que_packets)

        return [packet]

    def write_sentinel(self):
        """Writes a sentinel packet """

        self.queue.put(None, block=self.block, timeout=self.timeout)
        self.queue.close()
        # self.queue.cancel_join_thread()
        self.log.info("Queue closed")

    def dispose(self):
        """Writes a sentinel"""
        try:
            self.write_sentinel()
        finally:
            self.log.info("Disposed")
