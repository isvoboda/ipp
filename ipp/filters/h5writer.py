# coding=utf-8
"""H5 file writer"""

from __future__ import division, print_function

import pandas as pd

from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP

# pylint: disable=R0903
# pylint: disable=C0302


__all__ = [
    "PandasH5Writer"
]


class PandasH5Writer(Filter):

    """
    Writes the HDF5 data file based on Pandas

    Attributes
    ----------
    h5file : str
        Path to a h5 file.
    data_path : str
        Path do a dataset in a h5 file.
    columns : list
        List of column names to be saved to a dataset.
    Raises
    ------
    ValueError

    """
    parameters = [
        FP(name="h5file", required=True, value_type=str,
           help_msg="Path to the HDF5 file."),
        FP(name="table_name", required=True, value_type=str,
           help_msg="Name of table (e.g. table \
           will be stored to /table/table)."),
        FP(name="columns", required=False,
           help_msg="List of relevant columns. \
           If None is given, all columns are used."),
    ]

    def __init__(self, h5file=None, table_name=None, columns=None):
        """Initializes H5Reader

        Parameters
        ----------
        h5file : str
            Path to the HDF5 file.
        data_path : str
            Name of table (e.g. table will be stored to /table/table).
        columns : list
            List of relevant columns. If None is given, all columns are used.
        """
        super(PandasH5Writer, self).__init__()
        self.h5file = h5file
        self.table_name = table_name
        self.columns = columns

        self.rows = []

    # pylint: disable=W0613
    def write(self, packet, hpacket=None):
        """Writes a row to the memory for later storing to h5 file

        Parameters
        ----------
        packet : Packet
            packet to be saved
        hpacket not used

        Returns
        -------
        out : [Packet]
            Forwards list of packets from the input

        """
        dict_in_packet = {}

        if self.columns is not None:
            for key, value in packet.__dict__.items():
                if key in self.columns:
                    dict_in_packet[key] = value
        else:
            dict_in_packet = packet.__dict__

        self.rows.append(dict_in_packet)
        return [packet]

    def dispose(self):
        """Strore rows to h5 with pandas"""
        df = pd.DataFrame(self.rows)
        store = pd.HDFStore(self.h5file)
        store.put(self.table_name, df, format="table", data_columns=True)
        store.flush(fsync=True)
        store.close()

    def __call__(self, packet, hpacket=None):
        return self.write(packet, hpacket)
