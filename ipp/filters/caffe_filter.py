# coding=utf-8
"""Caffe test net filter"""

from __future__ import division, print_function

import numpy as np

import caffe
from ipp.filters import FilterParameter as FP
from ipp.filters import Filter

__all__ = [
    "CaffeFilter",
    "OpCaffeMean"
]


class CaffeFilter(Filter):

    """Simple caffe model filter

    A purpose of CaffeFilter is to provide an ability to plug the caffe
    model evaluation into the processing pipeline.
    In case of fully convolutional network, the filter can reshape
    the model according the input data.

    Parameters
    ----------
    net : str
        Path to cafe net definition file (*.prototxt).
    model : str, optional
        Is the path to caffe model (*.caffemodel). If a model path
        is not provided an randomly initialized model based on caffe
        definition is used.
    mode : str, optional
        Defines a caffe mode to operate in. Available values are:
        'gpu' and 'cpu'. Default is 'cpu'.
    gpu_id : int, optional
        Defines a GPU id on a multi GPU machine. In case it is not
        specified and a mode is set to 'gpu', the one with most
        available memory is used.
    layers: list
        List of layer names to extract from the network.
    batch_size : int
        Size of input minibatch.

    Returns
    -------
    out : list
        A list of Packet with the net_out data added.

    Note
    ----
    Because of caffe wrapping filter, which can be run in other process
    then in which it was initialized, the caffe initialization is
    postponed to first packet processing.
    """

    parameters = [
        FP(name="net", required=True,
           help_msg="Path to cafe net definition file (*.prototxt)."),
        FP(name="model", required=False,
           help_msg="Is the path to caffe model (*.caffemodel). If a model \
           path is not provided an randomly initialized model based on caffe \
           definition is used."),
        FP(name="mode", required=False,
           help_msg="Defines a caffe mode to operate in. Available values \
           are: 'gpu' and 'cpu'. Default is 'cpu'."),
        FP(name="gpu_id", required=False,
           help_msg="Defines a GPU id on a multi GPU machine. In case it is \
           not specified and a mode is set to 'gpu', the one with most \
           available memory is used."),
        FP(name="layers", required=False,
           help_msg="List of layer names to extract from the network."),
        FP(name="batch_size", required=False, default=1,
           help_msg="Size of input minibatch.")
    ]

    def __init__(self, net=None, model=None, mode=None, gpu_id=None,
                 layers=None, batch_size=1):
        super(CaffeFilter, self).__init__()
        self.net_definition = net
        self.model = model
        self.mode = mode or "cpu"
        self.gpu_id = gpu_id
        self.layers = layers
        self.batch_size = batch_size

        # Has to be initialized later when run as it may be in other proc.
        self.initialized = False

    def _select_input(self, packet):
        """Binds a propper method according the 'in_net' of packet"""

        if isinstance(packet.in_net[0], (np.ndarray,)):
            self.set_data = self._set_inputs
        elif isinstance(packet.in_net[0], (list,)):
            self.set_data = self._set_inputs_batch
        else:
            raise ValueError

    def _init_caffe(self):
        if self.mode.lower() == "gpu":
            caffe.set_mode_gpu()
            if self.gpu_id is None:
                from .. utils.gpu_info import GPUInfo
                self.gpu_id = GPUInfo.gpu_id()
            caffe.set_device(self.gpu_id)
        else:
            caffe.set_mode_cpu()

        self.net = caffe.Net(self.net_definition, caffe.TEST)
        self.initialized = True

        if self.model is not None:
            self.log.info("Using a model: %s", self.model)
            self.net.copy_from(self.model)
        else:
            self.log.info("Using randomly initialized model!")

    def _set_input(self, packet):
        """Model has single input"""
        i_batch = 0
        key = packet.label
        net_input_shape = self.net.blobs[key].data.shape[1:]
        packet_input_shape = packet.in_net.shape

        self.log.debug("Packet mapped to layer %s", key)
        self.log.debug("Packet blob %s size: %s", key, packet_input_shape)
        self.log.debug("Input layer %s size: %s", key, packet_input_shape)

        # Check if reshape is needed
        reshape_flag = all(packet_blob == net_blob
                           for packet_blob, net_blob
                           in zip(packet_input_shape, net_input_shape))

        if reshape_flag is False:
            self.log.info("Reshaping the network from %s to %s",
                          net_input_shape, packet_input_shape)

            self.net.blobs[key].reshape(self.batch_size, *packet_input_shape)
            self.net.reshape()

        # Set net input data
        self.net.blobs[key].data[i_batch][...] = packet.in_net

    def _set_inputs(self, packet):
        """Set single data per layer

        Every input layer is addressed by a list of names - packet.label
        aka keys.
        Input data are addressed by a list of data aka packet.in_net.
        """

        i_batch = 0
        keys = packet.label
        net_input_shapes = [self.net.blobs[key].data.shape[1:] for key in keys]
        packet_input_shapes = [
            packet.in_net[i].shape for i in range(len(packet.in_net))]

        for key, packet_shape, net_shape in zip(keys, packet_input_shapes,
                                                net_input_shapes):
            self.log.debug("Packet mapped to layer %s", key)
            self.log.debug("Packet blob %s size: %s", key, packet_shape)
            self.log.debug("Input layer %s size: %s", key, net_shape)

        # Check if reshape is needed
        reshape_flag = all(np.array_equal(packet_blob, net_blob)
                           for packet_blob, net_blob
                           in zip(packet_input_shapes, net_input_shapes))

        if reshape_flag is False:
            for key, packet_shape, net_shape in zip(keys, packet_input_shapes,
                                                    net_input_shapes):
                self.log.info("Reshaping the network's layer %s from %s to %s",
                              key, net_shape, packet_shape)

                self.net.blobs[key].reshape(self.batch_size, *packet_shape)

            self.log.info("Reshape the whole network based on input layers")
            self.net.reshape()

        # Set net input data
        for key, data in zip(keys, packet.in_net):
            self.net.blobs[key].data[i_batch][...] = data

    def _set_inputs_batch(self, packet):
        """Set batch data per layer

        Every input layer is addressed by a list of names - packet.label
        aka keys.
        Input data are addressed by a list of data aka packet.in_net.
        """
        assert len(packet.in_net[0]) == self.batch_size
        self._reshape(packet)

        # Set net input data
        for key, data in zip(packet.label, packet.in_net):
            for i_batch in range(self.batch_size):
                self.net.blobs[key].data[i_batch][...] = data[i_batch]

    def _reshape(self, packet):
        """Check input packet blobs shape and input layer shapes"""

        n_shapes = [self.net.blobs[key].data.shape for key in packet.label]
        p_shapes = [[self.batch_size] + list(packet.in_net[i][0].shape)
                    for i in range(len(packet.in_net))]

        for key, p_shape, n_shape in zip(packet.label, p_shapes, n_shapes):
            self.log.debug("Packet mapped to layer %s", key)
            self.log.debug("Packet blob %s size: %s", key, p_shape)
            self.log.debug("Input layer %s size: %s", key, n_shape)

        # Check if reshape is needed
        reshape_flag = not all(np.array_equal(p_blob, n_blob)
                               for p_blob, n_blob in zip(p_shapes, n_shapes))

        if reshape_flag:
            for key, p_shape, n_shape in zip(packet.label, p_shapes, n_shapes):
                self.log.info("Reshaping the network's layer %s from %s to %s",
                              key, n_shape, p_shape)
                self.net.blobs[key].reshape(*p_shape)

            self.log.info("Reshape the whole network based on input layers")
            self.net.reshape()

    def __call__(self, packet, hpacket=None):
        if not self.initialized:
            self._init_caffe()
            self._select_input(packet)

        self.set_data(packet)
        net_out = self.net.forward()

        if self.layers is not None:
            packet.net_out = {layer: self.net.blobs[
                layer].data for layer in self.layers}
        else:
            packet.net_out = net_out

        return [packet]


class OpCaffeMean(Filter):

    """Provides an addition/subtraction of binaryproto (caffe) mean image

    Parameters
    ----------
    path : str
        Mean image path.
    op : str
        Defines the operation to provide. Supported is 'plus' or '+'
        for addition and 'minus' or '-' for subtraction.
    attr : str, optional
        Attribute to apply the operation on. Default is 'img'

    """

    def __init__(self, path, op, attr=None):

        super(OpCaffeMean, self).__init__()

        self.path = path
        self.op_str = op
        self.attr = "img" if attr is None else attr
        self.log.info("Loading mean image: %s", self.path)
        self.log.info("Operation: %s", self.op_str)
        blob = caffe.proto.caffe_pb2.BlobProto()
        with open(name=self.path, mode='rb') as mean_file:
            blob.ParseFromString(mean_file.read())
            self.mean_img = caffe.io.blobproto_to_array(blob)[0]

        if self.op_str == "+" or self.op_str.lower() == "plus":
            self.op = lambda x: x + self.mean_img  # pylint: disable=C0103
        elif self.op_str == "-" or self.op_str.lower() == "minus":
            self.op = lambda x: x - self.mean_img

    def __call__(self, packet, hpacket=None):
        img = getattr(packet, self.attr)
        img = self.op(img)
        setattr(packet, self.attr, img)

        return [packet]
