# coding=utf-8
"""TXT file writer"""

from __future__ import division, print_function, unicode_literals

import codecs

from .filter_factory import Filter
from .filter_parameter import FilterParameter as FP

__all__ = [
    "TXTWriter"
    "CSVWriter"
]


class TXTWriter(Filter):
    """Writes TXT file

    Parameters
    ----------
    path : str
        TXT file path.

    """

    parameters = [
        FP(name="file", required=True, help_msg="TXT file path.")
    ]

    def __init__(self, file=None, encoding="utf-8"):
        super(TXTWriter, self).__init__()
        self.file = file
        self.encoding = encoding

        self.open_file = codecs.open(self.file, "w", encoding=encoding)
        self.log.info("Opened %s", self.file)

    def __call__(self, packet=None, hpacket=None):

        self.open_file.write(str(packet))
        self.open_file.write("\n")
        # As the filter runs in a different process than it was created,
        # ie file object acquired, without the flush here,
        # the subprocess buffer will never be written in to
        # the file (even the close is called as it is called in
        # the parent process).
        self.open_file.flush()
        return [packet]

    def dispose(self):
        self.open_file.close()
        self.log.info("Closed %s", self.file)


class CSVWriter(Filter):
    """Writes CSV file

    Parameters
    ----------
    path : str
        CSV file path.

    ignore : list of str
        array of packet attributes to be ignored

    separator : str
        string which will separate attributes in the file

    """

    parameters = [
        FP(name="file", required=True, help_msg="CSV file path."),
        FP(name="ignore", required=False,
           help_msg="Packet attributes to be ignored."),
        FP(name="separator", required=False,
           help_msg="Packet attributes to be ignored.")
    ]

    def __init__(self, file=None, encoding="utf-8", ignore=None, separator="\t"):
        super(CSVWriter, self).__init__()
        self.file = file
        self.encoding = encoding
        self.ignore = ignore if ignore is not None else []
        self.separator = separator

    def _lazy_open(self):
        self.open_file = codecs.open(self.file, "w", encoding=self.encoding)
        self.log.info("Opened %s", self.file)

    def _write_header(self, packet):
        self.header = [key for key in packet.__dict__.keys()
                       if key not in self.ignore]
        self.header.sort()
        self.open_file.write(self.separator.join(self.header))
        self.open_file.write("\n")

    def _write_body(self, packet):
        row = ["{}".format(getattr(packet, key, None)) for key in self.header]
        self.open_file.write(self.separator.join(row))
        self.open_file.write("\n")

    def __call__(self, packet=None, hpacket=None):

        try:
            self._write_body(packet)
        except AttributeError:
            # If self.header is not created yet, create it and do it only once
            self._write_header(packet)
            self._write_body(packet)

        # Not true since the LazyInit decorator. Follows old note.
        # ------------------
        # As the filter runs in a different process than it was created,
        # ie file object acquired, without the flush here,
        # the subprocess buffer will never be written in to
        # the file (even the close is called as it is called in
        # the parent process).
        self.open_file.flush()

        return [packet]

    def dispose(self):
        self.open_file.close()
        self.log.info("Closed %s", self.file)
